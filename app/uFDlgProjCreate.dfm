object FDlgProjCreate: TFDlgProjCreate
  Left = 238
  Top = 142
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1057#1086#1079#1076#1072#1090#1100' '#1087#1088#1086#1077#1082#1090
  ClientHeight = 258
  ClientWidth = 516
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 15
    Top = 25
    Width = 59
    Height = 13
    Caption = #1064#1080#1088#1080#1085#1072' (px)'
  end
  object Label2: TLabel
    Left = 15
    Top = 60
    Width = 58
    Height = 13
    Caption = #1042#1099#1089#1086#1090#1072' (px)'
  end
  object Label3: TLabel
    Left = 15
    Top = 95
    Width = 54
    Height = 13
    Caption = #1062#1074#1077#1090' '#1092#1086#1085#1072
  end
  object Label4: TLabel
    Left = 260
    Top = 25
    Width = 53
    Height = 13
    Caption = #1054#1087#1080#1089#1072#1085#1080#1077':'
  end
  object Bevel1: TBevel
    Left = 15
    Top = 187
    Width = 481
    Height = 20
    Shape = bsBottomLine
  end
  object Label5: TLabel
    Left = 15
    Top = 130
    Width = 96
    Height = 13
    Caption = #1052#1072#1089#1096'. '#1074#1099#1095#1080#1089#1083#1077#1085#1080#1081
  end
  object Button1: TButton
    Left = 420
    Top = 220
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 1
  end
  object Button2: TButton
    Left = 330
    Top = 220
    Width = 75
    Height = 25
    Caption = 'Ok'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object Button3: TButton
    Left = 15
    Top = 220
    Width = 75
    Height = 25
    Caption = #1057#1086#1093#1088'. '#1087#1086' '#1091#1084'.'
    TabOrder = 7
    OnClick = Button3Click
  end
  object ColorBox1: TColorBox
    Left = 120
    Top = 90
    Width = 120
    Height = 22
    DefaultColorColor = clWhite
    NoneColorColor = clWhite
    Selected = clWhite
    Style = [cbStandardColors, cbPrettyNames]
    ItemHeight = 16
    TabOrder = 4
  end
  object SpEdW: TSpinEdit
    Left = 120
    Top = 20
    Width = 120
    Height = 22
    Increment = 10
    MaxValue = 10000
    MinValue = 1
    TabOrder = 2
    Value = 1
  end
  object SpEdH: TSpinEdit
    Left = 120
    Top = 55
    Width = 120
    Height = 22
    Increment = 10
    MaxValue = 10000
    MinValue = 1
    TabOrder = 3
    Value = 1
  end
  object Memo1: TMemo
    Left = 260
    Top = 55
    Width = 236
    Height = 57
    ScrollBars = ssVertical
    TabOrder = 6
  end
  object EdScale: TEdit
    Left = 120
    Top = 125
    Width = 120
    Height = 21
    TabOrder = 5
    Text = '0,01'
    OnKeyPress = EdScaleKeyPress
  end
  object Button4: TButton
    Left = 105
    Top = 220
    Width = 75
    Height = 25
    Caption = #1047#1072#1075#1088'. '#1087#1086' '#1091#1084'.'
    TabOrder = 8
    OnClick = Button4Click
  end
end
