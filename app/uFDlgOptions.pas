unit uFDlgOptions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, Spin, IniFiles, Registry;

type
  TFDlgOptions = class(TForm)
    Button1: TButton;
    Button2: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CheckBox1: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    TabSheet3: TTabSheet;
    Label3: TLabel;
    Label4: TLabel;
    SpinEdit3: TSpinEdit;
    SpinEdit4: TSpinEdit;
    CheckBox2: TCheckBox;
    TrackBar1: TTrackBar;
    Label5: TLabel;
    TrackBar2: TTrackBar;
    Label6: TLabel;
    TrackBar3: TTrackBar;
    TrackBar4: TTrackBar;
    TabSheet4: TTabSheet;
    Bevel1: TBevel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Button3: TButton;
    Button4: TButton;
    Label11: TLabel;
    SpinEdit1: TSpinEdit;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure TrackBar2Change(Sender: TObject);
    procedure TrackBar3Change(Sender: TObject);
    procedure TrackBar4Change(Sender: TObject);
    procedure SpinEdit4Change(Sender: TObject);
    procedure SpinEdit3Change(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure CheckBox4Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure WriteSettings;
    procedure ReadSettings;
    procedure AssociateFile;
    procedure DeassociateFile;
    procedure CheckAssociation;
  end;

var
  FDlgOptions: TFDlgOptions;

implementation

uses uFMain;

{$R *.dfm}

{-----------------------------------------------------------------------------}
{  procedure     WriteSettings                                                }
{                                                                             }
{  discription   ���������� �������� ��������� � ���.ini                      }
{-----------------------------------------------------------------------------}
procedure TFDlgOptions.WriteSettings;
begin
  FMain.cfg.WriteBool('Options','FullPath',CheckBox1.Checked);

  FMain.cfg.WriteInteger('Options','TimerDelay',TrackBar1.Position * 10);
  FMain.cfg.WriteInteger('Options','ViewDelay',TrackBar2.Position * 10);
  FMain.cfg.WriteBool('Options','Visualization',CheckBox4.Checked);
  FMain.cfg.WriteBool('Options','StopWhenChanging',CheckBox2.Checked);

  FMain.cfg.WriteInteger('Options','CopyX',SpinEdit3.Value);
  FMain.cfg.WriteInteger('Options','CopyY',SpinEdit4.Value);

  FMain.cfg.WriteBool('History','Enabled',CheckBox3.Checked);
  FMain.cfg.WriteInteger('History','MaxItemCount',SpinEdit1.Value);
  FMain.cfg.UpdateFile;
end;

{-----------------------------------------------------------------------------}
{  procedure     ReadSettings                                                 }
{                                                                             }
{  discription   ���������� �������� ��������� �� ���.ini                     }
{-----------------------------------------------------------------------------}
procedure TFDlgOptions.ReadSettings;
begin
  CheckBox1.Checked:=FMain.cfg.ReadBool('Options','FullPath',False);

  TrackBar1.Position:=FMain.cfg.ReadInteger('Options','TimerDelay',10) div 10;
  TrackBar2.Position:=FMain.cfg.ReadInteger('Options','ViewDelay',10) div 10;
  CheckBox4.Checked:=FMain.cfg.ReadBool('Options','Visualization',True);
  CheckBox2.Checked:=FMain.cfg.ReadBool('Options','StopWhenChanging',False);

  SpinEdit3.Value:=FMain.cfg.ReadInteger('Options','CopyX',50);
  SpinEdit4.Value:=FMain.cfg.ReadInteger('Options','CopyY',50);

  CheckBox3.Checked:=FMain.cfg.ReadBool('History','Enabled',True);
  SpinEdit1.Value:=FMain.cfg.ReadInteger('History','MaxItemCount',10);
end;

{-----------------------------------------------------------------------------}
{  procedure     AssociateFile                                                }
{                                                                             }
{  discription   ���������� � ����� ������ pro                                }
{-----------------------------------------------------------------------------}
procedure TFDlgOptions.AssociateFile;
var
  Reg: TRegistry;
begin
  try
    Reg:=TRegistry.Create;
    Reg.RootKey:=HKEY_CLASSES_ROOT;
  finally
    Reg.OpenKey('.pro', True);
    Reg.WriteString('', 'profile');
    Reg.CloseKey;
    Reg.OpenKey('profile\DefaultIcon', True);
    Reg.WriteString('', ExtractFilePath(ParamStr(0))+'FileIcon.ico');
    Reg.CloseKey;
    Reg.OpenKey('profile\shell\open\command', True);
    Reg.WriteString('', Application.ExeName+' "%1"');
    Reg.CloseKey;
    Reg.OpenKey('profile\', True);
    Reg.WriteString('', 'MIG Project');
    Reg.CloseKey;
    Reg.Free;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     DeassociateFile                                              }
{                                                                             }
{  discription   �������� ���������� � ����� ������ pro                       }
{-----------------------------------------------------------------------------}
procedure TFDlgOptions.DeassociateFile;
var
  Reg: TRegistry;
begin
  try
    Reg:=TRegistry.Create;
    Reg.RootKey:=HKEY_CLASSES_ROOT;
  finally
    Reg.DeleteKey('.pro');
    Reg.DeleteKey('profile\');
    Reg.Free;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     CheckAssociation                                             }
{                                                                             }
{  discription   �������� ���������� � ����� ������ pro                       }
{-----------------------------------------------------------------------------}
procedure TFDlgOptions.CheckAssociation;
var
  Reg: TRegistry;
begin
  try
    Reg:=TRegistry.Create;
    Reg.RootKey:=HKEY_CLASSES_ROOT;
  finally
    Reg.OpenKey('profile\shell\open\command', True);
    if Reg.ReadString('')=Application.ExeName+' "%1"' then
      Label9.Caption:='����'
    else
      Label9.Caption:='���';
    Reg.CloseKey;
    Reg.Free;
  end;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             FDlgOptions                                     }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFDlgOptions.FormCreate(Sender: TObject);
begin
  ReadSettings;
end;

procedure TFDlgOptions.FormShow(Sender: TObject);
begin
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
  CheckAssociation;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             <Other procedures>                              }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFDlgOptions.TrackBar1Change(Sender: TObject);
begin
  Label5.Caption:=IntToStr(TrackBar1.Position*10)+' ��';
end;

procedure TFDlgOptions.TrackBar2Change(Sender: TObject);
begin
  Label6.Caption:=IntToStr(TrackBar2.Position*10)+' ��';
end;

procedure TFDlgOptions.TrackBar3Change(Sender: TObject);
begin
  SpinEdit3.Value:=TrackBar3.Position;
end;

procedure TFDlgOptions.TrackBar4Change(Sender: TObject);
begin
  SpinEdit4.Value:=TrackBar4.Position;
end;

procedure TFDlgOptions.SpinEdit3Change(Sender: TObject);
begin
  TrackBar3.Position:=SpinEdit3.Value;
end;

procedure TFDlgOptions.SpinEdit4Change(Sender: TObject);
begin
  TrackBar4.Position:=SpinEdit4.Value;
end;

procedure TFDlgOptions.Button3Click(Sender: TObject);
begin
  AssociateFile;
  CheckAssociation;
end;

procedure TFDlgOptions.Button4Click(Sender: TObject);
begin
  DeassociateFile;
  CheckAssociation;
end;

procedure TFDlgOptions.SpinEdit1Change(Sender: TObject);
begin
  if SpinEdit1.Value = 0 then
    CheckBox3.Checked:=False
  else
    CheckBox3.Checked:=True;
end;

procedure TFDlgOptions.CheckBox3Click(Sender: TObject);
begin
  if CheckBox3.Checked then
    if SpinEdit1.Value = 0 then
      SpinEdit1.Value:=1;
end;

procedure TFDlgOptions.CheckBox4Click(Sender: TObject);
begin
  TrackBar2.Enabled:=CheckBox4.Checked;
  Label2.Enabled:=CheckBox4.Checked;
  Label6.Enabled:=CheckBox4.Checked;
end;

end.
