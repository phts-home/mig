unit uFDlgObjAddRegionPick;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TFDlgObjAddRegionPick = class(TForm)
    Image1: TImage;
    procedure FormShow(Sender: TObject);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    PickX, PickY: Integer;
    CancelPressed: Boolean;
  end;

var
  FDlgObjAddRegionPick: TFDlgObjAddRegionPick;

implementation

uses uFView, uFMain;

{$R *.dfm}

procedure TFDlgObjAddRegionPick.FormShow(Sender: TObject);
begin
  Image1.Picture:=nil;
  Image1.Width:=FView.Image1.Width;
  Image1.Height:=FView.Image1.Height;
  Image1.Picture:=FView.Image1.Picture;
  ClientWidth:=Image1.Width;
  ClientHeight:=Image1.Height;
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
  CancelPressed:=True;
end;

procedure TFDlgObjAddRegionPick.Image1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  PickX:=Round(X/FMain.Scale);
  PickY:=Round((Image1.Height - Y)/FMain.Scale);
  CancelPressed:=False;
  Close;
end;

procedure TFDlgObjAddRegionPick.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key=#27 then
    Close;
end;

end.
