object FPanelHistogr1: TFPanelHistogr1
  Left = 237
  Top = 184
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = #1043#1080#1089#1090#1086#1075#1088#1072#1084#1084#1099
  ClientHeight = 265
  ClientWidth = 486
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 5
    Top = 5
    Width = 476
    Height = 236
    ActivePage = TabSheet1
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = #1054#1090#1086#1073#1088#1072#1078#1077#1085#1080#1077
      object Chart1: TChart
        Left = 0
        Top = 0
        Width = 468
        Height = 208
        AllowPanning = pmNone
        AllowZoom = False
        BackWall.Brush.Color = clWhite
        BackWall.Color = clWhite
        Gradient.Direction = gdBottomTop
        MarginBottom = 3
        MarginTop = 3
        PrintProportional = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BackColor = clWhite
        BottomAxis.ExactDateTime = False
        BottomAxis.LogarithmicBase = 1
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.Maximum = 920.000000000000000000
        Legend.Visible = False
        RightAxis.Visible = False
        TopAxis.Visible = False
        View3D = False
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 0
        object Series1: TBarSeries
          Marks.ArrowLength = 20
          Marks.Clip = True
          Marks.Visible = False
          SeriesColor = clRed
          BarBrush.Color = clWhite
          BarWidthPercent = 30
          OffsetPercent = 100
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Bar'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
          CustomBarWidth = 15
        end
        object Series2: TBarSeries
          Active = False
          Marks.ArrowLength = 20
          Marks.Clip = True
          Marks.Visible = False
          SeriesColor = clBlue
          BarBrush.Color = clWhite
          BarWidthPercent = 30
          OffsetPercent = 100
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Bar'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
          CustomBarWidth = 15
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1075#1080#1089#1090#1086#1075#1088#1072#1084#1084
      ImageIndex = 1
      object GroupBox8: TGroupBox
        Left = 160
        Top = 15
        Width = 146
        Height = 181
        Caption = '     '
        TabOrder = 4
        object Label3: TLabel
          Left = 10
          Top = 147
          Width = 41
          Height = 13
          Caption = #1062#1074#1077#1090' #2'
        end
        object Label8: TLabel
          Left = 10
          Top = 127
          Width = 41
          Height = 13
          Caption = #1062#1074#1077#1090' #1'
        end
        object Label10: TLabel
          Left = 10
          Top = 107
          Width = 43
          Height = 13
          Caption = #1047#1072#1083#1080#1074#1082#1072
        end
        object Label11: TLabel
          Left = 10
          Top = 87
          Width = 37
          Height = 13
          Caption = #1060#1086#1088#1084#1072
        end
        object RadioButton3: TRadioButton
          Left = 10
          Top = 25
          Width = 126
          Height = 17
          Caption = #1042#1089#1077' '#1086#1073#1098#1077#1082#1090#1099
          TabOrder = 0
          OnClick = RadioButton3Click
        end
        object RadioButton4: TRadioButton
          Left = 10
          Top = 47
          Width = 126
          Height = 17
          Caption = #1050#1083#1072#1089#1089
          Checked = True
          TabOrder = 1
          TabStop = True
          OnClick = RadioButton4Click
        end
        object ComboBox6: TComboBox
          Left = 65
          Top = 85
          Width = 71
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          ItemIndex = 5
          TabOrder = 2
          Text = 'Rectangle'
          OnChange = ComboBox6Change
          Items.Strings = (
            'Arrow'
            'Cilinder'
            'Ellipse'
            'InvPyramid'
            'Pyramid'
            'Rectangle'
            'RectGradient')
        end
        object ComboBox7: TComboBox
          Left = 65
          Top = 105
          Width = 71
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          ItemIndex = 6
          TabOrder = 3
          Text = 'Solid'
          OnChange = ComboBox7Change
          Items.Strings = (
            'BDiagonal'
            'Clear'
            'Cross'
            'DiagCross'
            'FDiagonal'
            'Horizontal'
            'Solid'
            'Vertical')
        end
        object ColorBox4: TColorBox
          Left = 65
          Top = 125
          Width = 71
          Height = 18
          Selected = clBlue
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 4
          OnChange = ColorBox4Change
        end
        object ColorBox5: TColorBox
          Left = 65
          Top = 145
          Width = 71
          Height = 18
          Selected = clWhite
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 5
          OnChange = ColorBox5Change
        end
        object ComboBox8: TComboBox
          Left = 65
          Top = 47
          Width = 71
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          TabOrder = 6
          OnChange = ComboBox8Change
        end
      end
      object GroupBox2: TGroupBox
        Left = 5
        Top = 15
        Width = 146
        Height = 181
        Caption = '     '
        TabOrder = 3
        object Label7: TLabel
          Left = 10
          Top = 147
          Width = 41
          Height = 13
          Caption = #1062#1074#1077#1090' #2'
        end
        object Label1: TLabel
          Left = 10
          Top = 127
          Width = 41
          Height = 13
          Caption = #1062#1074#1077#1090' #1'
        end
        object Label6: TLabel
          Left = 10
          Top = 107
          Width = 43
          Height = 13
          Caption = #1047#1072#1083#1080#1074#1082#1072
        end
        object Label9: TLabel
          Left = 10
          Top = 87
          Width = 37
          Height = 13
          Caption = #1060#1086#1088#1084#1072
        end
        object RadioButton1: TRadioButton
          Left = 10
          Top = 25
          Width = 126
          Height = 17
          Caption = #1042#1089#1077' '#1086#1073#1098#1077#1082#1090#1099
          TabOrder = 0
          OnClick = RadioButton1Click
        end
        object RadioButton2: TRadioButton
          Left = 10
          Top = 47
          Width = 126
          Height = 17
          Caption = #1050#1083#1072#1089#1089
          Checked = True
          TabOrder = 1
          TabStop = True
          OnClick = RadioButton2Click
        end
        object ComboBox3: TComboBox
          Left = 65
          Top = 85
          Width = 71
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          ItemIndex = 5
          TabOrder = 2
          Text = 'Rectangle'
          OnChange = ComboBox3Change
          Items.Strings = (
            'Arrow'
            'Cilinder'
            'Ellipse'
            'InvPyramid'
            'Pyramid'
            'Rectangle'
            'RectGradient')
        end
        object ComboBox2: TComboBox
          Left = 65
          Top = 105
          Width = 71
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          ItemIndex = 6
          TabOrder = 3
          Text = 'Solid'
          OnChange = ComboBox2Change
          Items.Strings = (
            'BDiagonal'
            'Clear'
            'Cross'
            'DiagCross'
            'FDiagonal'
            'Horizontal'
            'Solid'
            'Vertical')
        end
        object ColorBox1: TColorBox
          Left = 65
          Top = 125
          Width = 71
          Height = 18
          Selected = clRed
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 4
          OnChange = ColorBox1Change
        end
        object ColorBox3: TColorBox
          Left = 65
          Top = 145
          Width = 71
          Height = 18
          Selected = clWhite
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 5
          OnChange = ColorBox3Change
        end
        object ComboBox1: TComboBox
          Left = 65
          Top = 47
          Width = 71
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          TabOrder = 6
          OnChange = ComboBox1Change
        end
      end
      object GroupBox1: TGroupBox
        Left = 315
        Top = 15
        Width = 146
        Height = 86
        Caption = ' '#1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '
        TabOrder = 0
        object Label2: TLabel
          Left = 75
          Top = 25
          Width = 61
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '100 '#1084#1089
        end
        object Label4: TLabel
          Left = 9
          Top = 25
          Width = 52
          Height = 13
          Caption = #1048#1085#1090#1077#1088#1074#1072#1083':'
        end
        object TrackBar1: TTrackBar
          Left = 3
          Top = 45
          Width = 140
          Height = 26
          Max = 20
          Min = 1
          Position = 1
          TabOrder = 0
          ThumbLength = 15
          OnChange = TrackBar1Change
        end
      end
      object CheckBox2: TCheckBox
        Left = 15
        Top = 13
        Width = 76
        Height = 17
        Caption = #1043#1088#1072#1092#1080#1082' #1'
        Checked = True
        State = cbChecked
        TabOrder = 1
        OnClick = CheckBox2Click
      end
      object CheckBox4: TCheckBox
        Left = 170
        Top = 13
        Width = 76
        Height = 17
        Caption = #1043#1088#1072#1092#1080#1082' #2'
        TabOrder = 2
        OnClick = CheckBox4Click
      end
      object RadioGroup1: TRadioGroup
        Left = 315
        Top = 110
        Width = 146
        Height = 86
        Caption = ' '#1042#1077#1083#1080#1095#1080#1085#1072' '
        ItemIndex = 0
        Items.Strings = (
          #1057#1082#1086#1088#1086#1089#1090#1100' ('#1084'/'#1089')'
          #1050#1080#1085'. '#1101#1085#1077#1088#1075#1080#1103' ('#1044#1078')')
        TabOrder = 5
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103
      ImageIndex = 2
      object GroupBox3: TGroupBox
        Left = 160
        Top = 15
        Width = 146
        Height = 86
        Caption = '        '
        TabOrder = 1
        object ClrBxX: TColorBox
          Left = 70
          Top = 45
          Width = 66
          Height = 18
          Selected = clGray
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 2
          OnChange = ClrBxXChange
        end
        object ChBxXGr: TCheckBox
          Left = 10
          Top = 25
          Width = 56
          Height = 17
          Caption = #1057#1077#1090#1082#1072
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = ChBxXGrClick
        end
        object ComboBox4: TComboBox
          Left = 70
          Top = 25
          Width = 66
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          ItemIndex = 4
          TabOrder = 1
          Text = 'Dot'
          OnChange = ComboBox4Change
          Items.Strings = (
            'Clear'
            'Dash'
            'DashDot'
            'DashDotDot'
            'Dot'
            'InsideFrame'
            'Solid')
        end
      end
      object ChBxXEn: TCheckBox
        Left = 170
        Top = 13
        Width = 53
        Height = 17
        Caption = #1054#1089#1100' X'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = ChBxXEnClick
      end
      object GroupBox6: TGroupBox
        Left = 160
        Top = 110
        Width = 146
        Height = 86
        Caption = '        '
        TabOrder = 5
        object ClrBxY: TColorBox
          Left = 70
          Top = 45
          Width = 66
          Height = 18
          Selected = clGray
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 2
          OnChange = ClrBxYChange
        end
        object ChBxYGr: TCheckBox
          Left = 10
          Top = 25
          Width = 56
          Height = 17
          Caption = #1057#1077#1090#1082#1072
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = ChBxYGrClick
        end
        object ComboBox5: TComboBox
          Left = 70
          Top = 25
          Width = 66
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          ItemIndex = 4
          TabOrder = 1
          Text = 'Dot'
          OnChange = ComboBox5Change
          Items.Strings = (
            'Clear'
            'Dash'
            'DashDot'
            'DashDotDot'
            'Dot'
            'InsideFrame'
            'Solid')
        end
      end
      object GroupBox5: TGroupBox
        Left = 5
        Top = 15
        Width = 146
        Height = 181
        Caption = ' '#1054#1092#1086#1088#1084#1083#1077#1085#1080#1077' '
        TabOrder = 3
        object Label5: TLabel
          Left = 10
          Top = 52
          Width = 26
          Height = 13
          Caption = #1060#1086#1085':'
        end
        object Label14: TLabel
          Left = 10
          Top = 77
          Width = 42
          Height = 13
          Caption = #1064#1080#1088#1080#1085#1072':'
        end
        object CheckBox3: TCheckBox
          Left = 10
          Top = 20
          Width = 61
          Height = 17
          Caption = '3D'
          TabOrder = 0
          OnClick = CheckBox3Click
        end
        object ColorBox2: TColorBox
          Left = 70
          Top = 50
          Width = 66
          Height = 18
          Selected = clWhite
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 2
          OnChange = ColorBox2Change
        end
        object CheckBox1: TCheckBox
          Left = 75
          Top = 20
          Width = 66
          Height = 17
          Caption = #1052#1077#1090#1082#1080
          TabOrder = 1
          OnClick = CheckBox1Click
        end
        object SpinEdit1: TSpinEdit
          Left = 70
          Top = 75
          Width = 66
          Height = 22
          EditorEnabled = False
          MaxValue = 50
          MinValue = 1
          TabOrder = 3
          Value = 15
          OnChange = SpinEdit1Change
        end
        object CheckBox5: TCheckBox
          Left = 10
          Top = 105
          Width = 126
          Height = 17
          Caption = #1052#1086#1085#1086#1093#1088#1086#1084#1085#1072#1103
          TabOrder = 4
          OnClick = CheckBox5Click
        end
      end
      object GroupBox4: TGroupBox
        Left = 315
        Top = 15
        Width = 146
        Height = 86
        Caption = ' '#1052#1072#1089#1096#1090#1072#1073' ('#1086#1089#1100' X) '
        TabOrder = 2
        object Label15: TLabel
          Left = 10
          Top = 65
          Width = 42
          Height = 13
          Caption = '1 '#1076#1077#1083'. = '
        end
        object Label16: TLabel
          Left = 10
          Top = 40
          Width = 31
          Height = 13
          Caption = 'max = '
        end
        object ChBxXAu: TCheckBox
          Left = 10
          Top = 20
          Width = 61
          Height = 17
          Caption = #1040#1074#1090#1086
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = ChBxXAuClick
        end
        object SpEdX: TSpinEdit
          Left = 70
          Top = 35
          Width = 66
          Height = 22
          EditorEnabled = False
          Increment = 5
          MaxValue = 2048
          MinValue = 0
          TabOrder = 1
          Value = 20
          OnChange = SpEdXChange
        end
        object EdUnit: TEdit
          Left = 70
          Top = 60
          Width = 66
          Height = 21
          TabOrder = 2
          Text = '100'
        end
      end
      object ChBxYEn: TCheckBox
        Left = 170
        Top = 108
        Width = 53
        Height = 17
        Caption = #1054#1089#1100' Y'
        Checked = True
        State = cbChecked
        TabOrder = 4
        OnClick = ChBxYEnClick
      end
      object GroupBox7: TGroupBox
        Left = 315
        Top = 110
        Width = 146
        Height = 86
        Caption = ' '#1052#1072#1089#1096#1090#1072#1073' ('#1086#1089#1100' Y) '
        TabOrder = 6
        object Label17: TLabel
          Left = 10
          Top = 40
          Width = 31
          Height = 13
          Caption = 'max = '
        end
        object SpEdY: TSpinEdit
          Left = 70
          Top = 35
          Width = 66
          Height = 22
          EditorEnabled = False
          Increment = 5
          MaxValue = 2048
          MinValue = 0
          TabOrder = 0
          Value = 100
          OnChange = SpEdYChange
        end
        object ChBxYAu: TCheckBox
          Left = 10
          Top = 20
          Width = 56
          Height = 17
          Caption = #1040#1074#1090#1086
          Checked = True
          State = cbChecked
          TabOrder = 1
          OnClick = ChBxYAuClick
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 5
    Top = 245
    Width = 476
    Height = 16
    BevelOuter = bvNone
    TabOrder = 0
    object SpeedButton2: TSpeedButton
      Left = 365
      Top = 0
      Width = 111
      Height = 16
      Caption = #1057#1086#1093#1088'. '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
      OnClick = SpeedButton2Click
    end
    object SpeedButton1: TSpeedButton
      Left = 170
      Top = 0
      Width = 71
      Height = 16
      Caption = #1057#1086#1093#1088'. '#1074' BMP'
      OnClick = SpeedButton1Click
    end
    object SpeedButton3: TSpeedButton
      Left = 250
      Top = 0
      Width = 111
      Height = 16
      Caption = #1047#1072#1075#1088'. '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
      OnClick = SpeedButton3Click
    end
  end
  object Panel1: TPanel
    Left = 435
    Top = 5
    Width = 46
    Height = 17
    BevelOuter = bvNone
    TabOrder = 2
    object ChBxEn: TCheckBox
      Left = 0
      Top = 0
      Width = 46
      Height = 17
      Caption = #1042#1082#1083
      TabOrder = 0
      OnClick = ChBxEnClick
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 20
    Top = 180
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'bmp'
    Filter = 'BMP Images (*.bmp)|*.bmp'
    Left = 49
    Top = 179
  end
end
