unit uFDlgObjAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Spin, Math;

type
  TFDlgObjAdd = class(TForm)
    Label1: TLabel;
    Label2: TLabel;        
    Label5: TLabel;
    Label6: TLabel;
    Label15: TLabel;
    EdSAngl: TEdit;
    EdSpeed: TEdit;
    Label16: TLabel;
    EdAcc: TEdit;
    EdAAngl: TEdit;
    Label7: TLabel;
    Label3: TLabel;
    Button1: TButton;
    Button2: TButton;
    EdName: TEdit;
    Label4: TLabel;
    Bevel1: TBevel;
    EdMass: TEdit;
    ColorBox1: TColorBox;
    Label8: TLabel;
    Edx0: TEdit;
    Edy0: TEdit;
    Button4: TButton;
    Label9: TLabel;
    ColorBox2: TColorBox;
    SpinEdit1: TSpinEdit;
    ComboBox1: TComboBox;
    Label10: TLabel;
    SpEditRad: TSpinEdit;
    procedure Button4Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdNameClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }        
  end;

var
  FDlgObjAdd: TFDlgObjAdd;

implementation

uses uFMain, uFPanelObjects;

{$R *.dfm}

procedure TFDlgObjAdd.Button4Click(Sender: TObject);
begin
  FMain.AddObject(ComboBox1.ItemIndex+1,EdName.Text, StrToFloat(EdSpeed.Text),
    DegToRad(StrToFloat(EdSAngl.Text)), StrToFloat(EdAcc.Text),
    DegToRad(StrToFloat(EdAAngl.Text)), SpEditRad.Value,
    StrToFloat(EdMass.Text), StrToFloat(Edx0.Text), StrToFloat(Edy0.Text),
    ColorBox1.Selected, ColorBox2.Selected, SpinEdit1.Value);
  FMain.ReloadObjectList;
  FMain.RedrawImage;
end;

procedure TFDlgObjAdd.ComboBox1Change(Sender: TObject);
begin
  with FMain.Project.ProjClasses[ComboBox1.ItemIndex+1] do
  begin
    EdName.Text:=ObjName;
    if Val1 = 0 then
      EdSpeed.Text:=FloatToStr(V0)
    else
      EdSpeed.Text:=FloatToStr((Random(Int2*10000-Int1*10000)+Int1*10000)/10000);

    if Val2 = 0 then
      EdSAngl.Text:=FloatToStr(RadToDeg(AngleV))
    else
      EdSAngl.Text:=FloatToStr((Random(Int4*10000-Int3*10000)+Int3*10000)/10000);

    if Val3 = 0 then
      EdAcc.Text:=FloatToStr(F0)
    else
      EdAcc.Text:=FloatToStr((Random(Int6*10000-Int5*10000)+Int5*10000)/10000);

    if Val4 = 0 then
      EdAAngl.Text:=FloatToStr(RadToDeg(AngleF))
    else
      EdAAngl.Text:=FloatToStr((Random(Int8*10000-Int7*10000)+Int7*10000)/10000);

    if Val5 = 0 then
      SpEditRad.Value:=Radius
    else
      SpEditRad.Value:=Random(Int10-Int9+1)+Int9;

    if Val6 = 0 then
      EdMass.Text:=FloatToStr(Mass)
    else
      EdMass.Text:=FloatToStr((Random(Int12*10000-Int11*10000)+Int11*10000)/10000);

    if not Ch1 then
      ColorBox1.Selected:=FillColor
    else
      ColorBox1.Selected:=ColorList[Random(16)];

    if not Ch2 then
      ColorBox2.Selected:=BorderColor
    else
      ColorBox2.Selected:=ColorList[Random(16)];

    if not Ch3 then
      SpinEdit1.Value:=BorderWidth
    else
      SpinEdit1.Value:=Random(Int14-Int13)+Int13;
  end;
end;

procedure TFDlgObjAdd.FormShow(Sender: TObject);
begin
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
  EdName.SetFocus;
end;

procedure TFDlgObjAdd.EdNameClick(Sender: TObject);
begin
  (Sender as TEdit).SelectAll;
end;

end.
