unit uFMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Spin, Math, Buttons, Menus, IniFiles,
  ComCtrls, ToolWin, ImgList, ActnList, Jpeg, ShellAPI;

const
  ColorList: Array [0..15] of Integer = (clBlack, clMaroon, clGreen, clOlive,
      clNavy, clPurple, clTeal, clGray, clSilver, clRed, clLime, clYellow,
      clBlue, clFuchsia, clAqua, clWhite);

type
  TClassOfObjects = Object
    ClassID: Integer;
    ClassName1: String[32];
    ObjName: String[28];
    AngleV, AngleF, Mass, V0, F0: Real;
    Radius: Integer;
    FillColor, BorderColor: TColor;
    BorderWidth: Integer;
    Val1, Val2, Val3, Val4, Val5, Val6: Integer;
    Int1, Int2, Int3, Int4, Int5, Int6, Int7, Int8, Int9, Int10, Int11, Int12,
      Int13, Int14: Integer;
    Ch1, Ch2, Ch3: Boolean;
  end;
{
  TPhysObject = Record
    ObjectID, ClassID: Integer;
    PartName: String[29];
    Name: String[32];
    AngleV, AngleF, V, Vx, Vy, F, Fx, Fy, Mass, V0, F0, x, y, Radius, x0,
      y0: Real;
    FillColor, BorderColor: TColor;
    BorderWidth: Integer;
  end;
}
  TPhysicalObject = Object
    ObjectID, ClassID: Integer;
    PartName: String[28];
    Name: String[32];
    AngleV, AngleF, V, Vx, Vy, F, Fx, Fy, Mass, V0, F0, x, y, x0,
      y0: Real;
    Radius: Integer;
    FillColor, BorderColor: TColor;
    BorderWidth: Integer;
//    Hits: Set of Byte;
//    procedure Move;
//    procedure CalcSpeeds(k: Integer);
//    procedure CheckCollisionWithBorder;
//    procedure CheckCollisionWithBall;//(k: Integer);
//    procedure RevertXDirection;
//    procedure RevertYDirection;
  end;

  TProject = Record
    ProjObjectsCount, ProjClassesCount: Integer;
    ProjActiveClass: Integer;
    ProjObjects: Array[1..2048] of TPhysicalObject;
    ProjClasses: Array[1..32] of TClassOfObjects;
    ProjWidth, ProjHeight: Integer;
    ProjBackgr: TColor;
    ProjDiscription: String[255];
    ProjScale: Real;
  end;

  TFMain = class(TForm)
    TimerMain: TTimer;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    ItemClose: TMenuItem;
    N3: TMenuItem;
    ItemAbout: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    ItemAddObj: TMenuItem;
    ItemDelObj: TMenuItem;
    ItemEditObj: TMenuItem;
    ItemPOptions: TMenuItem;
    ItemStart: TMenuItem;
    ItemStop: TMenuItem;
    ItemPause: TMenuItem;
    N14: TMenuItem;
    ItemShowObjList: TMenuItem;
    ItemNew: TMenuItem;
    ItemOpen: TMenuItem;
    ItemSave: TMenuItem;
    ItemSaveAs: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    ItemQAdd: TMenuItem;
    N8: TMenuItem;
    ItemDelAllObj: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    ItemAddObjFromFile: TMenuItem;
    ItemShowRes: TMenuItem;
    TimerView: TTimer;
    N13: TMenuItem;
    N15: TMenuItem;
    ImageList1: TImageList;
    ItemShowProp: TMenuItem;
    ItemDublicateObj: TMenuItem;
    N16: TMenuItem;
    StatusBar1: TStatusBar;
    CoolBar1: TCoolBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton21: TToolButton;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton20: TToolButton;
    ToolButton19: TToolButton;
    N10: TMenuItem;
    ItemAddClass: TMenuItem;
    ItemDelClass: TMenuItem;
    ItemDublicateClass: TMenuItem;
    ItemEditClass: TMenuItem;
    N20: TMenuItem;
    ItemShowClasses: TMenuItem;
    temAddClassFromFile: TMenuItem;
    ToolButton24: TToolButton;
    N4: TMenuItem;
    N9: TMenuItem;
    N18: TMenuItem;
    ItemDelAllObjOfActiveClass: TMenuItem;
    N19: TMenuItem;
    N21: TMenuItem;
    ItemDrawNames: TMenuItem;
    N2: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    N24: TMenuItem;
    ItemQSel: TMenuItem;
    ToolButton7: TToolButton;
    ActionList1: TActionList;
    aAddClass: TAction;
    aDelClass: TAction;
    aEditClass: TAction;
    aDublClass: TAction;
    aDelObjectsOfClass: TAction;
    aAddObject: TAction;
    aDelObject: TAction;
    aDelAllObjects: TAction;
    aEditObject: TAction;
    aDublObject: TAction;
    aOptions: TAction;
    aStart: TAction;
    aStop: TAction;
    aPause: TAction;
    aProjOptions: TAction;
    aNew: TAction;
    aOpen: TAction;
    aSave: TAction;
    aSaveAs: TAction;
    aExit: TAction;
    aAbout: TAction;
    aShowObjects: TAction;
    aShowClasses: TAction;
    aShowProp: TAction;
    aShowRes: TAction;
    aHideAllPanels: TAction;
    aEnQSel: TAction;
    aEnQAdd: TAction;
    aDrawNames: TAction;
    aDrawSel: TAction;
    aRefresh: TAction;
    aAddObjectFromFile: TAction;
    aAddClassFromFile: TAction;
    N25: TMenuItem;
    aShowGraph: TAction;
    ToolButton9: TToolButton;
    ODO1: TMenuItem;
    N26: TMenuItem;
    ItemExport: TMenuItem;
    N28: TMenuItem;
    ItemExportBMP: TMenuItem;
    ItemExportJPEG: TMenuItem;
    aExportBMP: TAction;
    aExportJPEG: TAction;
    aContents: TAction;
    SvDlgExp: TSaveDialog;
    N27: TMenuItem;
    aShowHistogr1: TAction;
    N29: TMenuItem;
    aSetDefPos: TAction;
    ToolButton6: TToolButton;
    ToolButton18: TToolButton;
    N30: TMenuItem;
    N31: TMenuItem;
    aSelNothing: TAction;
    ItemHistory: TMenuItem;
    aSetDefSize: TAction;
    N32: TMenuItem;
    N33: TMenuItem;
    aAddObjectInRegion: TAction;
    OpnDlgAddObjCl: TOpenDialog;
    N34: TMenuItem;
    N1001: TMenuItem;
    N501: TMenuItem;
    N2001: TMenuItem;
    N4001: TMenuItem;
    aScale50: TAction;
    aScale100: TAction;
    aScale200: TAction;
    aScale400: TAction;
    N35: TMenuItem;
    N36: TMenuItem;
    N37: TMenuItem;
    aScaleIn: TAction;
    aScaleOut: TAction;
    N1501: TMenuItem;
    aScale150: TAction;
    ToolButton2: TToolButton;
    PopupMenu1: TPopupMenu;
    ObjectList: TListView;
    aStepForward: TAction;
    N7: TMenuItem;
    N38: TMenuItem;
    N39: TMenuItem;
    N17: TMenuItem;
    procedure TimerMainTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerViewTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure aStartExecute(Sender: TObject);
    procedure aPauseExecute(Sender: TObject);
    procedure aStopExecute(Sender: TObject);
    procedure aProjOptionsExecute(Sender: TObject);
    procedure aOptionsExecute(Sender: TObject);
    procedure aNewExecute(Sender: TObject);
    procedure aOpenExecute(Sender: TObject);
    procedure aSaveExecute(Sender: TObject);
    procedure aSaveAsExecute(Sender: TObject);
    procedure aExitExecute(Sender: TObject);
    procedure aAddClassExecute(Sender: TObject);
    procedure aDublClassExecute(Sender: TObject);
    procedure aEditClassExecute(Sender: TObject);
    procedure aDelClassExecute(Sender: TObject);
    procedure aDelObjectsOfClassExecute(Sender: TObject);
    procedure aAddObjectExecute(Sender: TObject);
    procedure aDublObjectExecute(Sender: TObject);
    procedure aEditObjectExecute(Sender: TObject);
    procedure aDelObjectExecute(Sender: TObject);
    procedure aDelAllObjectsExecute(Sender: TObject);
    procedure aAboutExecute(Sender: TObject);
    procedure aShowClassesExecute(Sender: TObject);
    procedure aShowObjectsExecute(Sender: TObject);
    procedure aShowPropExecute(Sender: TObject);
    procedure aShowResExecute(Sender: TObject);
    procedure aHideAllPanelsExecute(Sender: TObject);
    procedure aEnQSelExecute(Sender: TObject);
    procedure aEnQAddExecute(Sender: TObject);
    procedure aDrawNamesExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aDrawSelExecute(Sender: TObject);
    procedure aAddObjectFromFileExecute(Sender: TObject);
    procedure aAddClassFromFileExecute(Sender: TObject);
    procedure aShowGraphExecute(Sender: TObject);
    procedure aExportBMPExecute(Sender: TObject);
    procedure aExportJPEGExecute(Sender: TObject);
    procedure aContentsExecute(Sender: TObject);
    procedure aShowHistogr1Execute(Sender: TObject);
    procedure aSetDefPosExecute(Sender: TObject);
    procedure aSelNothingExecute(Sender: TObject);
    procedure aSetDefSizeExecute(Sender: TObject);
    procedure aAddObjectInRegionExecute(Sender: TObject);
    procedure aScale50Execute(Sender: TObject);
    procedure aScale100Execute(Sender: TObject);
    procedure aScale200Execute(Sender: TObject);
    procedure aScaleInExecute(Sender: TObject);
    procedure aScaleOutExecute(Sender: TObject);
    procedure aScale400Execute(Sender: TObject);
    procedure aScale150Execute(Sender: TObject);
    procedure aStepForwardExecute(Sender: TObject);
  private
    { Private declarations }
  public
    cfg: TMemIniFile;
    Project, DefaultProject, TempProject: TProject;
    DefaultClass: TClassOfObjects;
    ProjectFile, DefaultProjFile, TempProjectFile: File of TProject;
    DefaultClassFile: File of TClassOfObjects;
    DefaultObjFile: File of TPhysicalObject;
    CurrentPath, CurrentFileName: String;
    Changed, StopWhenChanging, EnableVisualization, Pause: Boolean;
    ActiveObject: Integer;
    Visibility: Array [1..7] of Boolean;
    Scale: Real;

    procedure RedrawImage;
    procedure DrawRect;
    function AddClass(FClassName, FObjName: String; FSpeed, FSAngle, FForce,
      FFAngle: Real;
      FRadius: Integer;
      FMass: Real; FFillColor, FBorderColor: TColor;
      FBorderWidth: Integer; FVal1, FVal2, FVal3, FVal4, FVal5, FVal6, FInt1,
      FInt2, FInt3, FInt4, FInt5, FInt6, FInt7, FInt8, FInt9, FInt10, FInt11,
      FInt12, FInt13, FInt14: Integer; FCh1, FCh2, FCh3: Boolean):Integer;
    procedure ModifyClass(FClassName, FObjName: String; FSpeed, FSAngle, FForce,
      FFAngle: Real;
      FRadius: Integer;
      FMass: Real; FFillColor, FBorderColor: TColor;
      FBorderWidth: Integer; FVal1, FVal2, FVal3, FVal4, FVal5, FVal6, FInt1,
      FInt2, FInt3, FInt4, FInt5, FInt6, FInt7, FInt8, FInt9, FInt10, FInt11,
      FInt12, FInt13, FInt14: Integer; FCh1, FCh2, FCh3: Boolean);
    procedure DelClass(Index: Integer);
    procedure ChangeObjClass(ObjIndex, ClassIndex: Integer);
    procedure AddObject(FClassID: Integer; FPartName: String; FSpeed, FSAngle,
      FForce, FFAngle: Real;
      FRadius: Integer;
      FMass, Fx0, Fy0: Real;
      FFillColor, FBorderColor: TColor; FBorderWidth: Integer);
    procedure ModifyObject(Index, FClassID: Integer; FPartName: String;
      FSpeed, FSAngle, FForce, FFAngle: Real;
      FRadius: Integer;
      FMass, Fx0, Fy0, Fxx, Fyy: Real;
      FFillColor, FBorderColor: TColor; FBorderWidth: Integer);
    procedure DelObject(Index: Integer);
    procedure QuickAddObject(CurX, CurY: Integer);
    procedure ResetCoords;
    procedure SaveProject(Path: String);
    procedure LoadProject(Path: String);
    procedure SaveDefaultClass;
    procedure LoadDefaultClass;
    procedure SaveDefaultProject;
    procedure LoadDefaultProject;
    procedure ApplyProjectParam;
    procedure ApplyProgramParam;
    procedure CreateNewProject;
    procedure ReloadObjectList;
    procedure ReloadClassesList;
    procedure StartAnimation;
    procedure StopAnimation;

    procedure AddToHistory(ItemCaption: String);
    procedure ExecuteHistoryItem(Sender: TObject);
    procedure SaveHistory;
    procedure LoadHistory;
  end;

var
  FMain: TFMain;

implementation

uses uFDlgObjAdd, uFPanelObjects, uFPanelRes, uFDlgOptions, uFDlgAbout,
  uFDlgProjOpt, uFDlgProjCreate, uFDlgObjEdit, uFPanelProp, uFView,
  uFPanelClasses, uFDlgClassAdd, uFDlgClassEdit, uFPanelGraph,
  uFPanelHistogr1, uFDlgJPEG, uFDlgObjAddRegion;

{$R *.dfm}
{$R winxpstyle.res}

{-----------------------------------------------------------------------------}
{  procedure     RedrawImage                                                  }
{                                                                             }
{  discription   ����������� ��������                                         }
{-----------------------------------------------------------------------------}
{-----------------------------------------------------------------------------}
{                                      TODO                                   }
{-----------------------------------------------------------------------------}
procedure TFMain.RedrawImage;
var
  i: Integer;
begin
  FView.Image1.Canvas.Brush.Style:=bsSolid;
  FView.Image1.Canvas.Brush.Color:=Project.ProjBackgr;
  FView.Image1.Canvas.Pen.Color:=Project.ProjBackgr;
  FView.Image1.Canvas.Rectangle(0,0,FView.Image1.Width,FView.Image1.Height);
  for i:=1 to Project.ProjObjectsCount do
    with Project.ProjObjects[i] do
    begin
      FView.Image1.Canvas.Brush.Color:=FillColor;
      FView.Image1.Canvas.Pen.Color:=BorderColor;
      FView.Image1.Canvas.Pen.Width:=BorderWidth;
      FView.Image1.Canvas.Ellipse(Round((x*Project.ProjScale-Radius)*Scale),
        FView.Image1.Height-Round((y*Project.ProjScale+Radius)*Scale),
        Round((x*Project.ProjScale+Radius)*Scale),
        FView.Image1.Height-Round((y*Project.ProjScale-Radius)*Scale));
      if aDrawNames.Checked then
      begin
        FView.Image1.Canvas.Brush.Color:=Project.ProjBackgr;
        FView.Image1.Canvas.TextOut(Round(x*Project.ProjScale * Scale),
          FView.Image1.Height-Round(y*Project.ProjScale * Scale),Name);
      end;
    end;
  if aDrawSel.Checked and(ActiveObject<>0)then DrawRect;
end;

{-----------------------------------------------------------------------------}
{  procedure     DrawRect                                                     }
{                                                                             }
{  discription   ���������� ����� ���������                                   }
{-----------------------------------------------------------------------------}
procedure TFMain.DrawRect;
begin
  FView.Image1.Canvas.Pen.Width:=1;
  FView.Image1.Canvas.Pen.Color:=clSilver;
  FView.Image1.Canvas.Brush.Style:=bsClear;
  with Project.ProjObjects[ActiveObject] do
    FView.Image1.Canvas.Rectangle(Round(Scale*(x*Project.ProjScale-Radius))-5,
      FView.Image1.Height - Round(Scale*(y*Project.ProjScale-Radius))+5,
      Round(Scale*(x*Project.ProjScale+Radius))+5,
      FView.Image1.Height - Round(Scale*(y*Project.ProjScale+Radius))-5);
end;

{-----------------------------------------------------------------------------}
{  function      AddClass                                                     }
{                                                                             }
{  discription   �������� ������                                              }
{-----------------------------------------------------------------------------}
function TFMain.AddClass(FClassName, FObjName: String; FSpeed, FSAngle, FForce,
      FFAngle: Real;
      FRadius: Integer;
      FMass: Real; FFillColor, FBorderColor: TColor;
      FBorderWidth: Integer; FVal1, FVal2, FVal3, FVal4, FVal5, FVal6, FInt1,
      FInt2, FInt3, FInt4, FInt5, FInt6, FInt7, FInt8, FInt9, FInt10, FInt11,
      FInt12, FInt13, FInt14: Integer; FCh1, FCh2, FCh3: Boolean):Integer;
var
  S: String;
  i, j, Count: Integer;
  Ex: Boolean;
begin
  Inc(Project.ProjClassesCount);
  with Project.ProjClasses[Project.ProjClassesCount] do
  begin
    ClassID:=Project.ProjClassesCount;
    ObjName:=FObjName;
    ClassName1:=FClassName;
    V0:=FSpeed;
    AngleV:=FSAngle;
    F0:=FForce;
    AngleF:=FFAngle;
    Radius:=FRadius;
    if FMass = 0 then FMass:=1;
    Mass:=FMass;
    FillColor:=FFillColor;
    BorderColor:=FBorderColor;
    BorderWidth:=FBorderWidth;
    Val1:=FVal1;
    Val2:=FVal2;
    Val3:=FVal3;
    Val4:=FVal4;
    Val5:=FVal5;
    Val6:=FVal6;
    Int1:=FInt1;
    Int2:=FInt2;
    Int3:=FInt3;
    Int4:=FInt4;
    Int5:=FInt5;
    Int6:=FInt6;
    Int7:=FInt7;
    Int8:=FInt8;
    Int9:=FInt9;
    Int10:=FInt10;
    Int11:=FInt11;
    Int12:=FInt12;
    Int13:=FInt13;
    Int14:=FInt14;
    Ch1:=FCh1;
    Ch2:=FCh2;
    Ch3:=FCh3;
    Result:=ClassID;
  end;
  Changed:=True;
  ReloadClassesList;
  RedrawImage;
end;

{-----------------------------------------------------------------------------}
{  procedure     ModifyClass                                                  }
{                                                                             }
{  discription   ��������� ������                                             }
{-----------------------------------------------------------------------------}
procedure TFMain.ModifyClass(FClassName, FObjName: String; FSpeed, FSAngle,
      FForce, FFAngle: Real;
      FRadius: Integer;
      FMass: Real; FFillColor, FBorderColor: TColor;
      FBorderWidth: Integer; FVal1, FVal2, FVal3, FVal4, FVal5, FVal6, FInt1,
      FInt2, FInt3, FInt4, FInt5, FInt6, FInt7, FInt8, FInt9, FInt10, FInt11,
      FInt12, FInt13, FInt14: Integer; FCh1, FCh2, FCh3: Boolean);
var
  i: Integer;
  QV, QF, QAnglF, QAnglV, QM: Real;
  QR: Integer;
  QFC, QBC: TColor;
  QBW: Integer;
begin
  with Project.ProjClasses[Project.ProjActiveClass] do
  begin
    ClassName1:=FClassName;
    ObjName:=FObjName;
    V0:=FSpeed;
    AngleV:=FSAngle;
    F0:=FForce;
    AngleF:=FFAngle;
    Radius:=FRadius;
    Mass:=FMass;
    if Mass = 0 then Mass:=1;
    FillColor:=FFillColor;
    BorderColor:=FBorderColor;
    BorderWidth:=FBorderWidth;
    Val1:=FVal1;
    Val2:=FVal2;
    Val3:=FVal3;
    Val4:=FVal4;
    Val5:=FVal5;
    Val6:=FVal6;
    Int1:=FInt1;
    Int2:=FInt2;
    Int3:=FInt3;
    Int4:=FInt4;
    Int5:=FInt5;
    Int6:=FInt6;
    Int7:=FInt7;
    Int8:=FInt8;
    Int9:=FInt9;
    Int10:=FInt10;
    Int11:=FInt11;
    Int12:=FInt12;
    Int13:=FInt13;
    Int14:=FInt14;
    Ch1:=FCh1;
    Ch2:=FCh2;
    Ch3:=FCh3;
    for i:=1 to Project.ProjObjectsCount do
    begin
      if Project.ProjObjects[i].ClassID =
        Project.ProjClasses[Project.ProjActiveClass].ClassID then
      begin
        if Val1 = 0 then
          QV:=V0
        else
          QV:=(Random(Int2*10000-Int1*10000)+Int1*10000)/10000;
        if Val2 = 0 then
          QAnglV:=AngleV
        else
          QAnglV:=DegToRad((Random(Int4*10000-Int3*10000)+Int3*10000)/10000);
        if Val3 = 0 then
          QF:=F0
        else
          QF:=(Random(Int6*10000-Int5*10000)+Int5*10000)/10000;
        if Val4 = 0 then
          QAnglF:=AngleF
        else
          QAnglF:=DegToRad((Random(Int8*10000-Int7*10000)+Int7*10000)/10000);
        if Val5 = 0 then
          QR:=Radius
        else
          QR:=Random(Int10-Int9+1)+Int9;
        if Val6 = 0 then
          QM:=Mass
        else
          QM:=(Random(Int12*10000-Int11*10000)+Int11*10000)/10000;
        if not Ch1 then
          QFC:=FillColor
        else
          QFC:=ColorList[Random(16)];
        if not Ch2 then
          QBC:=BorderColor
        else
          QBC:=ColorList[Random(16)];
        if not Ch3 then
          QBW:=BorderWidth
        else
          QBW:=Random(Int14-Int13)+Int13;
        ModifyObject(i,ClassID,ObjName,QV,QAnglV,QF,
          QAnglF,QR,QM,Project.ProjObjects[i].x0,Project.ProjObjects[i].y0,
          Project.ProjObjects[i].x,Project.ProjObjects[i].y,QFC,QBC,QBW);
      end;
    end;
    Changed:=True;
    ReloadClassesList;
    ReloadObjectList;
    RedrawImage;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     DelClass                                                     }
{                                                                             }
{  discription   �������� ������                                              }
{-----------------------------------------------------------------------------}
procedure TFMain.DelClass(Index: Integer);
var
  i, j: Integer;
begin
  for i:=Project.ProjObjectsCount downto 1 do
    if Project.ProjObjects[i].ClassID = Index then
      DelObject(i);
  for i:=Index to Project.ProjClassesCount-1 do
  begin
    Project.ProjClasses[i]:=Project.ProjClasses[i+1];
    Project.ProjClasses[i].ClassID:=i;
    for j:=1 to Project.ProjObjectsCount do
      if Project.ProjObjects[j].ClassID = i+1 then
        Project.ProjObjects[j].ClassID:=i;
  end;
  Dec(Project.ProjClassesCount);
  Changed:=True;
  ReloadClassesList;
  ReloadObjectList;
  if Project.ProjObjectsCount = 0 then
  begin
    ActiveObject:=0;
    FPanelProp.UnloadProperties;
  end;
  RedrawImage;
end;

{-----------------------------------------------------------------------------}
{  procedure     ChangeObjClass                                               }
{                                                                             }
{  discription   ����� ������ �������                                         }
{-----------------------------------------------------------------------------}
procedure TFMain.ChangeObjClass(ObjIndex, ClassIndex: Integer);
var
  QV, QF, QAnglF, QAnglV, QM: Real;
  QR: Integer;
  QFC, QBC: TColor;
  QBW: Integer;
begin
  with Project.ProjClasses[ClassIndex] do
  begin
    if Val1 = 0 then
      QV:=V0
    else
      QV:=(Random(Int2*10000-Int1*10000)+Int1*10000)/10000;

    if Val2 = 0 then
      QAnglV:=AngleV
    else
      QAnglV:=DegToRad((Random(Int4*10000-Int3*10000)+Int3*10000)/10000);

    if Val3 = 0 then
      QF:=F0
    else
      QF:=(Random(Int6*10000-Int5*10000)+Int5*10000)/10000;

    if Val4 = 0 then
      QAnglF:=AngleF
    else
      QAnglF:=DegToRad((Random(Int8*10000-Int7*10000)+Int7*10000)/10000);

    if Val5 = 0 then
      QR:=Radius
    else
      QR:=Random(Int10-Int9+1)+Int9;

    if Val6 = 0 then
      QM:=Mass
    else
      QM:=(Random(Int12*10000-Int11*10000)+Int11*10000)/10000;

    if not Ch1 then
      QFC:=FillColor
    else
      QFC:=ColorList[Random(16)];

    if not Ch2 then
      QBC:=BorderColor
    else
      QBC:=ColorList[Random(16)];

    if not Ch3 then
      QBW:=BorderWidth
    else
      QBW:=Random(Int14-Int13)+Int13;
  end;
  ModifyObject(ObjIndex,ClassIndex,Project.ProjClasses[ClassIndex].ObjName,
    QV,QAnglV,QF,QAnglF,QR,QM,Project.ProjObjects[ObjIndex].x0,
    Project.ProjObjects[ObjIndex].y0,Project.ProjObjects[ObjIndex].x,
    Project.ProjObjects[ObjIndex].y,QFC,QBC,QBW);
  Changed:=True;
end;

{-----------------------------------------------------------------------------}
{  procedure     AddObject                                                    }
{                                                                             }
{  discription   ���������� �������                                           }
{-----------------------------------------------------------------------------}
procedure TFMain.AddObject(FClassID: Integer; FPartName: String; FSpeed,
      FSAngle, FForce, FFAngle: Real;
      FRadius: Integer;
      FMass, Fx0, Fy0: Real;
      FFillColor, FBorderColor: TColor; FBorderWidth: Integer);
var
  S: String;
  i, ii, Count: Integer;
  Ex: Boolean;
begin
  Inc(Project.ProjObjectsCount);
  with Project.ProjObjects[Project.ProjObjectsCount] do
  begin
    ClassID:=FClassID;
    ObjectID:=Project.ProjObjectsCount;
    PartName:=FPartName;

    for ii:=1 to 2048 do
    begin
      Ex:=False;
      Count:=ii;
      for i:=1 to Project.ProjObjectsCount-1 do
        if(PartName = Project.ProjObjects[i].PartName)
          and(Count = StrToInt(Copy(Project.ProjObjects[i].Name,
          Length(Project.ProjObjects[i].Name)-3,4)))then
        begin
          Ex:=True;
          Break;
        end;
      if not Ex then Break;
    end;
    S:=IntToStr(Count);
    case Length(S) of
      1: S:='000'+S;
      2: S:='00'+S;
      3: S:='0'+S;
    end;

    Name:=PartName+S;
    V0:=FSpeed;
    AngleV:=FSAngle;
    F0:=FForce;
    AngleF:=FFAngle;
    if FMass = 0 then FMass:=1;
    Mass:=FMass;
    Radius:=FRadius;
    V:=V0;
    F:=F0;
    x0:=Fx0;
    y0:=Fy0;
    FillColor:=FFillColor;
    BorderColor:=FBorderColor;
    BorderWidth:=FBorderWidth;

    Vx:=V*cos(AngleV);
    Vy:=V*sin(AngleV);
    Fx:=F*cos(AngleF);
    Fy:=F*sin(AngleF);

    x:=x0;
    y:=y0;
  end;
  Changed:=True;
end;

{-----------------------------------------------------------------------------}
{  procedure     ModifyObject                                                 }
{                                                                             }
{  discription   ��������� �������                                            }
{-----------------------------------------------------------------------------}
procedure TFMain.ModifyObject(Index, FClassID: Integer; FPartName: String;
      FSpeed, FSAngle, FForce, FFAngle: Real;
      FRadius: Integer;
      FMass, Fx0, Fy0, Fxx, Fyy: Real;
      FFillColor, FBorderColor: TColor;
      FBorderWidth: Integer);
var
  S: String;
  i, ii, Count: Integer;
  Ex: Boolean;
begin
  if StopWhenChanging then aStop.Execute;
  with Project.ProjObjects[Index] do
  begin
    if FPartName<>Project.ProjObjects[Index].PartName then
    begin
      PartName:=FPartName;
      for ii:=1 to 2048 do
      begin
        Ex:=False;
        Count:=ii;
        for i:=1 to Project.ProjObjectsCount do
          if (i<>Index)and(PartName = Project.ProjObjects[i].PartName)
            and(Count = StrToInt(Copy(Project.ProjObjects[i].Name,
            Length(Project.ProjObjects[i].Name)-3,4)))then
          begin
            Ex:=True;
            Break;
          end;
        if not Ex then Break;
      end;
      S:=IntToStr(Count);
      case Length(S) of
        1: S:='000'+S;
        2: S:='00'+S;
        3: S:='0'+S;
      end;
      Name:=PartName+S;
    end;

    ClassID:=FClassID;
    V0:=FSpeed;
    AngleV:=FSAngle;
    F0:=FForce;
    AngleF:=FFAngle;
    if FMass = 0 then FMass:=1;
    Mass:=FMass;
    Radius:=FRadius;
    V:=V0;
    F:=F0;
    x0:=Fx0;
    y0:=Fy0;
    FillColor:=FFillColor;
    BorderColor:=FBorderColor;
    BorderWidth:=FBorderWidth;

    Vx:=V*cos(AngleV);
    Vy:=V*sin(AngleV);
    Fx:=F*cos(AngleF);
    Fy:=F*sin(AngleF);

    x:=Fxx;
    y:=Fyy;
  end;
  Changed:=True;
  RedrawImage;
end;

{-----------------------------------------------------------------------------}
{  procedure     DelObject                                                    }
{                                                                             }
{  discription   �������� �������                                             }
{-----------------------------------------------------------------------------}
procedure TFMain.DelObject(Index: Integer);
var
  i: Integer;
begin
  for i:=Index to Project.ProjObjectsCount-1 do
  begin
    Project.ProjObjects[i]:=Project.ProjObjects[i+1];
    Project.ProjObjects[i].ObjectID:=i;
  end;
  Dec(Project.ProjObjectsCount);
  Changed:=True;
end;

{-----------------------------------------------------------------------------}
{  procedure     QuickAddObject                                               }
{                                                                             }
{  discription   ������� ���������� �������                                   }
{-----------------------------------------------------------------------------}
procedure TFMain.QuickAddObject(CurX, CurY: Integer);
var
  QN: String;
  QV, QF, QAnglF, QAnglV, QM: Real;
  QR: Integer;
  QFC, QBC: TColor;
  QBW: Integer;
begin
  if StopWhenChanging then aStop.Execute;
  with Project.ProjClasses[Project.ProjActiveClass] do
  begin
    QN:=ObjName;
    if Val1 = 0 then
      QV:=V0
    else
      QV:=(Random(Int2*10000-Int1*10000)+Int1*10000)/10000;

    if Val2 = 0 then
      QAnglV:=AngleV
    else
      QAnglV:=DegToRad((Random(Int4*10000-Int3*10000)+Int3*10000)/10000);

    if Val3 = 0 then
      QF:=F0
    else
      QF:=(Random(Int6*10000-Int5*10000)+Int5*10000)/10000;

    if Val4 = 0 then
      QAnglF:=AngleF
    else
      QAnglF:=DegToRad((Random(Int8*10000-Int7*10000)+Int7*10000)/10000);

    if Val5 = 0 then
      QR:=Radius
    else
      QR:=Random(Int10-Int9+1)+Int9;

    if Val6 = 0 then
      QM:=Mass
    else
      QM:=(Random(Int12*10000-Int11*10000)+Int11*10000)/10000;

    if not Ch1 then
      QFC:=FillColor
    else
      QFC:=ColorList[Random(16)];

    if not Ch2 then
      QBC:=BorderColor
    else
      QBC:=ColorList[Random(16)];

    if not Ch3 then
      QBW:=BorderWidth
    else
      QBW:=Random(Int14-Int13)+Int13;
  end;
  AddObject(Project.ProjActiveClass,QN,QV,QAnglV,QF,QAnglF,QR,QM,CurX,CurY,QFC,
    QBC,QBW);
  if not FDlgObjAddRegion.Showing then
    ReloadObjectList;
//  RedrawImage;
end;

{-----------------------------------------------------------------------------}
{  procedure     ResetCoords                                                  }
{                                                                             }
{  discription   ����� ����. ���� �������� �� x0,y0,V0,F0                     }
{-----------------------------------------------------------------------------}
procedure TFMain.ResetCoords;
var
  i: Integer;
begin
  for i:=1 to Project.ProjObjectsCount do
    with Project.ProjObjects[i] do
    begin
      x:=x0;
      y:=y0;
      V:=V0;
      F:=F0;
      Vx:=V*cos(AngleV);
      Vy:=V*sin(AngleV);
      Fx:=F*cos(AngleF);
      Fy:=F*sin(AngleF);
    end;
end;

{-----------------------------------------------------------------------------}
{  procedure     SaveProject                                                  }
{                                                                             }
{  discription   ���������� �������                                           }
{-----------------------------------------------------------------------------}
procedure TFMain.SaveProject(Path: String);
begin
  AssignFile(ProjectFile,Path);
  Rewrite(ProjectFile);

  Write(ProjectFile,Project);

  CurrentPath:=Path;
  CurrentFileName:=ExtractFileName(Path);
  if cfg.ReadBool('Options','FullPath',False) then
    FView.Caption:=CurrentPath
  else
    FView.Caption:=CurrentFileName;
  if cfg.ReadBool('History','Enabled',True) then
    AddToHistory(CurrentPath);

  SaveDialog1.InitialDir:=ExtractFileDir(CurrentPath);

  CloseFile(ProjectFile);
  Changed:=False;
end;

{-----------------------------------------------------------------------------}
{  procedure     LoadProject                                                  }
{                                                                             }
{  discription   �������� �������                                             }
{-----------------------------------------------------------------------------}
procedure TFMain.LoadProject(Path: String);
var
  TempProject: TProject;
begin
  AssignFile(ProjectFile,Path);
  try
    Reset(ProjectFile);
    Read(ProjectFile,TempProject);
    CloseFile(ProjectFile);
  except
    Application.MessageBox(PChar('���� ����� ����������� ������'+#13+Path),
      PChar('���'),mb_OK+MB_ICONERROR);
    Exit;
  end;
  Reset(ProjectFile);
  Read(ProjectFile,Project);
  ApplyProjectParam;

  CurrentPath:=Path;
  CurrentFileName:=ExtractFileName(Path);
  if cfg.ReadBool('Options','FullPath',False) then
    FView.Caption:=CurrentPath
  else
    FView.Caption:=CurrentFileName;
  if cfg.ReadBool('History','Enabled',True) then
    AddToHistory(CurrentPath);

  OpenDialog1.InitialDir:=ExtractFileDir(CurrentPath);

  CloseFile(ProjectFile);

  ReloadClassesList;
  FPanelObjects.ComboBox1.ItemIndex:=0;
  ReloadObjectList;
  Changed:=False;
  ApplyProjectParam;
end;

{-----------------------------------------------------------------------------}
{  procedure     LoadDefaultClass                                             }
{                                                                             }
{  discription   �������� ������ �� ���������                                 }
{-----------------------------------------------------------------------------}
procedure TFMain.LoadDefaultClass;
begin
  try
    AssignFile(DefaultClassFile,
      ExtractFilePath(ParamStr(0))+'Data\DefaultClass.dat');
    Reset(DefaultClassFile);
    Read(DefaultClassFile,DefaultClass);
    CloseFile(DefaultClassFile);
  except
    with DefaultClass do
    begin
      ClassName1:='CDefault';
      ObjName:='Object';
      ClassID:=1;
      V0:=2000;
      AngleV:=DegToRad(0);
      F0:=0;
      AngleF:=DegToRad(-90);
      Radius:=4;
      Mass:=1.66E-27;
      FillColor:=clYellow;
      BorderColor:=clBlack;
      BorderWidth:=2;
      Val1:=0;
      Val2:=1;
      Val3:=0;
      Val4:=0;
      Val5:=0;
      Val6:=0;
      Int1:=1;
      Int2:=2;
      Int3:=0;
      Int4:=360;
      Int5:=0;
      Int6:=1;
      Int7:=30;
      Int8:=60;
      Int9:=5;
      Int10:=10;
      Int11:=10;
      Int12:=30;
      Int13:=1;
      Int14:=9;
      Ch1:=False;
      Ch2:=False;
      Ch3:=False;
    end;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     SaveDefaultClass                                             }
{                                                                             }
{  discription   ���������� ������ �� ���������                               }
{-----------------------------------------------------------------------------}
procedure TFMain.SaveDefaultClass;
begin
  CreateDir(ExtractFilePath(ParamStr(0))+'Data\');
  AssignFile(DefaultClassFile,
    ExtractFilePath(ParamStr(0))+'Data\DefaultClass.dat');
  Rewrite(DefaultClassFile);
  Write(DefaultClassFile,DefaultClass);
  CloseFile(DefaultClassFile);
end;

{-----------------------------------------------------------------------------}
{  procedure     LoadDefaultProject                                           }
{                                                                             }
{  discription   �������� ������� �� ���������                                }
{-----------------------------------------------------------------------------}
procedure TFMain.LoadDefaultProject;
begin
  try
    AssignFile(DefaultProjFile,
      ExtractFilePath(ParamStr(0))+'Data\DefaultProject.dat');
    Reset(DefaultProjFile);
    Read(DefaultProjFile,DefaultProject);
    CloseFile(DefaultProjFile);
  except
    with DefaultProject do
    begin
      ProjObjectsCount:=0;
      ProjWidth:=600;
      ProjHeight:=400;
      ProjBackgr:=clWhite;
      ProjClassesCount:=1;
      ProjDiscription:='';
      ProjScale:=0.0001;
    end;
  end;
  DefaultProject.ProjClasses[1]:=DefaultClass;
  DefaultProject.ProjActiveClass:=1;
  Project:=DefaultProject;
  FView.Caption:='��� �����';
  CurrentPath:='��� �����';
  CurrentFileName:='��� �����';
  Changed:=False;
  ApplyProjectParam;
end;

{-----------------------------------------------------------------------------}
{  procedure     SaveDefaultProject                                           }
{                                                                             }
{  discription   ���������� ������� �� ���������                              }
{-----------------------------------------------------------------------------}
procedure TFMain.SaveDefaultProject;
begin
  CreateDir(ExtractFilePath(ParamStr(0))+'Data\');
  AssignFile(DefaultProjFile,
    ExtractFilePath(ParamStr(0))+'Data\DefaultProject.dat');
  Rewrite(DefaultProjFile);
  Write(DefaultProjFile,DefaultProject);
  CloseFile(DefaultProjFile);
end;

{-----------------------------------------------------------------------------}
{  procedure     ApplyProjectParam                                            }
{                                                                             }
{  discription   ���������� ���������� �������                                }
{-----------------------------------------------------------------------------}
procedure TFMain.ApplyProjectParam;
var
  OldState: TWindowState;
begin
  FView.Image1.Picture:=nil;
  FView.Image1.Width:=Round(Project.ProjWidth * Scale);
  FView.Image1.Height:=Round(Project.ProjHeight * Scale);
  OldState:=FView.WindowState;
  FView.WindowState:=wsNormal;
  FView.ClientWidth:=FView.Image1.Width;
  FView.ClientHeight:=FView.Image1.Height;
  FView.WindowState:=OldState;
  RedrawImage;
end;

{-----------------------------------------------------------------------------}
{  procedure     ApplyProgramParam                                            }
{                                                                             }
{  discription   ���������� ���������� ���������                              }
{-----------------------------------------------------------------------------}
procedure TFMain.ApplyProgramParam;
var
  i, m: Integer;
begin
  if cfg.ReadBool('Options','FullPath',False)then
    FView.Caption:=CurrentPath
  else
    FView.Caption:=CurrentFileName;

  TimerMain.Interval:=cfg.ReadInteger('Options','TimerDelay',10);
  TimerView.Interval:=cfg.ReadInteger('Options','ViewDelay',10);
  StopWhenChanging:=cfg.ReadBool('Options','StopWhenChanging',False);
  EnableVisualization:=cfg.ReadBool('Options','Visualization',True);

  if cfg.ReadBool('History','Enabled',True) then
  begin
    m:=cfg.ReadInteger('History','MaxItemCount',10);
    for i:=ItemHistory.Count-1 downto 0 do
      if i+1 > m then
      begin
        ItemHistory.Delete(i);
        PopupMenu1.Items.Delete(i);
      end;
  end
  else
  begin
    ItemHistory.Clear;
    ItemHistory.Enabled:=False;
    PopupMenu1.Items.Clear;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     CreateNewProject                                             }
{                                                                             }
{  discription   �������� �������                                             }
{-----------------------------------------------------------------------------}
procedure TFMain.CreateNewProject;
begin
  FDlgProjCreate.ShowModal;
  if FDlgProjCreate.ModalResult = idOk then
  begin
    LoadDefaultProject;
    Project.ProjWidth:=FDlgProjCreate.SpEdW.Value;
    Project.ProjHeight:=FDlgProjCreate.SpEdH.Value;
    Project.ProjBackgr:=FDlgProjCreate.ColorBox1.Selected;
    Project.ProjDiscription:=FDlgProjCreate.Memo1.Lines.Text;
    Project.ProjScale:=StrToFloat(FDlgProjCreate.EdScale.Text);

    FView.Caption:='��� �����';
    CurrentPath:='��� �����';
    CurrentFileName:='��� �����';

    ReloadClassesList;
    FPanelObjects.ListView1.ItemIndex:=-1;
    ActiveObject:=0;
    ReloadObjectList;
    ApplyProjectParam;
    Changed:=False;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     ReloadObjectList                                             }
{                                                                             }
{  discription   �������� ������ ��������                                     }
{-----------------------------------------------------------------------------}
procedure TFMain.ReloadObjectList;
var
  i, Ind{, C}: Integer;
begin
  FPanelProp.UnloadProperties;

  while ObjectList.Items.Count > Project.ProjObjectsCount do
    ObjectList.Items.Delete(ObjectList.Items.Count-1);

  for i:=0 to ObjectList.Items.Count-1 do
    with ObjectList.Items[i] do
    begin
      Caption:=Project.ProjClasses[Project.ProjObjects[i+1].ClassID].ClassName1;
      SubItems[0]:=Project.ProjObjects[i+1].Name;
      SubItems[1]:=IntToStr(Project.ProjObjects[i+1].ObjectID);
    end;
  for i:=ObjectList.Items.Count+1 to Project.ProjObjectsCount do
    with ObjectList.Items.Add do
    begin
      Caption:=Project.ProjClasses[Project.ProjObjects[i].ClassID].ClassName1;
      SubItems.Add(Project.ProjObjects[i].Name);
      SubItems.Add(IntToStr(Project.ProjObjects[i].ObjectID));
    end;

  Ind:=FPanelObjects.ListView1.ItemIndex;
//  C:=FPanelObjects.ListView1.Items.Count;

  if FPanelObjects.ComboBox1.ItemIndex = 0 then
    FPanelObjects.ListView1.Items:=ObjectList.Items
  else
  begin
    FPanelObjects.ListView1.Clear;
    for i:=1 to Project.ProjObjectsCount do
      with Project.ProjObjects[i] do
        if ClassID = FPanelObjects.ComboBox1.ItemIndex then
          with FPanelObjects.ListView1.Items.Add do
          begin
            Caption:=Project.ProjClasses[Project.ProjObjects[i].ClassID].ClassName1;
            SubItems.Add(Project.ProjObjects[i].Name);
            SubItems.Add(IntToStr(Project.ProjObjects[i].ObjectID));
          end;
  end;

  if(Ind >= FPanelObjects.ListView1.Items.Count)then
    FPanelObjects.ListView1.ItemIndex:=FPanelObjects.ListView1.Items.Count-1
  else
    FPanelObjects.ListView1.ItemIndex:=Ind;
    (*
  if C<>FPanelObjects.ListView1.Items.Count then
    FPanelObjects.ListView1.ItemIndex:=FPanelObjects.ListView1.Items.Count-1
  else
    FPanelObjects.ListView1.ItemIndex:=Ind;
    *)

  if(FPanelObjects.ListView1.ItemIndex = -1)
    and(FPanelObjects.ListView1.Items.Count <> 0)then
    FPanelObjects.ListView1.ItemIndex:=0;

  FPanelObjects.StatusBar1.SimpleText:='�����: '
    +IntToStr(Project.ProjObjectsCount)+' ('
    +IntToStr(FPanelObjects.ListView1.Items.Count)+')';

  if FDlgProjOpt.Showing then
  begin
    FDlgProjOpt.Label4.Caption:=FPanelObjects.StatusBar1.SimpleText;
    FDlgProjOpt.ListView1.Items:=FPanelObjects.ListView1.Items;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     ReloadClassesList                                            }
{                                                                             }
{  discription   �������� ������ �������                                      }
{-----------------------------------------------------------------------------}
procedure TFMain.ReloadClassesList;
var
  i, Ind, C: Integer;
begin
  Ind:=FPanelClasses.CheckListBox1.ItemIndex;
  C:=FPanelClasses.CheckListBox1.Items.Count;
  FPanelClasses.CheckListBox1.Clear;

  // FPanelClasses
  for i:=1 to Project.ProjClassesCount do
    FPanelClasses.CheckListBox1.Items.Add(Project.ProjClasses[i].ClassName1);

  if C <> FPanelClasses.CheckListBox1.Items.Count then
    FPanelClasses.CheckListBox1.ItemIndex:=FPanelClasses.CheckListBox1.Items.Count-1
  else
    FPanelClasses.CheckListBox1.ItemIndex:=Ind;
  if FPanelClasses.CheckListBox1.ItemIndex = -1 then
    FPanelClasses.CheckListBox1.ItemIndex:=0;
  FPanelClasses.StatusBar1.SimpleText:='�����: '
    +IntToStr(Project.ProjClassesCount);
  FDlgProjOpt.Label5.Caption:=FPanelClasses.StatusBar1.SimpleText;

  // FPanelObjects
  Ind:=FPanelObjects.ComboBox1.ItemIndex;
  if Ind = -1 then Ind:=0;
  FPanelObjects.ComboBox1.Items:=FPanelClasses.CheckListBox1.Items;
  FPanelObjects.ComboBox1.Items.Insert(0,'[ ��� ������� ]');
  FPanelObjects.ComboBox1.ItemIndex:=Ind;

  // FPanelRes
  Ind:=FPanelRes.ComboBox1.ItemIndex;
  if Ind = -1 then Ind:=0;
  FPanelRes.ComboBox1.Items:=FPanelClasses.CheckListBox1.Items;
  if Ind > FPanelRes.ComboBox1.Items.Count-1 then
    FPanelRes.ComboBox1.ItemIndex:=FPanelRes.ComboBox1.Items.Count-1
  else
    FPanelRes.ComboBox1.ItemIndex:=Ind;

  // FPanelHistogr1
  Ind:=FPanelHistogr1.ComboBox1.ItemIndex;
  if Ind = -1 then Ind:=0;
  FPanelHistogr1.ComboBox1.Items:=FPanelClasses.CheckListBox1.Items;
  if Ind > FPanelHistogr1.ComboBox1.Items.Count-1 then
    FPanelHistogr1.ComboBox1.ItemIndex:=FPanelHistogr1.ComboBox1.Items.Count-1
  else
    FPanelHistogr1.ComboBox1.ItemIndex:=Ind;
  Ind:=FPanelHistogr1.ComboBox8.ItemIndex;
  if Ind = -1 then Ind:=0;
  FPanelHistogr1.ComboBox8.Items:=FPanelClasses.CheckListBox1.Items;
  if Ind > FPanelHistogr1.ComboBox8.Items.Count-1 then
    FPanelHistogr1.ComboBox8.ItemIndex:=FPanelHistogr1.ComboBox8.Items.Count-1
  else
    FPanelHistogr1.ComboBox8.ItemIndex:=Ind;

  // FPanelGraph
  Ind:=FPanelGraph.ComboBox1.ItemIndex;
  if Ind = -1 then Ind:=0;
  FPanelGraph.ComboBox1.Items:=FPanelClasses.CheckListBox1.Items;
  if Ind > FPanelGraph.ComboBox1.Items.Count-1 then
    FPanelGraph.ComboBox1.ItemIndex:=FPanelGraph.ComboBox1.Items.Count-1
  else
    FPanelGraph.ComboBox1.ItemIndex:=Ind;
  Ind:=FPanelGraph.ComboBox2.ItemIndex;
  if Ind = -1 then Ind:=0;
  FPanelGraph.ComboBox2.Items:=FPanelClasses.CheckListBox1.Items;
  if Ind > FPanelGraph.ComboBox2.Items.Count-1 then
    FPanelGraph.ComboBox2.ItemIndex:=FPanelGraph.ComboBox2.Items.Count-1
  else
    FPanelGraph.ComboBox2.ItemIndex:=Ind;
  Ind:=FPanelGraph.ComboBox3.ItemIndex;
  if Ind = -1 then Ind:=0;
  FPanelGraph.ComboBox3.Items:=FPanelClasses.CheckListBox1.Items;
  if Ind > FPanelGraph.ComboBox3.Items.Count-1 then
    FPanelGraph.ComboBox3.ItemIndex:=FPanelGraph.ComboBox3.Items.Count-1
  else
    FPanelGraph.ComboBox3.ItemIndex:=Ind;
  FPanelGraph.RefreshLegend;

  // FPanelClasses
  for i:=0 to FPanelClasses.CheckListBox1.Count-1 do
  begin
    FPanelClasses.CheckListBox1.Checked[i]:=False;
  end;
  FPanelClasses.CheckListBox1.Checked[FPanelClasses.CheckListBox1.ItemIndex]:=True;
  Project.ProjActiveClass:=FPanelClasses.CheckListBox1.ItemIndex+1;

  // FDlgProjOpt
  if FDlgProjOpt.Showing then
  begin
    FDlgProjOpt.CheckListBox1.Items:=FPanelClasses.CheckListBox1.Items;
    FDlgProjOpt.CheckListBox1.ItemIndex:=FPanelClasses.CheckListBox1.ItemIndex;
    FDlgProjOpt.ComboBox1.Items:=FPanelObjects.ComboBox1.Items;
    FDlgProjOpt.ComboBox1.ItemIndex:=FPanelObjects.ComboBox1.ItemIndex;
    for i:=0 to FPanelClasses.CheckListBox1.Count-1 do
      FDlgProjOpt.CheckListBox1.Checked[i]:=False;
    FDlgProjOpt.CheckListBox1.Checked[FPanelClasses.CheckListBox1.ItemIndex]:=True;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     StartAnimation                                               }
{                                                                             }
{  discription   ������ ��������                                              }
{-----------------------------------------------------------------------------}
procedure TFMain.StartAnimation;
begin
  TimerMain.Enabled:=True;
  if EnableVisualization then
    TimerView.Enabled:=True;
end;

{-----------------------------------------------------------------------------}
{  procedure     StopAnimation                                                }
{                                                                             }
{  discription   ��������� ��������                                           }
{-----------------------------------------------------------------------------}
procedure TFMain.StopAnimation;
begin
  TimerMain.Enabled:=False;
  TimerView.Enabled:=False;
  FPanelRes.ClearValues;
  ResetCoords;
  RedrawImage;
end;

{-----------------------------------------------------------------------------}
{  procedure     AddToHistory                                                 }
{                                                                             }
{  discription   ���������� ����� � �������                                   }
{-----------------------------------------------------------------------------}
procedure TFMain.AddToHistory(ItemCaption: String);
var
  i, MaxVal: Integer;
  T1, T2: TMenuItem;
begin
  ItemHistory.Enabled:=True;
  MaxVal:=cfg.ReadInteger('History','MaxItemCount',10);
  if ItemHistory.Count = 0 then
  begin
    T1:=TMenuItem.Create(Self);
    T1.Caption:=ItemCaption;
    T1.OnClick:=ExecuteHistoryItem;
    ItemHistory.Insert(0,T1);
    T2:=TMenuItem.Create(Self);
    T2.Caption:=ItemCaption;
    T2.OnClick:=ExecuteHistoryItem;
    PopupMenu1.Items.Insert(0,T2);
  end
  else
  begin
    for i:=0 to ItemHistory.Count-1 do
      if ItemHistory.Items[i].Caption = ItemCaption then
      begin
        ItemHistory.Delete(i);
        PopupMenu1.Items.Delete(i);
        T1:=TMenuItem.Create(Self);
        T1.Caption:=ItemCaption;
        T1.OnClick:=ExecuteHistoryItem;
        ItemHistory.Insert(0,T1);
        T2:=TMenuItem.Create(Self);
        T2.Caption:=ItemCaption;
        T2.OnClick:=ExecuteHistoryItem;
        PopupMenu1.Items.Insert(0,T2);
        Exit;
      end;
    if ItemHistory.Count = MaxVal then ItemHistory.Delete(MaxVal-1);
    T1:=TMenuItem.Create(Self);
    T1.Caption:=ItemCaption;
    T1.OnClick:=ExecuteHistoryItem;
    ItemHistory.Insert(0,T1);
    T2:=TMenuItem.Create(Self);
    T2.Caption:=ItemCaption;
    T2.OnClick:=ExecuteHistoryItem;
    PopupMenu1.Items.Insert(0,T2);
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     ExecuteHistoryItem                                           }
{                                                                             }
{  discription   �������� ����� �� �������                                    }
{-----------------------------------------------------------------------------}
procedure TFMain.ExecuteHistoryItem(Sender: TObject);
var
  FileName: String;
  Res, i: Integer;
begin
  aStop.Execute;
  FileName:=(Sender as TMenuItem).Caption;
  if FileExists(FileName) then
  begin
    Res:=idNo;
    if Changed then
      Res:=Application.MessageBox(PChar('������ ['+CurrentFileName
        +'] ��� �������. ��������� ���������?'),PChar('���'),
        mb_YesNoCancel+MB_ICONQUESTION);
    case Res of
    idYes:
      begin
        if FView.Caption = '��� �����' then
        begin
          if SaveDialog1.Execute then
          begin
            SaveProject(SaveDialog1.FileName);
            LoadProject(FileName);
          end;
        end
        else
        begin
          SaveProject(CurrentPath);
          LoadProject(FileName);
        end;
      end;
    idNo:
      LoadProject(FileName);
    end;
  end
  else
  begin
    Res:=Application.MessageBox(PChar('������ ��� ������ ����� ��� ���� �� ������'+
      #13#13+'������� �� �������?'),PChar('���'),mb_YesNo+MB_ICONERROR);
    if Res = idYes then
    begin
      for i:=0 to ItemHistory.Count-1 do
        if ItemHistory.Items[i].Caption = FileName then
        begin
          ItemHistory.Delete(i);
          PopupMenu1.Items.Delete(i);
          Break;
        end;
      if ItemHistory.Count = 0 then ItemHistory.Enabled:=False;
    end;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     LoadHistory                                                  }
{                                                                             }
{  discription   �������� �������                                             }
{-----------------------------------------------------------------------------}
procedure TFMain.LoadHistory;
var
  i: Integer;
  Strings: TStringList;
  FileName: String;
  T1, T2: TMenuItem;
begin
  if cfg.SectionExists('HistoryItems') then
  begin
    Strings:=TStringList.Create;
    ItemHistory.Enabled:=True;
    cfg.ReadSection('HistoryItems',Strings);
    for i:=0 to Strings.Count-1 do
    begin
      FileName:=cfg.ReadString('HistoryItems','Item'+IntToStr(i),'');
      if FileName <> '' then
      begin
        T1:=TMenuItem.Create(Self);
        T1.Caption:=FileName;
        T1.OnClick:=ExecuteHistoryItem;
        ItemHistory.Add(T1);
        T2:=TMenuItem.Create(Self);
        T2.Caption:=FileName;
        T2.OnClick:=ExecuteHistoryItem;
        PopupMenu1.Items.Add(T2);
      end;
    end;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     SaveHistory                                                  }
{                                                                             }
{  discription   ���������� �������                                           }
{-----------------------------------------------------------------------------}
procedure TFMain.SaveHistory;
var
  i: Integer;
begin
  if cfg.SectionExists('HistoryItems') then
    cfg.EraseSection('HistoryItems');
  for i:=0 to ItemHistory.Count-1 do
    cfg.WriteString('HistoryItems','Item'+IntToStr(i),ItemHistory.Items[i].Caption);
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                                 FMain                                       }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFMain.FormCreate(Sender: TObject);
begin
  Randomize;
  cfg:=TMemIniFile.Create(ExtractFilePath(ParamStr(0))+'MIG.ini');
  ActiveObject:=0;
  Scale:=1;
  LoadHistory;
  Top:=cfg.ReadInteger('Form','Top',155);
  Left:=cfg.ReadInteger('Form','Left',195);
  Width:=cfg.ReadInteger('Form','Width',600);
  Height:=cfg.ReadInteger('Form','Height',600);

  case cfg.ReadInteger('Form','WindowState',1) of
    0: WindowState:=wsNormal;
    1: WindowState:=wsMaximized;
    2: WindowState:=wsMinimized;
  end;

  aEnQAdd.Checked:=cfg.ReadBool('Program','QuickAddEnabled',True);
  aEnQSel.Checked:=cfg.ReadBool('Program','QuickSelEnabled',False);
  if aEnQSel.Checked then aEnQAdd.Checked:=False;

  aDrawNames.Checked:=cfg.ReadBool('Program','DrawNames',False);
  aDrawSel.Checked:=cfg.ReadBool('Program','DrawSel',True);

  OpenDialog1.InitialDir:=cfg.ReadString('Program','OpenDir',
    ExtractFilePath(ParamStr(0)));
  SaveDialog1.InitialDir:=cfg.ReadString('Program','SaveDir',
    ExtractFilePath(ParamStr(0)));

  SvDlgExp.InitialDir:=ExtractFilePath(ParamStr(0));
  OpnDlgAddObjCl.InitialDir:=ExtractFilePath(ParamStr(0));

  ApplyProgramParam;
end;

procedure TFMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  cfg.WriteBool('PanelClasses','Visible',FPanelClasses.Showing);
  cfg.WriteInteger('PanelClasses','Left',FPanelClasses.Left);
  cfg.WriteInteger('PanelClasses','Top',FPanelClasses.Top);
  cfg.WriteInteger('PanelClasses','Height',FPanelClasses.Height);

  cfg.WriteBool('PanelObjects','Visible',FPanelObjects.Showing);
  cfg.WriteInteger('PanelObjects','Left',FPanelObjects.Left);
  cfg.WriteInteger('PanelObjects','Top',FPanelObjects.Top);
  cfg.WriteInteger('PanelObjects','Height',FPanelObjects.Height);

  cfg.WriteBool('PanelProperties','Visible',FPanelProp.Showing);
  cfg.WriteInteger('PanelProperties','Left',FPanelProp.Left);
  cfg.WriteInteger('PanelProperties','Top',FPanelProp.Top);
  cfg.WriteInteger('PanelProperties','Height',FPanelProp.Height);

  cfg.WriteBool('PanelTracking','Visible',FPanelRes.Showing);
  cfg.WriteInteger('PanelTracking','Left',FPanelRes.Left);
  cfg.WriteInteger('PanelTracking','Top',FPanelRes.Top);

  cfg.WriteBool('PanelGraph','Visible',FPanelGraph.Showing);
  cfg.WriteInteger('PanelGraph','Left',FPanelGraph.Left);
  cfg.WriteInteger('PanelGraph','Top',FPanelGraph.Top);

  cfg.WriteBool('PanelHistogram','Visible',FPanelHistogr1.Showing);
  cfg.WriteInteger('PanelHistogram','Left',FPanelHistogr1.Left);
  cfg.WriteInteger('PanelHistogram','Top',FPanelHistogr1.Top);

  cfg.WriteBool('Program','QuickAddEnabled',aEnQAdd.Checked);
  cfg.WriteBool('Program','QuickSelEnabled',aEnQSel.Checked);
  cfg.WriteBool('Program','DrawNames',aDrawNames.Checked);
  cfg.WriteBool('Program','DrawSel',aDrawSel.Checked);

  if WindowState=wsNormal then
    cfg.WriteInteger('Form','WindowState',0)
  else
  begin
    if WindowState=wsMaximized then
    begin
      WindowState:=wsNormal;
      cfg.WriteInteger('Form','WindowState',1);
    end
    else
    begin
      WindowState:=wsNormal;
      cfg.WriteInteger('Form','WindowState',2);
    end;
  end;
  cfg.WriteInteger('Form','Top',Top);
  cfg.WriteInteger('Form','Left',Left);
  cfg.WriteInteger('Form','Width',Width);
  cfg.WriteInteger('Form','Height',Height);
  cfg.WriteString('Program','OpenDir',OpenDialog1.InitialDir);
  cfg.WriteString('Program','SaveDir',SaveDialog1.InitialDir);

  SaveHistory;

  cfg.UpdateFile;
end;

procedure TFMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  Res: Integer;
begin
  aStop.Execute;
  CanClose:=False;
  Res:=idNo;
  if Changed then
    Res:=Application.MessageBox(PChar('������ ['+CurrentFileName
        +'] ��� �������. ��������� ���������?'),PChar('���'),
        mb_YesNoCancel+MB_ICONQUESTION);
  case Res of
    idYes:
      begin
        SaveDialog1.FileName:=CurrentFileName;
        if FView.Caption = '��� �����' then
        begin
          if SaveDialog1.Execute then
          begin
            SaveProject(SaveDialog1.FileName);
            CanClose:=True;
          end;
        end
        else
        begin
          SaveProject(CurrentPath);
          CanClose:=True;
        end;
      end;
  idNo:
    CanClose:=True;
  end;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                                 TIMERS                                      }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFMain.TimerMainTimer(Sender: TObject);
var
  i, j, n: Integer;
  dx, dy, dl: Real;
  fi, adir, bdir, av, bv, vn, dv1, dv2: Real;
  Xf, Yf: Real;
begin
  for i:=1 to Project.ProjObjectsCount do
    with Project.ProjObjects[i], Project do
    begin
      { �������� ������������ � ����� � ������ ��������� }
      Xf:=x * Project.ProjScale;
      if(Xf <= Radius)or(Xf >= ProjWidth-Radius)then
      begin
        Vx:=-Vx;
        n:=0;
        while(n<100)and((Xf <= Radius)or(Xf >= ProjWidth-Radius))do
        begin
          x:=x + Vx;
          Xf:=x * Project.ProjScale;
          Inc(n);
        end;
      end;
      { �������� ������������ � ����� � ������ ��������� }
      Yf:=y * Project.ProjScale;
      if(Yf <= Radius)or(Yf >= ProjHeight-Radius)then
      begin
        Vy:=-Vy;
        n:=0;
        while(n<100)and((Yf <= Radius)or(Yf >= ProjHeight-Radius))do
        begin
          y:=y + Vy;
          Yf:=y * Project.ProjScale;
          Inc(n);
        end;
      end;
      { �������� ������������ � ������� ��������� }
      for j:=1 to ProjObjectsCount do
        if j<>i then
        begin
          dx:=x - ProjObjects[j].x;
          dy:=y - ProjObjects[j].y;
          dl:=Sqrt(dx*dx + dy*dy)*Project.ProjScale;
          if dl <= (Radius+ProjObjects[j].Radius) then
          begin
            fi:=ArcTan2(ProjObjects[j].y-y, ProjObjects[j].x-x);
            adir:=ArcTan2(Vy,Vx);
            bdir:=ArcTan2(ProjObjects[j].Vy, ProjObjects[j].Vx);
            av:=Sqrt(Vx*Vx + Vy*Vy);
            bv:=Sqrt(ProjObjects[j].Vx*ProjObjects[j].Vx
                + ProjObjects[j].Vy*ProjObjects[j].Vy);
            vn:=av*sin(adir-fi+PI/2) - bv*sin(bdir-fi+PI/2);

            dv1:=-2*ProjObjects[j].Mass / (Mass+ProjObjects[j].Mass)*vn;
            dv2:=2*Mass / (Mass+ProjObjects[j].Mass)*vn;

            Vx:=Vx + dv1*cos(fi);
            Vy:=Vy + dv1*sin(fi);
            ProjObjects[j].Vx:=ProjObjects[j].Vx + dv2*cos(fi);
            ProjObjects[j].Vy:=ProjObjects[j].Vy + dv2*sin(fi);

            { ���������� ��������� �������� }
            dl:=Sqrt(dx*dx + dy*dy) * Project.ProjScale;
            while dl <= (Radius+ProjObjects[j].Radius)do
            begin
              x:=x + Vx;// * TimerMain.Interval;
              y:=y + Vy;// * TimerMain.Interval;
              ProjObjects[j].x:=ProjObjects[j].x
                  + ProjObjects[j].Vx;// * TimerMain.Interval;
              ProjObjects[j].y:=ProjObjects[j].y
                  + ProjObjects[j].Vy;// * TimerMain.Interval;
              dx:=x - ProjObjects[j].x;
              dy:=y - ProjObjects[j].y;
              dl:=Sqrt(dx*dx + dy*dy)*Project.ProjScale;
            end;
          end;
        end;
      Vx:=Vx + Fx / Mass;
      Vy:=Vy + Fy / Mass;
      x:=x + Vx;// * TimerMain.Interval;
      y:=y + Vy;// * TimerMain.Interval;
    end;
end;

procedure TFMain.TimerViewTimer(Sender: TObject);
begin
  RedrawImage;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                                 ACTION LIST                                 }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFMain.aStartExecute(Sender: TObject);
begin
  if aPause.Checked then
  begin
    TimerMain.Enabled:=True;
    if EnableVisualization then
      TimerView.Enabled:=True;
    aPause.Checked:=False;
    aStart.Checked:=False;
    aStart.Checked:=True;
    aStepForward.Enabled:=False;
  end
  else
  begin
    StopAnimation;
    StartAnimation;
    aStart.Checked:=False;
    aStart.Checked:=True;
    aStepForward.Enabled:=False;
  end;
end;

procedure TFMain.aStepForwardExecute(Sender: TObject);
begin
  if(aStart.Checked)and(aPause.Checked)then
  begin
    TimerMainTimer(TimerMain);
    if EnableVisualization then
      TimerViewTimer(TimerMain);
  end;
end;

procedure TFMain.aPauseExecute(Sender: TObject);
begin
  if aStart.Checked then
  begin
    TimerMain.Enabled:=not TimerMain.Enabled;
    TimerView.Enabled:=not TimerView.Enabled;
    aPause.Checked:=not aPause.Checked;
    aStepForward.Enabled:=not aStepForward.Enabled;
    RedrawImage;
  end;
end;

procedure TFMain.aStopExecute(Sender: TObject);
begin
  StopAnimation;
  aStart.Checked:=False;
  aPause.Checked:=False;
  aStepForward.Enabled:=False;
end;

procedure TFMain.aProjOptionsExecute(Sender: TObject);
var
  i: Integer;
  NewScale: Real;
begin
  aStop.Execute;
  FDlgProjOpt.ShowModal;
  if FDlgProjOpt.ModalResult = idOk then
  begin
    Project.ProjBackgr:=FDlgProjOpt.ColorBox1.Selected;
    Project.ProjWidth:=FDlgProjOpt.SpEdW.Value;
    Project.ProjHeight:=FDlgProjOpt.SpEdH.Value;
    Project.ProjDiscription:=FDlgProjOpt.Memo1.Lines.Text;
    NewScale:=StrToFloat(FDlgProjOpt.EdScale.Text);
    if(NewScale <> Project.ProjScale)then
    begin
      for i:=1 to Project.ProjObjectsCount do
      begin
        Project.ProjObjects[i].x0:=Project.ProjObjects[i].x0 * Project.ProjScale;
        Project.ProjObjects[i].x0:=Project.ProjObjects[i].x0 / NewScale;
        Project.ProjObjects[i].y0:=Project.ProjObjects[i].y0 * Project.ProjScale;
        Project.ProjObjects[i].y0:=Project.ProjObjects[i].y0 / NewScale;
      end;
    end;
    Project.ProjScale:=NewScale;
    ResetCoords;

    Changed:=True;
    ApplyProjectParam;
  end;
end;

procedure TFMain.aOptionsExecute(Sender: TObject);
begin
  aStop.Execute;
  FDlgOptions.ReadSettings;
  FDlgOptions.ShowModal;
  if FDlgOptions.ModalResult = idOk then
  begin
    FDlgOptions.WriteSettings;
    ApplyProgramParam;
  end;
end;

procedure TFMain.aNewExecute(Sender: TObject);
var
  Res: Integer;
begin
  Res:=idNo;
  if Changed then
    Res:=Application.MessageBox(PChar('������ ['+CurrentFileName
      +'] ��� �������. ��������� ���������?'),PChar('���'),
      mb_YesNoCancel+MB_ICONQUESTION);
  case Res of
    idYes:
      begin
        SaveDialog1.FileName:=CurrentFileName;
        if FView.Caption = '��� �����' then
        begin
          if SaveDialog1.Execute then
          begin
            SaveProject(SaveDialog1.FileName);
            CreateNewProject;
          end;
        end
        else
        begin
          SaveProject(CurrentPath);
          CreateNewProject;
        end;
      end;
    idNo:
      CreateNewProject;
  end;
end;

procedure TFMain.aOpenExecute(Sender: TObject);
var
  Res: Integer;
begin
  aStop.Execute;
  Res:=idNo;
  if Changed then
    Res:=Application.MessageBox(PChar('������ ['+CurrentFileName
      +'] ��� �������. ��������� ���������?'),PChar('���'),
      mb_YesNoCancel+MB_ICONQUESTION);
  case Res of
    idYes:
      begin
        SaveDialog1.FileName:=CurrentFileName;
        if FView.Caption = '��� �����' then
        begin
          if SaveDialog1.Execute then
          begin
            SaveProject(SaveDialog1.FileName);
            if OpenDialog1.Execute then LoadProject(OpenDialog1.FileName);
          end;
        end
        else
        begin
          SaveProject(CurrentPath);
          if OpenDialog1.Execute then LoadProject(OpenDialog1.FileName);
        end;
      end;
    idNo:
      if OpenDialog1.Execute then LoadProject(OpenDialog1.FileName);
  end;
end;

procedure TFMain.aSaveExecute(Sender: TObject);
begin
  aStop.Execute;
  SaveDialog1.FileName:=CurrentFileName;
  if FView.Caption = '��� �����' then
  begin
    if SaveDialog1.Execute then SaveProject(SaveDialog1.FileName);
  end
  else
    SaveProject(CurrentPath);
end;

procedure TFMain.aSaveAsExecute(Sender: TObject);
begin
  aStop.Execute;
  SaveDialog1.FileName:=CurrentFileName;
  if SaveDialog1.Execute then SaveProject(SaveDialog1.FileName);
end;

procedure TFMain.aExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TFMain.aAddClassExecute(Sender: TObject);
begin
  if StopWhenChanging then aStop.Execute;
  with FDlgClassAdd do
  begin
    ShowModal;
    if ModalResult = idOk then
      AddClass(EdName.Text,EdObjName.Text,StrToFloat(EdSpeed1.Text),
          DegToRad(StrToFloat(EdSAngl1.Text)), StrToFloat(EdAcc1.Text),
          DegToRad(StrToFloat(EdAAngl1.Text)),SpEditRad.Value,
          StrToFloat(EdMass1.Text),ColorBox1.Selected,
          ColorBox2.Selected,SpEdBW.Value, RadioGroup1.ItemIndex,
          RadioGroup2.ItemIndex,RadioGroup3.ItemIndex, RadioGroup4.ItemIndex,
          RadioGroup5.ItemIndex,RadioGroup6.ItemIndex, SpinEdit1.Value,
          SpinEdit2.Value,SpinEdit3.Value,SpinEdit4.Value, SpinEdit5.Value,
          SpinEdit6.Value,SpinEdit7.Value,SpinEdit8.Value, SpinEdit9.Value,
          SpinEdit10.Value,SpinEdit11.Value,SpinEdit12.Value, SpinEdit13.Value,
          SpinEdit14.Value,CheckBox1.Checked, CheckBox2.Checked,
          CheckBox3.Checked);
  end;
end;

procedure TFMain.aDublClassExecute(Sender: TObject);
begin
  if FPanelClasses.CheckListBox1.ItemIndex<>-1 then
    with Project.ProjClasses[Project.ProjActiveClass] do
      AddClass(ClassName1+'_Copy', ObjName+'_Copy', V0, AngleV, F0, AngleF,
          Radius, Mass, FillColor, BorderColor, BorderWidth, Val1, Val2, Val3,
          Val4, Val5, Val6, Int1, Int2, Int3, Int4, Int5, Int6, Int7, Int8,
          Int9, Int10, Int11, Int12, Int13,Int14,Ch1,Ch2,Ch3);
end;

procedure TFMain.aEditClassExecute(Sender: TObject);
begin
  if StopWhenChanging then aStop.Execute;
  if FPanelClasses.CheckListBox1.ItemIndex<>-1 then
  begin
    with FDlgClassEdit do
    begin
      with Project.ProjClasses[Project.ProjActiveClass] do
      begin
        EdName.Text:=ClassName1;
        EdObjName.Text:=ObjName;
        EdSpeed1.Text:=FloatToStr(V0);
        EdSAngl1.Text:=FloatToStr(RadToDeg(AngleV));
        EdAcc1.Text:=FloatToStr(F0);
        EdAAngl1.Text:=FloatToStr(RadToDeg(AngleF));
        EdMass1.Text:=FloatToStr(Mass);
        SpEditRad.Value:=Radius;
        ColorBox1.Selected:=FillColor;
        ColorBox2.Selected:=BorderColor;
        SpEdBW.Value:=BorderWidth;

        SpinEdit1.Value:=Int1;
        SpinEdit2.Value:=Int2;
        SpinEdit3.Value:=Int3;
        SpinEdit4.Value:=Int4;
        SpinEdit5.Value:=Int5;
        SpinEdit6.Value:=Int6;
        SpinEdit7.Value:=Int7;
        SpinEdit8.Value:=Int8;
        SpinEdit9.Value:=Int9;
        SpinEdit10.Value:=Int10;
        SpinEdit11.Value:=Int11;
        SpinEdit12.Value:=Int12;
        SpinEdit13.Value:=Int13;
        SpinEdit14.Value:=Int14;

        RadioGroup1.ItemIndex:=Val1;
        RadioGroup2.ItemIndex:=Val2;
        RadioGroup3.ItemIndex:=Val3;
        RadioGroup4.ItemIndex:=Val4;
        RadioGroup5.ItemIndex:=Val5;
        RadioGroup6.ItemIndex:=Val6;

        CheckBox1.Checked:=Ch1;
        CheckBox2.Checked:=Ch2;
        CheckBox3.Checked:=Ch3;
      end;
      ShowModal;
      if ModalResult = idOk then
        with Project.ProjClasses[Project.ProjActiveClass] do
          ModifyClass(EdName.Text,EdObjName.Text, StrToFloat(EdSpeed1.Text),
              DegToRad(StrToFloat(EdSAngl1.Text)), StrToFloat(EdAcc1.Text),
              DegToRad(StrToFloat(EdAAngl1.Text)), SpEditRad.Value,
              StrToFloat(EdMass1.Text), ColorBox1.Selected, ColorBox2.Selected,
              SpEdBW.Value, RadioGroup1.ItemIndex, RadioGroup2.ItemIndex,
              RadioGroup3.ItemIndex, RadioGroup4.ItemIndex,
              RadioGroup5.ItemIndex,RadioGroup6.ItemIndex, SpinEdit1.Value,
              SpinEdit2.Value, SpinEdit3.Value, SpinEdit4.Value,
              SpinEdit5.Value, SpinEdit6.Value, SpinEdit7.Value,
              SpinEdit8.Value, SpinEdit9.Value, SpinEdit10.Value,
              SpinEdit11.Value, SpinEdit12.Value, SpinEdit13.Value,
              SpinEdit14.Value, CheckBox1.Checked, CheckBox2.Checked,
              CheckBox3.Checked);
    end;
  end;
end;

procedure TFMain.aDelClassExecute(Sender: TObject);
begin
  if StopWhenChanging then aStop.Execute;
  if (FPanelClasses.CheckListBox1.ItemIndex<>-1)
    and(Project.ProjClassesCount>1)then
    if Application.MessageBox(PChar('�������� ������ �������� � �������� ��������,'+#13+
      '������������� ����� ������'+#13#13+'����������?'),
      PChar('���'),MB_YESNO+MB_ICONQUESTION) = idYes then
        DelClass(FPanelClasses.CheckListBox1.ItemIndex+1);
end;

procedure TFMain.aDelObjectsOfClassExecute(Sender: TObject);
var
  i: Integer;
begin
  if StopWhenChanging then aStop.Execute;
  for i:=Project.ProjObjectsCount downto 1 do
    if Project.ProjObjects[i].ClassID = Project.ProjActiveClass then
      DelObject(i);
  ReloadObjectList;
  if Project.ProjObjectsCount = 0 then
  begin
    ActiveObject:=0;
    FPanelProp.UnloadProperties;
  end;
  RedrawImage;
end;

procedure TFMain.aAddObjectExecute(Sender: TObject);
var
  i: Integer;
begin
  if StopWhenChanging then aStop.Execute;
  with FDlgObjAdd do
  begin
    ComboBox1.Clear;
    for i:=1 to Project.ProjClassesCount do
      ComboBox1.Items.Add(Project.ProjClasses[i].ClassName1);
    ComboBox1.ItemIndex:=Project.ProjActiveClass - 1;

    with Project.ProjClasses[Project.ProjActiveClass] do
    begin
      EdName.Text:=ObjName;
      if Val1 = 0 then
        EdSpeed.Text:=FloatToStr(V0)
      else
        EdSpeed.Text:=FloatToStr((Random(Int2*10000-Int1*10000)+Int1*10000)/10000);

      if Val2 = 0 then
        EdSAngl.Text:=FloatToStr(RadToDeg(AngleV))
      else
        EdSAngl.Text:=FloatToStr((Random(Int4*10000-Int3*10000)+Int3*10000)/10000);

      if Val3 = 0 then
        EdAcc.Text:=FloatToStr(F0)
      else
        EdAcc.Text:=FloatToStr((Random(Int6*10000-Int5*10000)+Int5*10000)/10000);

      if Val4 = 0 then
        EdAAngl.Text:=FloatToStr(RadToDeg(AngleF))
      else
        EdAAngl.Text:=FloatToStr((Random(Int8*10000-Int7*10000)+Int7*10000)/10000);

      if Val5 = 0 then
        SpEditRad.Value:=Radius
      else
        SpEditRad.Value:=Random(Int10-Int9+1)+Int9;

      if Val6 = 0 then
        EdMass.Text:=FloatToStr(Mass)
      else
        EdMass.Text:=FloatToStr((Random(Int12*10000-Int11*10000)+Int11*10000)/10000);

      if not Ch1 then
        ColorBox1.Selected:=FillColor
      else
        ColorBox1.Selected:=ColorList[Random(16)];
      if not Ch2 then
        ColorBox2.Selected:=BorderColor
      else
        ColorBox2.Selected:=ColorList[Random(16)];
      if not Ch3 then
        SpinEdit1.Value:=BorderWidth
      else
        SpinEdit1.Value:=Random(Int14-Int13)+Int13;
    end;

    ShowModal;
    if ModalResult = idOk then
    begin
      AddObject(ComboBox1.ItemIndex+1, EdName.Text, StrToFloat(EdSpeed.Text),
          DegToRad(StrToFloat(EdSAngl.Text)), StrToFloat(EdAcc.Text),
          DegToRad(StrToFloat(EdAAngl.Text)), SpEditRad.Value,
          StrToFloat(EdMass.Text), StrToFloat(Edx0.Text), StrToFloat(Edy0.Text),
          ColorBox1.Selected, ColorBox2.Selected,SpinEdit1.Value);
      ReloadObjectList;
      RedrawImage;
    end;
  end;
end;

procedure TFMain.aDublObjectExecute(Sender: TObject);
var
  xx,yy: Real;
begin
  if ActiveObject<>0 then
    with Project.ProjObjects[ActiveObject] do
    begin
      if x0+cfg.ReadInteger('Options','CopyX',50) > FView.Image1.Width - Radius then
        xx:=x0-cfg.ReadInteger('Options','CopyX',50)
      else
        xx:=x0+cfg.ReadInteger('Options','CopyX',50);
      if y0+cfg.ReadInteger('Options','CopyY',50) > FView.Image1.Height - Radius then
        yy:=y0-cfg.ReadInteger('Options','CopyY',50)
      else
        yy:=y0+cfg.ReadInteger('Options','CopyY',50);
      AddObject(ClassID,PartName,V,AngleV,F,AngleF,Radius,Mass,xx,yy,FillColor,BorderColor,BorderWidth);
      ReloadObjectList;
    end;
end;

procedure TFMain.aEditObjectExecute(Sender: TObject);
var
  i: Integer;
begin
  if StopWhenChanging then aStop.Execute;
  if ActiveObject<>0 then
    with FDlgObjEdit do
    begin
      ComboBox1.Clear;
      for i:=1 to Project.ProjClassesCount do
        ComboBox1.Items.Add(Project.ProjClasses[i].ClassName1);
      ComboBox1.ItemIndex:=Project.ProjObjects[ActiveObject].ClassID-1;
      with Project.ProjObjects[ActiveObject] do
      begin
        EdName.Text:=PartName;
        EdSpeed.Text:=FloatToStr(V);
        EdSAngl.Text:=FloatToStr(RadToDeg(AngleV));
        EdAcc.Text:=FloatToStr(F);
        EdAAngl.Text:=FloatToStr(RadToDeg(AngleF));
        EdMass.Text:=FloatToStr(Mass);
        SpEditRad.Value:=Radius;
        Edx0.Text:=FloatToStr(x0);
        Edy0.Text:=FloatToStr(y0);
        ColorBox1.Selected:=FillColor;
        ColorBox2.Selected:=BorderColor;
        SpinEdit1.Value:=BorderWidth;
      end;
      ShowModal;
      if ModalResult = idOk then
      begin
        ModifyObject(ActiveObject, ComboBox1.ItemIndex+1,
            EdName.Text,StrToFloat(EdSpeed.Text),
            DegToRad(StrToFloat(EdSAngl.Text)),
            StrToFloat(EdAcc.Text), DegToRad(StrToFloat(EdAAngl.Text)),
            SpEditRad.Value, StrToFloat(EdMass.Text),
            StrToFloat(Edx0.Text), StrToFloat(Edy0.Text),
            Project.ProjObjects[ActiveObject].x,
            Project.ProjObjects[ActiveObject].y,
            ColorBox1.Selected,ColorBox2.Selected,SpinEdit1.Value);
        ReloadObjectList;
      end;
    end;
end;

procedure TFMain.aDelObjectExecute(Sender: TObject);
begin
  if StopWhenChanging then aStop.Execute;
  if ActiveObject>0 then
  begin
    DelObject(ActiveObject);
    ReloadObjectList;
    if Project.ProjObjectsCount = 0 then
    begin
      ActiveObject:=0;
      FPanelProp.UnloadProperties;
    end;
    RedrawImage;
  end;
end;

procedure TFMain.aDelAllObjectsExecute(Sender: TObject);
begin
  if StopWhenChanging then aStop.Execute;
  Project.ProjObjectsCount:=0;
  ReloadObjectList;
  ActiveObject:=0;
  FPanelProp.UnloadProperties;
  RedrawImage;
  Changed:=True;
end;

procedure TFMain.aAboutExecute(Sender: TObject);
begin
  aStop.Execute;
  FDlgAbout.ShowModal;
end;

procedure TFMain.aShowClassesExecute(Sender: TObject);
begin
  FPanelClasses.Visible:=aShowClasses.Checked;
end;

procedure TFMain.aShowObjectsExecute(Sender: TObject);
begin
  FPanelObjects.Visible:=aShowObjects.Checked;
end;

procedure TFMain.aShowPropExecute(Sender: TObject);
begin
  FPanelProp.Visible:=aShowProp.Checked;
end;

procedure TFMain.aShowResExecute(Sender: TObject);
begin
  if aShowRes.Checked then
    FPanelRes.Visible:=True
  else
    FPanelRes.Close;
end;

procedure TFMain.aShowGraphExecute(Sender: TObject);
begin
  if aShowGraph.Checked then
    FPanelGraph.Visible:=True
  else
    FPanelGraph.Close;
end;

procedure TFMain.aShowHistogr1Execute(Sender: TObject);
begin
  if aShowHistogr1.Checked then
    FPanelHistogr1.Visible:=True
  else
    FPanelHistogr1.Close;
end;

procedure TFMain.aHideAllPanelsExecute(Sender: TObject);
begin
  if((FPanelClasses.Showing)or(FPanelObjects.Showing)
    or(FPanelProp.Showing)or(FPanelRes.Showing)
    or(FPanelGraph.Showing)or(FPanelHistogr1.Showing))then
  begin
    Visibility[1]:=FPanelClasses.Showing;
    Visibility[2]:=FPanelObjects.Showing;
    Visibility[3]:=FPanelProp.Showing;
    Visibility[4]:=FPanelRes.Showing;
    Visibility[5]:=FPanelGraph.Showing;
    Visibility[6]:=FPanelHistogr1.Showing;
    FPanelObjects.Close;
    FPanelProp.Close;
    FPanelClasses.Close;
    FPanelRes.Close;
    FPanelGraph.Close;
    FPanelHistogr1.Close;
  end
  else
  begin
    FPanelClasses.Visible:=Visibility[1];
    FPanelObjects.Visible:=Visibility[2];
    FPanelProp.Visible:=Visibility[3];
    FPanelRes.Visible:=Visibility[4];
    FPanelGraph.Visible:=Visibility[5];
    FPanelHistogr1.Visible:=Visibility[6];
  end;
end;

procedure TFMain.aSetDefPosExecute(Sender: TObject);
var
  Rect: TRect;
begin
  SystemParametersInfo(SPI_GETWORKAREA,0,@Rect,0);

  FPanelClasses.Top:=Rect.Top + 70;
  FPanelClasses.Left:=Rect.Left + 3;

  FPanelObjects.Top:=FPanelClasses.Height + Rect.Top + 70;
  FPanelObjects.Left:=Rect.Left + 3;

  FPanelProp.Top:=FPanelObjects.Height + FPanelClasses.Height + Rect.Top + 70;
  FPanelProp.Left:=Rect.Left + 3;

  FPanelRes.Top:=Rect.Top + 70;
  FPanelRes.Left:=Rect.Right - FPanelRes.Width - 3;

  FPanelHistogr1.Top:=Rect.Bottom - FPanelHistogr1.Height - 21;
  FPanelHistogr1.Left:=Rect.Right - FPanelHistogr1.Width - 3;

  FPanelGraph.Top:=Rect.Bottom - FPanelHistogr1.Height - FPanelGraph.Height
    - 21;
  FPanelGraph.Left:=Rect.Right - FPanelGraph.Width - 3;
end;

procedure TFMain.aSetDefSizeExecute(Sender: TObject);
begin
  FPanelClasses.Height:=157;
  FPanelObjects.Height:=217;
  FPanelProp.Height:=277;
end;

procedure TFMain.aEnQSelExecute(Sender: TObject);
begin
  FView.Image1.Cursor:=crDefault;
  aEnQAdd.Checked:=False;
end;

procedure TFMain.aEnQAddExecute(Sender: TObject);
begin
  if aEnQAdd.Checked then
    FView.Image1.Cursor:=crCross
  else
    FView.Image1.Cursor:=crDefault;
  aEnQSel.Checked:=False;
end;

procedure TFMain.aDrawNamesExecute(Sender: TObject);
begin
  RedrawImage;
end;

procedure TFMain.aSelNothingExecute(Sender: TObject);
begin             
  FPanelObjects.ListView1.ItemIndex:=-1;
  ActiveObject:=0;
  FPanelProp.UnloadProperties;
  RedrawImage;
end;

procedure TFMain.aRefreshExecute(Sender: TObject);
begin
  RedrawImage;
end;

procedure TFMain.aDrawSelExecute(Sender: TObject);
begin
  RedrawImage;
end;

procedure TFMain.aAddObjectFromFileExecute(Sender: TObject);
var
  i, j, OldClassID, NewClassID: Integer;
begin
  aStop.Execute;
  if OpnDlgAddObjCl.Execute then
  begin
    AssignFile(TempProjectFile,OpnDlgAddObjCl.FileName);
    try
      Reset(TempProjectFile);
      Read(TempProjectFile,TempProject);
    except
      CloseFile(TempProjectFile);
      Application.MessageBox(PChar('���� ����� ����������� ������'+#13
          +OpnDlgAddObjCl.FileName),PChar('���'),mb_OK+MB_ICONERROR);
      Exit;
    end;
    for i:=1 to TempProject.ProjClassesCount do
    begin
      with TempProject.ProjClasses[i] do
      begin
        OldClassID:=ClassID;
        NewClassID:=AddClass(ClassName1, ObjName, V0, AngleV, F0, AngleF,
            Radius,Mass, FillColor, BorderColor, BorderWidth, Val1, Val2, Val3,
            Val4, Val5, Val6, Int1, Int2, Int3, Int4, Int5, Int6, Int7, Int8,
            Int9, Int10, Int11, Int12, Int13, Int14, Ch1, Ch2, Ch3);
      end;
      for j:=1 to TempProject.ProjObjectsCount do
        with TempProject.ProjObjects[j] do
          if ClassID = OldClassID then
            AddObject(NewClassID, PartName, V, AngleV, F, AngleF, Radius, Mass,
                x0, y0, FillColor, BorderColor, BorderWidth);
    end;
    CloseFile(TempProjectFile);
    Changed:=True;
    ReloadObjectList;
    RedrawImage;
  end;
end;

procedure TFMain.aAddObjectInRegionExecute(Sender: TObject);
begin
  FDlgObjAddRegion.ShowModal;
end;

procedure TFMain.aAddClassFromFileExecute(Sender: TObject);
var
  i: Integer;
begin
  aStop.Execute;
  if OpnDlgAddObjCl.Execute then
  begin
    AssignFile(TempProjectFile,OpnDlgAddObjCl.FileName);
    try
      Reset(TempProjectFile);
      Read(TempProjectFile,TempProject);
    except
      CloseFile(TempProjectFile);
      Application.MessageBox(PChar('���� ����� ����������� ������'+#13
          +OpnDlgAddObjCl.FileName),PChar('���'),mb_OK+MB_ICONERROR);
      Exit;
    end;
    for i:=1 to TempProject.ProjClassesCount do
    begin
      with TempProject.ProjClasses[i] do
        AddClass(ClassName1, ObjName, V0, AngleV, F0, AngleF, Radius, Mass,
            FillColor, BorderColor, BorderWidth, Val1, Val2, Val3, Val4, Val5,
            Val6, Int1, Int2, Int3, Int4, Int5, Int6, Int7, Int8, Int9, Int10,
            Int11, Int12, Int13, Int14, Ch1, Ch2, Ch3);
    end;
    CloseFile(TempProjectFile);
    Changed:=True;
    RedrawImage;
  end;
end;

procedure TFMain.aExportBMPExecute(Sender: TObject);
begin
  SvDlgExp.Filter:='BMP Images (*.bmp)|*.bmp';
  SvDlgExp.DefaultExt:='bmp';
  if SvDlgExp.Execute then
    FView.Image1.Picture.SaveToFile(SvDlgExp.FileName);
end;

procedure TFMain.aExportJPEGExecute(Sender: TObject);
var
  JpegIm: TJpegImage;
begin
  SvDlgExp.Filter:='JPEG Images (*.jpg)|*.jpg';
  SvDlgExp.DefaultExt:='jpg';
  if SvDlgExp.Execute then
  begin
    FDlgJPEG.ShowModal;
    if FDlgJPEG.ModalResult = idOK then
    begin
      JpegIm:=TJpegImage.Create;
      JpegIm.Assign(FView.Image1.Picture.Graphic);
      JpegIm.CompressionQuality:=FDlgJPEG.CompValue;
      JpegIm.Compress;
      JpegIm.SaveToFile(SvDlgExp.FileName);
      JpegIm.Free;
    end;
  end;
end;

procedure TFMain.aContentsExecute(Sender: TObject);
var
  SF: String;
begin
  SF:=ExtractFilePath(ParamStr(0))+'MIG Help.chm';
  if FileExists(SF) then
    ShellExecute(0,'',PChar(SF),'','',1)
  else
    Application.MessageBox(PChar('������ ��� ������ ����� ��� ���� �� ������'+#13+SF),PChar('���'),MB_ICONERROR+MB_OK);
end;

procedure TFMain.aScale50Execute(Sender: TObject);
var
  FS: TWindowState;
begin
  aScale50.Checked:=True;
  aScale100.Checked:=False;
  aScale150.Checked:=False;
  aScale200.Checked:=False;
  aScale400.Checked:=False;
  aScaleIn.Enabled:=True;
  aScaleOut.Enabled:=False;
  Scale:=0.5;
  FS:=FView.WindowState;
  FView.WindowState:=wsNormal;
  FView.Image1.Picture:=nil;
  FView.Image1.Width:=Round(Project.ProjWidth * Scale);
  FView.Image1.Height:=Round(Project.ProjHeight * Scale);
  FView.ClientWidth:=FView.Image1.Width;
  FView.ClientHeight:=FView.Image1.Height;
  FView.WindowState:=FS;
  RedrawImage;
end;

procedure TFMain.aScale100Execute(Sender: TObject);
var
  FS: TWindowState;
begin
  aScale50.Checked:=False;
  aScale100.Checked:=True;
  aScale150.Checked:=False;
  aScale200.Checked:=False;
  aScale400.Checked:=False;
  aScaleIn.Enabled:=True;
  aScaleOut.Enabled:=True;
  Scale:=1;
  FS:=FView.WindowState;
  FView.WindowState:=wsNormal;
  FView.Image1.Picture:=nil;
  FView.Image1.Width:=Round(Project.ProjWidth * Scale);
  FView.Image1.Height:=Round(Project.ProjHeight * Scale);
  FView.ClientWidth:=FView.Image1.Width;
  FView.ClientHeight:=FView.Image1.Height;
  FView.WindowState:=FS;
  RedrawImage;
end;

procedure TFMain.aScale150Execute(Sender: TObject);
var
  FS: TWindowState;
begin
  aScale50.Checked:=False;
  aScale100.Checked:=False;
  aScale150.Checked:=True;
  aScale200.Checked:=False;
  aScale400.Checked:=False;
  aScaleIn.Enabled:=True;
  aScaleOut.Enabled:=True;
  Scale:=1.5;
  FS:=FView.WindowState;
  FView.WindowState:=wsNormal;
  FView.Image1.Picture:=nil;
  FView.Image1.Width:=Round(Project.ProjWidth * Scale);
  FView.Image1.Height:=Round(Project.ProjHeight * Scale);
  FView.ClientWidth:=FView.Image1.Width;
  FView.ClientHeight:=FView.Image1.Height;
  FView.WindowState:=FS;
  RedrawImage;
end;

procedure TFMain.aScale200Execute(Sender: TObject);
var
  FS: TWindowState;
begin
  aScale50.Checked:=False;
  aScale100.Checked:=False;
  aScale150.Checked:=False;
  aScale200.Checked:=True;
  aScale400.Checked:=False;
  aScaleIn.Enabled:=True;
  aScaleOut.Enabled:=True;
  Scale:=2;
  FS:=FView.WindowState;
  FView.WindowState:=wsNormal;
  FView.Image1.Picture:=nil;
  FView.Image1.Width:=Round(Project.ProjWidth * Scale);
  FView.Image1.Height:=Round(Project.ProjHeight * Scale);
  FView.ClientWidth:=FView.Image1.Width;
  FView.ClientHeight:=FView.Image1.Height;
  FView.WindowState:=FS;
  RedrawImage;
end;

procedure TFMain.aScale400Execute(Sender: TObject);
var
  FS: TWindowState;
begin
  aScale50.Checked:=False;
  aScale100.Checked:=False;
  aScale150.Checked:=False;
  aScale200.Checked:=False;
  aScale400.Checked:=True;
  aScaleIn.Enabled:=False;
  aScaleOut.Enabled:=True;
  Scale:=4;
  FS:=FView.WindowState;
  FView.WindowState:=wsNormal;
  FView.Image1.Picture:=nil;
  FView.Image1.Width:=Round(Project.ProjWidth * Scale);
  FView.Image1.Height:=Round(Project.ProjHeight * Scale);
  FView.ClientWidth:=FView.Image1.Width;
  FView.ClientHeight:=FView.Image1.Height;
  FView.WindowState:=FS;
  RedrawImage;
end;

procedure TFMain.aScaleInExecute(Sender: TObject);
begin
  if Scale = 0.5 then
    aScale100.Execute
  else
    if Scale = 1 then
      aScale150.Execute
    else
      if Scale = 1.5 then
        aScale200.Execute
      else
        if Scale = 2 then
          aScale400.Execute
end;

procedure TFMain.aScaleOutExecute(Sender: TObject);
begin
  if Scale = 4 then
    aScale200.Execute
  else
    if Scale = 2 then
      aScale150.Execute
    else
      if Scale = 1.5 then
        aScale100.Execute
      else
        if Scale = 1 then
          aScale50.Execute
end;


end.
