unit uFPanelRes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Spin, StdCtrls, Buttons, ExtCtrls, Math, ComCtrls;

type
  TFPanelRes = class(TForm)
    Timer1: TTimer;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label12: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label14: TLabel;
    Label6: TLabel;
    Label15: TLabel;
    Edit1: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit3: TEdit;
    Edit2: TEdit;
    TabSheet2: TTabSheet;
    Edit4: TEdit;
    Label4: TLabel;
    Edit10: TEdit;
    Edit9: TEdit;
    Label5: TLabel;
    Label7: TLabel;
    Panel2: TPanel;
    ChBxEn: TCheckBox;
    ComboBox1: TComboBox;
    GroupBox1: TGroupBox;
    TrackBar1: TTrackBar;
    Label13: TLabel;
    Label8: TLabel;
    Edit11: TEdit;
    Label9: TLabel;
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ChBxEnClick(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
  private
    { Private declarations }
  public
    procedure GetValues;
    procedure ClearValues;
  end;

var
  FPanelRes: TFPanelRes;

implementation

uses uFMain, uFPanelObjects;
                                      
{$R *.dfm}

{-----------------------------------------------------------------------------}
{  procedure     GetValues                                                    }
{                                                                             }
{  discription   ���������� �������� ���������� ������ � ����������� ��       }
{-----------------------------------------------------------------------------}
procedure TFPanelRes.GetValues;
const
  k = 1.380650524E-23;
var
  i, c: Integer;
  VCur, VV, AngVV, FF, AngFF, RR, MM, Es, Ea: Real;
begin
  c:=0;
  VV:=0;
  AngVV:=0;
  FF:=0;
  AngFF:=0;
  RR:=0;
  MM:=0;
  Es:=0;
  for i:=1 to FMain.Project.ProjObjectsCount do
  begin
    with FMain.Project.ProjObjects[i] do
    begin
      if ClassID = ComboBox1.ItemIndex+1 then
      begin
        VCur:=Sqrt(Vx*Vx+Vy*Vy);
        VV:=VV+VCur;
        AngVV:=AngVV+ArcTan2(Vy,Vx);
        FF:=FF+Sqrt(Fx*Fx+Fy*Fy);
        AngFF:=AngFF+ArcTan2(Fy,Fx);
        RR:=RR+Radius;
        MM:=MM+Mass;
        Es:=Es+VCur*VCur*Mass/2;
        Inc(c);
      end;
    end;
  end;
  if c<>0 then
  begin
    Ea:=Es/c;
    Edit1.Text:=Format('%.4g',[VV/c]);
    Edit2.Text:=Format('%.4g',[RadToDeg(AngVV/c)]);
    Edit3.Text:=Format('%.4g',[FF/c]);
    Edit4.Text:=Format('%.4g',[RadToDeg(AngFF/c)]);
    Edit5.Text:=Format('%.4g',[RR/c]);
    Edit6.Text:=Format('%.4g',[MM/c]);
    Edit9.Text:=Format('%.4g',[Ea]);
    Edit10.Text:=Format('%.4g',[Es]);
    Edit7.Text:=Format('%.4g',[2*Ea/k/3]);
  end
  else
  begin
    Edit1.Text:='<�������� ���>';
    Edit2.Text:='<�������� ���>';
    Edit3.Text:='<�������� ���>';
    Edit4.Text:='<�������� ���>';
    Edit5.Text:='<�������� ���>';
    Edit6.Text:='<�������� ���>';
    Edit7.Text:='<�������� ���>';
    Edit9.Text:='<�������� ���>';
    Edit10.Text:='<�������� ���>';
  end;
  Edit11.Text:=IntToStr(c);
end;

{-----------------------------------------------------------------------------}
{  procedure     ClearValues                                                  }
{                                                                             }
{  discription   �������� ���� �����                                          }
{-----------------------------------------------------------------------------}
procedure TFPanelRes.ClearValues;
begin
  Edit1.Text:='';
  Edit2.Text:='';
  Edit3.Text:='';
  Edit4.Text:='';
  Edit5.Text:='';
  Edit6.Text:='';
  Edit7.Text:='';
  Edit9.Text:='';
  Edit10.Text:='';
  Edit11.Text:='';
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             FPanelRes                                      }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFPanelRes.FormCreate(Sender: TObject);
var
  Rect: TRect;
begin
  SystemParametersInfo(SPI_GETWORKAREA,0,@Rect,0);
  Top:=FMain.cfg.ReadInteger('PanelTracking','Top',Rect.Top + 70);
  Left:=FMain.cfg.ReadInteger('PanelTracking','Left',Rect.Right - FPanelRes.Width - 3);
  Visible:=FMain.cfg.ReadBool('PanelTracking','Visible',False);
end;

procedure TFPanelRes.FormShow(Sender: TObject);
begin
  FMain.aShowRes.Checked:=True;
end;

procedure TFPanelRes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FMain.aShowRes.Checked:=False;
  ChBxEn.Checked:=False;
  Timer1.Enabled:=False;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             <Other procedures>                              }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFPanelRes.Timer1Timer(Sender: TObject);
begin
  GetValues;
end;

procedure TFPanelRes.ChBxEnClick(Sender: TObject);
begin
  if ComboBox1.ItemIndex=-1 then
    ComboBox1.ItemIndex:=0;
  Timer1.Enabled:=ChBxEn.Checked;
  if ChBxEn.Checked then
  begin
    GetValues;
    PageControl1.TabIndex:=0;
  end;
end;

procedure TFPanelRes.TrackBar1Change(Sender: TObject);
begin
  Timer1.Interval:=TrackBar1.Position*100;
  Label8.Caption:=IntToStr(Timer1.Interval)+' ��';
end;

procedure TFPanelRes.ComboBox1Change(Sender: TObject);
begin
  GetValues;
end;

end.
