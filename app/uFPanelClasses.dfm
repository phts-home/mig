object FPanelClasses: TFPanelClasses
  Left = 187
  Top = 370
  Width = 170
  Height = 157
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = #1050#1083#1072#1089#1089#1099
  Color = clBtnFace
  Constraints.MaxHeight = 1000
  Constraints.MaxWidth = 170
  Constraints.MinHeight = 150
  Constraints.MinWidth = 170
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object CheckListBox1: TCheckListBox
    Left = 5
    Top = 30
    Width = 152
    Height = 86
    ItemHeight = 13
    TabOrder = 0
    OnClick = CheckListBox1Click
    OnDblClick = CheckListBox1DblClick
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 162
    Height = 29
    Caption = 'ToolBar1'
    Images = FMain.ImageList1
    TabOrder = 1
    object ToolButton1: TToolButton
      Left = 0
      Top = 2
      Action = FMain.aAddClass
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton2: TToolButton
      Left = 23
      Top = 2
      Action = FMain.aDublClass
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton3: TToolButton
      Left = 46
      Top = 2
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 54
      Top = 2
      Action = FMain.aEditClass
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton7: TToolButton
      Left = 77
      Top = 2
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 23
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 85
      Top = 2
      Action = FMain.aDelClass
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton6: TToolButton
      Left = 108
      Top = 2
      Action = FMain.aDelObjectsOfClass
      ParentShowHint = False
      ShowHint = True
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 114
    Width = 156
    Height = 17
    Align = alCustom
    Panels = <>
    SimplePanel = True
    SimpleText = #1042#1089#1077#1075#1086': 0'
  end
end
