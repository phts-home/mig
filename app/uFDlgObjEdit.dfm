object FDlgObjEdit: TFDlgObjEdit
  Left = 189
  Top = 105
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
  ClientHeight = 258
  ClientWidth = 516
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 15
    Top = 80
    Width = 48
    Height = 13
    Caption = #1057#1082#1086#1088#1086#1089#1090#1100
  end
  object Label2: TLabel
    Left = 15
    Top = 110
    Width = 28
    Height = 13
    Caption = #1059#1075#1086#1083':'
  end
  object Label5: TLabel
    Left = 375
    Top = 80
    Width = 16
    Height = 13
    Caption = 'X0:'
  end
  object Label6: TLabel
    Left = 375
    Top = 110
    Width = 16
    Height = 13
    Caption = 'Y0:'
  end
  object Label15: TLabel
    Left = 195
    Top = 80
    Width = 39
    Height = 13
    Caption = #1056#1072#1076#1080#1091#1089':'
  end
  object Label16: TLabel
    Left = 195
    Top = 110
    Width = 36
    Height = 13
    Caption = #1052#1072#1089#1089#1072':'
  end
  object Label7: TLabel
    Left = 15
    Top = 185
    Width = 28
    Height = 13
    Caption = #1059#1075#1086#1083':'
  end
  object Label3: TLabel
    Left = 15
    Top = 155
    Width = 68
    Height = 13
    Caption = #1044#1077#1081#1089#1090#1074'.'#1089#1080#1083#1072':'
  end
  object Label4: TLabel
    Left = 15
    Top = 20
    Width = 25
    Height = 13
    Caption = #1048#1084#1103':'
  end
  object Bevel1: TBevel
    Left = 15
    Top = 55
    Width = 481
    Height = 11
    Shape = bsTopLine
  end
  object Label8: TLabel
    Left = 190
    Top = 155
    Width = 46
    Height = 13
    Caption = #1047#1072#1083#1080#1074#1082#1072':'
  end
  object Label9: TLabel
    Left = 355
    Top = 155
    Width = 38
    Height = 13
    Caption = #1050#1086#1085#1090#1091#1088':'
  end
  object Label10: TLabel
    Left = 195
    Top = 20
    Width = 34
    Height = 13
    Caption = #1050#1083#1072#1089#1089':'
  end
  object EdSAngl: TEdit
    Left = 85
    Top = 105
    Width = 100
    Height = 21
    TabOrder = 3
    Text = '30'
    OnClick = EdNameClick
  end
  object EdSpeed: TEdit
    Left = 85
    Top = 75
    Width = 100
    Height = 21
    TabOrder = 2
    Text = '1'
    OnClick = EdNameClick
  end
  object EdAcc: TEdit
    Left = 85
    Top = 150
    Width = 100
    Height = 21
    TabOrder = 4
    Text = '0'
    OnClick = EdNameClick
  end
  object EdAAngl: TEdit
    Left = 85
    Top = 180
    Width = 100
    Height = 21
    TabOrder = 5
    Text = '30'
    OnClick = EdNameClick
  end
  object EdName: TEdit
    Left = 85
    Top = 15
    Width = 100
    Height = 21
    MaxLength = 10
    TabOrder = 0
    Text = 'Circle'
    OnClick = EdNameClick
  end
  object EdMass: TEdit
    Left = 245
    Top = 105
    Width = 100
    Height = 21
    TabOrder = 6
    Text = '3'
    OnClick = EdNameClick
  end
  object Edx0: TEdit
    Left = 395
    Top = 75
    Width = 100
    Height = 21
    TabOrder = 7
    Text = '30'
    OnClick = EdNameClick
  end
  object Edy0: TEdit
    Left = 395
    Top = 105
    Width = 100
    Height = 21
    TabOrder = 8
    Text = '30'
    OnClick = EdNameClick
  end
  object Button4: TButton
    Left = 85
    Top = 220
    Width = 30
    Height = 25
    Caption = '>'
    TabOrder = 17
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 50
    Top = 220
    Width = 30
    Height = 25
    Caption = '<'
    TabOrder = 16
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 120
    Top = 220
    Width = 30
    Height = 25
    Caption = '>>'
    TabOrder = 18
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 15
    Top = 220
    Width = 30
    Height = 25
    Caption = '<<'
    TabOrder = 15
    OnClick = Button7Click
  end
  object Button1: TButton
    Left = 240
    Top = 220
    Width = 75
    Height = 25
    Caption = 'Ok'
    Default = True
    ModalResult = 1
    TabOrder = 12
  end
  object Button2: TButton
    Left = 330
    Top = 220
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 13
  end
  object Button8: TButton
    Left = 420
    Top = 220
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
    TabOrder = 14
    OnClick = Button8Click
  end
  object ColorBox1: TColorBox
    Left = 245
    Top = 150
    Width = 100
    Height = 22
    DefaultColorColor = clYellow
    NoneColorColor = clYellow
    Selected = clYellow
    Style = [cbStandardColors, cbPrettyNames]
    ItemHeight = 16
    TabOrder = 9
  end
  object ColorBox2: TColorBox
    Left = 435
    Top = 150
    Width = 61
    Height = 22
    Style = [cbStandardColors, cbPrettyNames]
    ItemHeight = 16
    TabOrder = 11
  end
  object SpinEdit1: TSpinEdit
    Left = 395
    Top = 150
    Width = 36
    Height = 22
    MaxValue = 9
    MinValue = 1
    TabOrder = 10
    Value = 1
  end
  object ComboBox1: TComboBox
    Left = 245
    Top = 15
    Width = 100
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    OnChange = ComboBox1Change
  end
  object SpEditRad: TSpinEdit
    Left = 245
    Top = 75
    Width = 100
    Height = 22
    MaxValue = 100
    MinValue = 1
    TabOrder = 19
    Value = 30
  end
end
