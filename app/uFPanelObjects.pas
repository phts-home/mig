unit uFPanelObjects;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, ComCtrls, ToolWin, ExtCtrls;

type
  TFPanelObjects = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton7: TToolButton;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    ListView1: TListView;
    StatusBar1: TStatusBar;
    ToolButton8: TToolButton;
    ComboBox1: TComboBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure ListView1SelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure ListView1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
  private
    { Private declarations }
  public
    IterCount: Integer;
    OldCol, OldBord: TColor;
  end;

var
  FPanelObjects: TFPanelObjects;

implementation

uses uFMain, uFPanelProp, uFPanelClasses, uFView, uFDlgProjOpt;

{$R *.dfm}

{-----------------------------------------------------------------------------}
{                                                                             }
{                               FPanelObjects                                 }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFPanelObjects.FormCreate(Sender: TObject);
var
  Rect: TRect;
begin
  SystemParametersInfo(SPI_GETWORKAREA,0,@Rect,0);
  Top:=FMain.cfg.ReadInteger('PanelObjects','Top',FPanelClasses.Height + Rect.Top + 70);
  Left:=FMain.cfg.ReadInteger('PanelObjects','Left',Rect.Left + 3);
  Height:=FMain.cfg.ReadInteger('PanelObjects','Height',217);
  Visible:=FMain.cfg.ReadBool('PanelObjects','Visible',True);
end;

procedure TFPanelObjects.FormResize(Sender: TObject);
begin
  StatusBar1.Width:=ClientWidth;
  StatusBar1.Top:=ClientHeight - StatusBar1.Height;
  ListView1.Height:=ClientHeight - 78;
end;

procedure TFPanelObjects.FormShow(Sender: TObject);
begin
  FMain.aShowObjects.Checked:=True;
end;

procedure TFPanelObjects.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FMain.aShowObjects.Checked:=False;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             <Other procedures>                              }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFPanelObjects.ListView1DblClick(Sender: TObject);
begin
  FMain.aEditObject.Execute;
end;

procedure TFPanelObjects.ListView1SelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  FMain.ActiveObject:=StrToInt(Item.SubItems[1]);
  try
    FPanelProp.LoadProperties(FMain.ActiveObject);
  except end;

  FMain.RedrawImage;
end;

procedure TFPanelObjects.ListView1Click(Sender: TObject);
begin
  if ListView1.ItemIndex=-1 then
  begin
    FPanelProp.UnloadProperties;
    FMain.ActiveObject:=0;
    FMain.RedrawImage;
  end;
end;



procedure TFPanelObjects.ComboBox1Change(Sender: TObject);
begin
  FMain.ReloadObjectList;
  if FDlgProjOpt.Showing then
    FPanelObjects.ListView1.Items:=ListView1.Items;
end;

end.
