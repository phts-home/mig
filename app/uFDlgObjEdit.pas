unit uFDlgObjEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Math, Spin;

type
  TFDlgObjEdit = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label7: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Bevel1: TBevel;
    EdSAngl: TEdit;
    EdSpeed: TEdit;
    EdAcc: TEdit;
    EdAAngl: TEdit;
    EdName: TEdit;
    EdMass: TEdit;
    Edx0: TEdit;
    Edy0: TEdit;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button1: TButton;
    Button2: TButton;
    Button8: TButton;
    Label8: TLabel;
    ColorBox1: TColorBox;
    Label9: TLabel;
    ColorBox2: TColorBox;
    SpinEdit1: TSpinEdit;
    Label10: TLabel;
    ComboBox1: TComboBox;
    SpEditRad: TSpinEdit;
    procedure Button7Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdNameClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDlgObjEdit: TFDlgObjEdit;

implementation

uses uFMain, uFPanelObjects, uFPanelProp;

{$R *.dfm}

procedure TFDlgObjEdit.Button7Click(Sender: TObject);
begin
  if FMain.Project.ProjObjectsCount <> 0 then
  begin
    FPanelObjects.ListView1.ItemIndex:=0;
    with FMain.Project.ProjObjects[1] do
    begin
      EdName.Text:=PartName;
      EdSpeed.Text:=FloatToStr(V);
      EdSAngl.Text:=FloatToStr(RadToDeg(AngleV));
      EdAcc.Text:=FloatToStr(F);
      EdAAngl.Text:=FloatToStr(RadToDeg(AngleF));
      EdMass.Text:=FloatToStr(Mass);
      SpEditRad.Value:=Radius;
      Edx0.Text:=FloatToStr(x0);
      Edy0.Text:=FloatToStr(y0);
      ColorBox1.Selected:=FillColor;
      ColorBox2.Selected:=BorderColor;
      SpinEdit1.Value:=BorderWidth;
    end;
  end;
end;

procedure TFDlgObjEdit.Button5Click(Sender: TObject);
begin
  if FMain.Project.ProjObjectsCount <> 0 then
  begin
    if FPanelObjects.ListView1.ItemIndex > 0 then
      FPanelObjects.ListView1.ItemIndex:=FPanelObjects.ListView1.ItemIndex-1;
    with FMain.Project.ProjObjects[FMain.ActiveObject] do
    begin
      EdName.Text:=PartName;
      EdSpeed.Text:=FloatToStr(V);
      EdSAngl.Text:=FloatToStr(RadToDeg(AngleV));
      EdAcc.Text:=FloatToStr(F);
      EdAAngl.Text:=FloatToStr(RadToDeg(AngleF));
      EdMass.Text:=FloatToStr(Mass);
      SpEditRad.Value:=Radius;
      Edx0.Text:=FloatToStr(x0);
      Edy0.Text:=FloatToStr(y0);
      ColorBox1.Selected:=FillColor;
      ColorBox2.Selected:=BorderColor;
      SpinEdit1.Value:=BorderWidth;
    end;
  end;
end;

procedure TFDlgObjEdit.Button4Click(Sender: TObject);
begin
  if FMain.Project.ProjObjectsCount <> 0 then
  begin
    if FPanelObjects.ListView1.ItemIndex < FPanelObjects.ListView1.Items.Count-1 then
      FPanelObjects.ListView1.ItemIndex:=FPanelObjects.ListView1.ItemIndex+1;
    with FMain.Project.ProjObjects[FMain.ActiveObject] do
    begin
      EdName.Text:=PartName;
      EdSpeed.Text:=FloatToStr(V);
      EdSAngl.Text:=FloatToStr(RadToDeg(AngleV));
      EdAcc.Text:=FloatToStr(F);
      EdAAngl.Text:=FloatToStr(RadToDeg(AngleF));
      EdMass.Text:=FloatToStr(Mass);
      SpEditRad.Value:=Radius;
      Edx0.Text:=FloatToStr(x0);
      Edy0.Text:=FloatToStr(y0);
      ColorBox1.Selected:=FillColor;
      ColorBox2.Selected:=BorderColor;
      SpinEdit1.Value:=BorderWidth;
    end;
  end;
end;

procedure TFDlgObjEdit.Button6Click(Sender: TObject);
begin
  if FMain.Project.ProjObjectsCount <> 0 then
  begin
    FPanelObjects.ListView1.ItemIndex:=FPanelObjects.ListView1.Items.Count-1;
    with FMain.Project.ProjObjects[FMain.ActiveObject] do
    begin
      EdName.Text:=PartName;
      EdSpeed.Text:=FloatToStr(V);
      EdSAngl.Text:=FloatToStr(RadToDeg(AngleV));
      EdAcc.Text:=FloatToStr(F);
      EdAAngl.Text:=FloatToStr(RadToDeg(AngleF));
      EdMass.Text:=FloatToStr(Mass);
      SpEditRad.Value:=Radius;
      Edx0.Text:=FloatToStr(x0);
      Edy0.Text:=FloatToStr(y0);
      ColorBox1.Selected:=FillColor;
      ColorBox2.Selected:=BorderColor;
      SpinEdit1.Value:=BorderWidth;
    end;
  end;
end;

procedure TFDlgObjEdit.Button8Click(Sender: TObject);
begin
  FMain.ModifyObject(FMain.ActiveObject,ComboBox1.ItemIndex+1,EdName.Text,
    StrToFloat(EdSpeed.Text),DegToRad(StrToFloat(EdSAngl.Text)),
    StrToFloat(EdAcc.Text),DegToRad(StrToFloat(EdAAngl.Text)),
    SpEditRad.Value,StrToFloat(EdMass.Text),StrToFloat(Edx0.Text),
    StrToFloat(Edy0.Text),
    FMain.Project.ProjObjects[FMain.ActiveObject].x,
    FMain.Project.ProjObjects[FMain.ActiveObject].y,
    ColorBox1.Selected,
    ColorBox2.Selected,SpinEdit1.Value);
  FPanelProp.LoadProperties(FMain.ActiveObject);
end;

procedure TFDlgObjEdit.ComboBox1Change(Sender: TObject);
begin
  with FMain.Project.ProjClasses[ComboBox1.ItemIndex+1] do
  begin
    EdName.Text:=ObjName;
    if Val1 = 0 then
      EdSpeed.Text:=FloatToStr(V0)
    else
      EdSpeed.Text:=FloatToStr((Random(Int2*10000-Int1*10000)+Int1*10000)/10000);

    if Val2 = 0 then
      EdSAngl.Text:=FloatToStr(RadToDeg(AngleV))
    else
      EdSAngl.Text:=FloatToStr((Random(Int4*10000-Int3*10000)+Int3*10000)/10000);

    if Val3 = 0 then
      EdAcc.Text:=FloatToStr(F0)
    else
      EdAcc.Text:=FloatToStr((Random(Int6*10000-Int5*10000)+Int5*10000)/10000);

    if Val4 = 0 then
      EdAAngl.Text:=FloatToStr(RadToDeg(AngleF))
    else
      EdAAngl.Text:=FloatToStr((Random(Int8*10000-Int7*10000)+Int7*10000)/10000);

    if Val5 = 0 then
      SpEditRad.Value:=Radius
    else
      SpEditRad.Value:=Random(Int10-Int9+1)+Int9;

    if Val6 = 0 then
      EdMass.Text:=FloatToStr(Mass)
    else
      EdMass.Text:=FloatToStr((Random(Int12*10000-Int11*10000)+Int11*10000)/10000);

    if not Ch1 then
      ColorBox1.Selected:=FillColor
    else
      ColorBox1.Selected:=ColorList[Random(16)];

    if not Ch2 then
      ColorBox2.Selected:=BorderColor
    else
      ColorBox2.Selected:=ColorList[Random(16)];

    if not Ch3 then
      SpinEdit1.Value:=BorderWidth
    else
      SpinEdit1.Value:=Random(Int14-Int13)+Int13;
  end; 
end;

procedure TFDlgObjEdit.FormShow(Sender: TObject);
begin
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
  EdName.SetFocus;
end;

procedure TFDlgObjEdit.EdNameClick(Sender: TObject);
begin
  (Sender as TEdit).SelectAll;
end;

end.
