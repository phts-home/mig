object FPanelObjects: TFPanelObjects
  Left = 190
  Top = 109
  Width = 170
  Height = 238
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = #1054#1073#1098#1077#1082#1090#1099
  Color = clBtnFace
  Constraints.MaxHeight = 1000
  Constraints.MaxWidth = 170
  Constraints.MinHeight = 150
  Constraints.MinWidth = 170
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 162
    Height = 29
    Caption = 'ToolBar1'
    Images = FMain.ImageList1
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 2
      Action = FMain.aAddObject
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton7: TToolButton
      Left = 23
      Top = 2
      Action = FMain.aDublObject
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton6: TToolButton
      Left = 46
      Top = 2
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 18
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 2
      Action = FMain.aEditObject
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton8: TToolButton
      Left = 77
      Top = 2
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 18
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 85
      Top = 2
      Action = FMain.aDelObject
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton4: TToolButton
      Left = 108
      Top = 2
      Action = FMain.aDelAllObjects
      ParentShowHint = False
      ShowHint = True
    end
  end
  object ListView1: TListView
    Left = 5
    Top = 55
    Width = 152
    Height = 136
    Columns = <
      item
        Caption = #1050#1083#1072#1089#1089
        Width = 65
      end
      item
        Caption = #1054#1073#1098#1077#1082#1090
        Width = 65
      end>
    ColumnClick = False
    FlatScrollBars = True
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 1
    ViewStyle = vsReport
    OnClick = ListView1Click
    OnDblClick = ListView1DblClick
    OnSelectItem = ListView1SelectItem
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 179
    Width = 151
    Height = 17
    Align = alCustom
    Panels = <>
    SimplePanel = True
    SimpleText = #1042#1089#1077#1075#1086': 0 (0)'
  end
  object ComboBox1: TComboBox
    Left = 5
    Top = 28
    Width = 152
    Height = 22
    Style = csOwnerDrawFixed
    ItemHeight = 16
    TabOrder = 3
    OnChange = ComboBox1Change
  end
end
