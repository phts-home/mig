object FPanelProp: TFPanelProp
  Left = 189
  Top = 384
  Width = 170
  Height = 277
  HorzScrollBar.Visible = False
  AutoSize = True
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = #1057#1074#1086#1081#1089#1090#1074#1072
  Color = clBtnFace
  Constraints.MaxHeight = 277
  Constraints.MaxWidth = 170
  Constraints.MinHeight = 150
  Constraints.MinWidth = 170
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ScrollBox1: TScrollBox
    Left = 0
    Top = 0
    Width = 162
    Height = 248
    BorderStyle = bsNone
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 0
    object StringGrid1: TStringGrid
      Left = 5
      Top = 5
      Width = 152
      Height = 238
      ColCount = 2
      Ctl3D = False
      DefaultColWidth = 74
      DefaultRowHeight = 17
      Enabled = False
      RowCount = 13
      FixedRows = 0
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
      ParentCtl3D = False
      TabOrder = 0
      OnKeyPress = StringGrid1KeyPress
      OnSelectCell = StringGrid1SelectCell
      RowHeights = (
        17
        17
        17
        17
        17
        17
        17
        17
        17
        17
        17
        17
        17)
    end
    object SpinEdit1: TSpinEdit
      Left = 80
      Top = 221
      Width = 76
      Height = 22
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxValue = 9
      MinValue = 1
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      Value = 1
      Visible = False
      OnChange = SpinEdit1Change
    end
    object ColorBox2: TColorBox
      Left = 80
      Top = 203
      Width = 76
      Height = 19
      Style = [cbStandardColors, cbPrettyNames]
      BevelInner = bvNone
      BevelOuter = bvNone
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
      Visible = False
      OnChange = ColorBox2Change
    end
    object ColorBox1: TColorBox
      Left = 80
      Top = 185
      Width = 76
      Height = 19
      Style = [cbStandardColors, cbPrettyNames]
      BevelInner = bvNone
      BevelOuter = bvNone
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 3
      Visible = False
      OnChange = ColorBox1Change
    end
    object ComboBox1: TComboBox
      Left = 80
      Top = 5
      Width = 76
      Height = 19
      BevelInner = bvNone
      BevelOuter = bvNone
      Style = csOwnerDrawFixed
      Ctl3D = False
      DropDownCount = 5
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
      Visible = False
      OnChange = ComboBox1Change
    end
  end
end
