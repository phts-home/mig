object FPanelRes: TFPanelRes
  Left = 508
  Top = 277
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = #1057#1083#1077#1078#1077#1085#1080#1077' '#1079#1072' '#1082#1083#1072#1089#1089#1086#1084
  ClientHeight = 153
  ClientWidth = 418
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 5
    Top = 5
    Width = 410
    Height = 145
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1057#1083#1077#1078#1077#1085#1080#1077
      object Label12: TLabel
        Left = 10
        Top = 10
        Width = 19
        Height = 13
        Caption = '<V>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 150
        Top = 10
        Width = 21
        Height = 13
        Caption = '< r >'
      end
      object Label2: TLabel
        Left = 150
        Top = 35
        Width = 20
        Height = 13
        Caption = '<m>'
      end
      object Label14: TLabel
        Left = 280
        Top = 60
        Width = 19
        Height = 13
        Caption = '<T>'
      end
      object Label6: TLabel
        Left = 10
        Top = 60
        Width = 18
        Height = 13
        Caption = '<F>'
      end
      object Label15: TLabel
        Left = 10
        Top = 35
        Width = 38
        Height = 13
        Caption = '<AngV>'
      end
      object Label4: TLabel
        Left = 10
        Top = 85
        Width = 37
        Height = 13
        Caption = '<AngF>'
      end
      object Label5: TLabel
        Left = 280
        Top = 10
        Width = 19
        Height = 13
        Caption = '<E>'
      end
      object Label7: TLabel
        Left = 280
        Top = 35
        Width = 26
        Height = 13
        Caption = 'Esum'
      end
      object Label9: TLabel
        Left = 145
        Top = 85
        Width = 50
        Height = 13
        Caption = #1054#1073#1098#1077#1082#1090#1086#1074
      end
      object Edit1: TEdit
        Left = 50
        Top = 5
        Width = 80
        Height = 21
        TabStop = False
        Color = clInactiveBorder
        ReadOnly = True
        TabOrder = 0
      end
      object Edit5: TEdit
        Left = 176
        Top = 5
        Width = 80
        Height = 21
        TabStop = False
        Color = clInactiveBorder
        ReadOnly = True
        TabOrder = 1
      end
      object Edit6: TEdit
        Left = 176
        Top = 30
        Width = 80
        Height = 21
        TabStop = False
        Color = clInactiveBorder
        ReadOnly = True
        TabOrder = 2
      end
      object Edit7: TEdit
        Left = 312
        Top = 55
        Width = 80
        Height = 21
        TabStop = False
        Color = clInactiveBorder
        ReadOnly = True
        TabOrder = 3
      end
      object Edit3: TEdit
        Left = 50
        Top = 55
        Width = 80
        Height = 21
        TabStop = False
        Color = clInactiveBorder
        ReadOnly = True
        TabOrder = 4
      end
      object Edit2: TEdit
        Left = 50
        Top = 30
        Width = 80
        Height = 21
        TabStop = False
        Color = clInactiveBorder
        ReadOnly = True
        TabOrder = 5
      end
      object Edit4: TEdit
        Left = 50
        Top = 80
        Width = 80
        Height = 21
        TabStop = False
        Color = clInactiveBorder
        ReadOnly = True
        TabOrder = 6
      end
      object Edit10: TEdit
        Left = 312
        Top = 30
        Width = 80
        Height = 21
        TabStop = False
        Color = clInactiveBorder
        ReadOnly = True
        TabOrder = 7
      end
      object Edit9: TEdit
        Left = 312
        Top = 5
        Width = 80
        Height = 21
        TabStop = False
        Color = clInactiveBorder
        ReadOnly = True
        TabOrder = 8
      end
      object Edit11: TEdit
        Left = 205
        Top = 80
        Width = 52
        Height = 21
        TabStop = False
        Color = clInactiveBorder
        ReadOnly = True
        TabOrder = 9
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
      ImageIndex = 1
      object GroupBox1: TGroupBox
        Left = 5
        Top = 15
        Width = 146
        Height = 81
        Caption = ' '#1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '
        TabOrder = 0
        object Label13: TLabel
          Left = 12
          Top = 25
          Width = 52
          Height = 13
          Caption = #1048#1085#1090#1077#1088#1074#1072#1083':'
        end
        object Label8: TLabel
          Left = 75
          Top = 25
          Width = 61
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '100 '#1084#1089
        end
        object TrackBar1: TTrackBar
          Left = 3
          Top = 45
          Width = 140
          Height = 26
          Max = 20
          Min = 1
          Position = 1
          TabOrder = 0
          ThumbLength = 15
          OnChange = TrackBar1Change
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 155
    Top = 5
    Width = 261
    Height = 18
    BevelOuter = bvNone
    TabOrder = 1
    object ChBxEn: TCheckBox
      Left = 215
      Top = 0
      Width = 46
      Height = 17
      Caption = #1042#1082#1083
      TabOrder = 0
      OnClick = ChBxEnClick
    end
    object ComboBox1: TComboBox
      Left = 130
      Top = 0
      Width = 71
      Height = 18
      Style = csOwnerDrawFixed
      ItemHeight = 12
      TabOrder = 1
      OnChange = ComboBox1Change
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 138
    Top = 3
  end
end
