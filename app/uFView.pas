unit uFView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Math;

type
  TFView = class(TForm)
    Image1: TImage;
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Image1DblClick(Sender: TObject);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Image1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    MouseDwn, MouseDrag: Boolean;
    OldX, OldY: Integer;
  end;

var
  FView: TFView;

implementation

uses uFMain, uFPanelObjects, uFPanelProp, ComCtrls;

{$R *.dfm}

procedure TFView.FormCreate(Sender: TObject);
begin
  if FMain.aEnQAdd.Checked then Image1.Cursor:=crCross;
  Left:=170;
  FMain.RedrawImage;
  FMain.LoadDefaultClass;
  FMain.LoadDefaultProject;
  Image1.Canvas.Pen.Width:=1;
end;

procedure TFView.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FMain.Close;
end;

procedure TFView.FormResize(Sender: TObject);
begin
  Image1.Left:=ClientWidth div 2 - Image1.Width div 2;
  Image1.Top:=ClientHeight div 2 - Image1.Height div 2;
  if ClientWidth<Image1.Width then
  begin
    Image1.Left:=0;
    ClientWidth:=Image1.Width;
  end;
  if ClientHeight<Image1.Height then
  begin
    Image1.Top:=0;
    ClientHeight:=Image1.Height;
  end;
end;

procedure TFView.Image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  i, j: Integer;
  Nothing: Boolean;
begin
  MouseDwn:=False;
  Screen.Cursor:=crDefault;
  if FMain.aEnQAdd.Checked then
    FMain.QuickAddObject(Round(X/FMain.Project.ProjScale/FMain.Scale),Round((Image1.Height - Y)/FMain.Project.ProjScale/FMain.Scale));
  if(not FMain.aStart.Checked or FMain.aPause.Checked)and(FMain.aEnQSel.Checked)then
  begin
    Nothing:=True;
    for i:=1 to FMain.Project.ProjObjectsCount do
    begin
      if(Round(X/FMain.Scale) > Round(FMain.Project.ProjObjects[i].x*FMain.Project.ProjScale-FMain.Project.ProjObjects[i].Radius))and
        (Round(X/FMain.Scale) < Round(FMain.Project.ProjObjects[i].x*FMain.Project.ProjScale+FMain.Project.ProjObjects[i].Radius))and
        (Round(FMain.Project.ProjObjects[i].y*FMain.Project.ProjScale+FMain.Project.ProjObjects[i].Radius) > Round((Image1.Height - Y)/FMain.Scale))and
        (Round(FMain.Project.ProjObjects[i].y*FMain.Project.ProjScale-FMain.Project.ProjObjects[i].Radius) < Round((Image1.Height - Y)/FMain.Scale))then
      begin
        FMain.ActiveObject:=i;
        FPanelProp.LoadProperties(FMain.ActiveObject);
        FMain.RedrawImage;
        for j:=0 to FPanelObjects.ListView1.Items.Count-1 do
          if FPanelObjects.ListView1.Items[j].SubItems[1] = IntToStr(FMain.ActiveObject) then
            FPanelObjects.ListView1.ItemIndex:=j;
        Nothing:=False;
        Break;
      end;
    end;
    if Nothing then
    begin
      FMain.aSelNothing.Execute;
    end;
    MouseDwn:=FPanelObjects.ListView1.ItemIndex <> -1;
    MouseDrag:=False;
    if MouseDwn then
    begin
      OldX:=Round(X/FMain.Project.ProjScale/FMain.Scale);
      OldY:=Round((Image1.Height - Y)/FMain.Project.ProjScale/FMain.Scale);
    end;
  end;
end;

procedure TFView.Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if MouseDwn and(Shift=[ssLeft])and((X<>OldX)or(Y<>Image1.Height - OldY))then
  begin
    Screen.Cursor:=crDrag;
    MouseDrag:=True;
  end;
end;

procedure TFView.Image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  NewX, NewY: Integer;
  OldVx, OldVy: Real;
begin
  if MouseDwn and MouseDrag then
  begin
    NewX:=Round(X/FMain.Project.ProjScale/FMain.Scale);
    NewY:=Round((Image1.Height - Y)/FMain.Project.ProjScale/FMain.Scale);
    with FMain.Project.ProjObjects[FMain.ActiveObject] do
    begin
      if FMain.aStart.Checked then
      begin
        OldVx:=FMain.Project.ProjObjects[FMain.ActiveObject].Vx;
        OldVy:=FMain.Project.ProjObjects[FMain.ActiveObject].Vy;
        FMain.ModifyObject(FMain.ActiveObject, ClassID, PartName, V, AngleV, F,
          AngleF, Radius, Mass, x0, y0, x+NewX-OldX, y+NewY-OldY, FillColor,
          BorderColor, BorderWidth);
        FMain.Project.ProjObjects[FMain.ActiveObject].Vx:=OldVx;
        FMain.Project.ProjObjects[FMain.ActiveObject].Vy:=OldVy;
      end
      else
        FMain.ModifyObject(FMain.ActiveObject, ClassID, PartName, V, AngleV, F,
          AngleF, Radius, Mass, x+NewX-OldX, y+NewY-OldY, x+NewX-OldX,
          y+NewY-OldY, FillColor, BorderColor, BorderWidth)
    end;
    FMain.RedrawImage;
    FPanelProp.LoadProperties(FMain.ActiveObject);
  end;
  MouseDwn:=False;
  Screen.Cursor:=crDefault;
end;

procedure TFView.Image1DblClick(Sender: TObject);
begin
  if(not FMain.aStart.Checked or FMain.aPause.Checked)and(FMain.aEnQSel.Checked)and(FMain.ActiveObject<>0)then
  begin
    FMain.aEditObject.Execute;
    MouseDwn:=False;
  end;
end;

procedure TFView.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  d: Byte;
begin
  if Shift = [ssShift] then d:=10 else d:=1;
  case Key of
    38:
      begin
        with FMain.Project.ProjObjects[FMain.ActiveObject] do
          if FMain.aStart.Checked then
            FMain.ModifyObject(FMain.ActiveObject, ClassID, PartName, V, AngleV,
              F, AngleF, Radius, Mass, x0, y0, x, y+d, FillColor, BorderColor,
              BorderWidth)
          else
            FMain.ModifyObject(FMain.ActiveObject, ClassID, PartName, V, AngleV,
              F, AngleF, Radius, Mass, x, y+d, x, y+d, FillColor, BorderColor,
              BorderWidth);
        FPanelProp.LoadProperties(FMain.ActiveObject);
      end;
    40:
      begin
        with FMain.Project.ProjObjects[FMain.ActiveObject] do
          if FMain.aStart.Checked then
            FMain.ModifyObject(FMain.ActiveObject, ClassID, PartName, V, AngleV,
              F, AngleF, Radius, Mass, x0, y0, x, y-d, FillColor, BorderColor,
              BorderWidth)
          else
            FMain.ModifyObject(FMain.ActiveObject, ClassID, PartName, V, AngleV,
              F, AngleF, Radius, Mass, x, y-d, x, y-d, FillColor, BorderColor,
              BorderWidth);
        FPanelProp.LoadProperties(FMain.ActiveObject);
      end;
    37:
      begin
        with FMain.Project.ProjObjects[FMain.ActiveObject] do
          if FMain.aStart.Checked then
            FMain.ModifyObject(FMain.ActiveObject, ClassID, PartName, V, AngleV,
              F, AngleF, Radius, Mass, x0, y0, x-d, y, FillColor, BorderColor,
              BorderWidth)
          else
            FMain.ModifyObject(FMain.ActiveObject, ClassID, PartName, V, AngleV,
              F, AngleF, Radius, Mass, x-d, y0, x-d, y, FillColor, BorderColor,
              BorderWidth);
        FPanelProp.LoadProperties(FMain.ActiveObject);
      end;
    39:
      begin
        with FMain.Project.ProjObjects[FMain.ActiveObject] do
          if FMain.aStart.Checked then
            FMain.ModifyObject(FMain.ActiveObject, ClassID, PartName, V, AngleV,
              F, AngleF, Radius, Mass, x0, y0, x+d, y, FillColor, BorderColor,
              BorderWidth)
          else
            FMain.ModifyObject(FMain.ActiveObject, ClassID, PartName, V, AngleV,
              F, AngleF, Radius, Mass, x+d, y, x+d, y, FillColor, BorderColor,
              BorderWidth);
        FPanelProp.LoadProperties(FMain.ActiveObject);
      end;
  end;
end;

end.

