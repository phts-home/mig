unit uFPanelClasses;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, ComCtrls, ToolWin;

type
  TFPanelClasses = class(TForm)
    CheckListBox1: TCheckListBox;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    StatusBar1: TStatusBar;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    procedure CheckListBox1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CheckListBox1DblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPanelClasses: TFPanelClasses;

implementation

uses uFMain;

{$R *.dfm}

{-----------------------------------------------------------------------------}
{                                                                             }
{                               FPanelClasses                                 }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFPanelClasses.FormCreate(Sender: TObject);
var
  Rect: TRect;
begin
  SystemParametersInfo(SPI_GETWORKAREA,0,@Rect,0);
  Top:=FMain.cfg.ReadInteger('PanelClasses','Top',Rect.Top + 70);
  Left:=FMain.cfg.ReadInteger('PanelClasses','Left',Rect.Left + 3);
  Height:=FMain.cfg.ReadInteger('PanelClasses','Height',157);
  Visible:=FMain.cfg.ReadBool('PanelClasses','Visible',True);
end;

procedure TFPanelClasses.FormResize(Sender: TObject);
begin
  StatusBar1.Width:=ClientWidth;
  StatusBar1.Top:=ClientHeight - StatusBar1.Height;
  CheckListBox1.Height:=ClientHeight - 52;
end;

procedure TFPanelClasses.FormShow(Sender: TObject);
begin
  FMain.aShowClasses.Checked:=True;
end;

procedure TFPanelClasses.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FMain.aShowClasses.Checked:=False;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             <Other procedures>                              }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFPanelClasses.CheckListBox1Click(Sender: TObject);
var
  i: Integer;
begin
  for i:=0 to CheckListBox1.Count-1 do
    CheckListBox1.Checked[i]:=False;
  CheckListBox1.Checked[CheckListBox1.ItemIndex]:=True;
  FMain.Project.ProjActiveClass:=CheckListBox1.ItemIndex+1;
end;

procedure TFPanelClasses.CheckListBox1DblClick(Sender: TObject);
begin
  FMain.aEditClass.Execute;
end;



end.
