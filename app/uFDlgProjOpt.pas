unit uFDlgProjOpt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Spin, Buttons, ComCtrls, CheckLst;

type
  TFDlgProjOpt = class(TForm)
    ColorBox1: TColorBox;
    SpEdW: TSpinEdit;
    SpEdH: TSpinEdit;
    Button2: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Button1: TButton;
    Button3: TButton;
    GroupBox1: TGroupBox;
    CheckListBox1: TCheckListBox;
    GroupBox2: TGroupBox;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    Label4: TLabel;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    ListView1: TListView;
    Label5: TLabel;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    ComboBox1: TComboBox;
    Memo1: TMemo;
    Label6: TLabel;
    Label7: TLabel;
    EdScale: TEdit;
    Button4: TButton;
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListView1Click(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure CheckListBox1Click(Sender: TObject);
    procedure CheckListBox1DblClick(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure EdScaleKeyPress(Sender: TObject; var Key: Char);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure SaveDefaultValues;
    procedure LoadDefaultValues;
  end;

var
  FDlgProjOpt: TFDlgProjOpt;

implementation

uses uFMain, uFPanelObjects, uFPanelProp, uFPanelClasses;

{$R *.dfm}

{-----------------------------------------------------------------------------}
{  procedure     SaveDefaultValues                                            }
{                                                                             }
{  discription   ���������� ���������� ������� �� ���������                   }
{-----------------------------------------------------------------------------}
procedure TFDlgProjOpt.SaveDefaultValues;
begin
  with FMain.DefaultProject do
  begin
    ProjObjectsCount:=0;
    ProjWidth:=SpEdW.Value;
    ProjHeight:=SpEdH.Value;
    ProjBackgr:=ColorBox1.Selected;
    ProjDiscription:=Memo1.Text;
    ProjScale:=StrToFloat(EdScale.Text);
  end;
  FMain.SaveDefaultProject;
end;

{-----------------------------------------------------------------------------}
{  procedure     LoadDefaultValues                                            }
{                                                                             }
{  discription   �������� ���������� ������� �� ���������                     }
{-----------------------------------------------------------------------------}
procedure TFDlgProjOpt.LoadDefaultValues;
begin
  with FMain.DefaultProject do
  begin
    SpEdW.Value:=ProjWidth;
    SpEdH.Value:=ProjHeight;
    ColorBox1.Selected:=ProjBackgr;
    Memo1.Text:=ProjDiscription;
    EdScale.Text:=FloatToStr(ProjScale);
  end;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             FDlgProjOpt                                     }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFDlgProjOpt.FormShow(Sender: TObject);
begin
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;

  SpEdW.Value:=FMain.Project.ProjWidth;
  SpEdH.Value:=FMain.Project.ProjHeight;
  ColorBox1.Selected:=FMain.Project.ProjBackgr;
  EdScale.Text:=FloatToStr(FMain.Project.ProjScale);
  Memo1.Lines.Text:=FMain.Project.ProjDiscription;
  ListView1.Items:=FPanelObjects.ListView1.Items;

  Label4.Caption:=FPanelObjects.StatusBar1.SimpleText;
  Label5.Caption:=FPanelClasses.StatusBar1.SimpleText;

  FMain.ReloadClassesList;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             <Other procedures>                              }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFDlgProjOpt.Button2Click(Sender: TObject);
begin
  SaveDefaultValues;
end;

procedure TFDlgProjOpt.Button4Click(Sender: TObject);
begin
  LoadDefaultValues;
end;

procedure TFDlgProjOpt.ListView1Click(Sender: TObject);
begin
  FPanelObjects.ListView1.ItemIndex:=ListView1.ItemIndex;
  if ListView1.ItemIndex=-1 then
  begin
    FPanelProp.UnloadProperties;
    FMain.ActiveObject:=0;
    FMain.RedrawImage;
  end;
end;

procedure TFDlgProjOpt.ListView1DblClick(Sender: TObject);
begin
  FMain.aEditObject.Execute;
end;

procedure TFDlgProjOpt.CheckListBox1Click(Sender: TObject);
var
  i: Integer;
begin
  for i:=0 to CheckListBox1.Count-1 do
  begin
    CheckListBox1.Checked[i]:=False;
    FPanelClasses.CheckListBox1.Checked[i]:=False;
  end;
  FPanelClasses.CheckListBox1.ItemIndex:=CheckListBox1.ItemIndex;
  CheckListBox1.Checked[CheckListBox1.ItemIndex]:=True;
  FPanelClasses.CheckListBox1.Checked[CheckListBox1.ItemIndex]:=True;
  FMain.Project.ProjActiveClass:=CheckListBox1.ItemIndex+1;
end;

procedure TFDlgProjOpt.CheckListBox1DblClick(Sender: TObject);
begin
  FMain.aEditClass.Execute;
end;

procedure TFDlgProjOpt.ComboBox1Change(Sender: TObject);
begin
  FPanelObjects.ComboBox1.ItemIndex:=ComboBox1.ItemIndex;
  FMain.ReloadObjectList;
end;

procedure TFDlgProjOpt.EdScaleKeyPress(Sender: TObject; var Key: Char);
begin
  if not(((Key >= #48)and(Key <= #57))or(Key = #46)or(Key = #44)or(Key = #8))then
    Key:=#0;
end;

end.
