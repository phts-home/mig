unit uFPanelProp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ValEdit, StdCtrls, ComCtrls, Math, ExtCtrls, Spin;

type
  TFPanelProp = class(TForm)
    ScrollBox1: TScrollBox;
    StringGrid1: TStringGrid;
    SpinEdit1: TSpinEdit;
    ColorBox2: TColorBox;
    ColorBox1: TColorBox;
    ComboBox1: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ColorBox1Change(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure ColorBox2Change(Sender: TObject);
    procedure StringGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure ComboBox1Change(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    procedure LoadProperties(Index: Integer);
    procedure UnloadProperties;
  end;

var
  FPanelProp: TFPanelProp;

implementation

uses uFMain, uFPanelObjects, uFPanelClasses;

{$R *.dfm}

{-----------------------------------------------------------------------------}
{  procedure     LoadProperties                                               }
{                                                                             }
{  discription   �������� ���������� ������� � ������                         }
{-----------------------------------------------------------------------------}
procedure TFPanelProp.LoadProperties(Index: Integer);
var i: Integer;
begin
  if Index>0 then
  begin
    ComboBox1.Clear;
    for i:=1 to FMain.Project.ProjClassesCount do
      ComboBox1.Items.Add(FMain.Project.ProjClasses[i].ClassName1);
    ComboBox1.ItemIndex:=FMain.Project.ProjObjects[Index].ClassID-1;
    StringGrid1.Cells[1,1]:=FMain.Project.ProjObjects[Index].PartName;
    StringGrid1.Cells[1,2]:=FloatToStr(FMain.Project.ProjObjects[Index].V0);
    StringGrid1.Cells[1,3]:=FloatToStr(RadToDeg(FMain.Project.ProjObjects[Index].AngleV));
    StringGrid1.Cells[1,4]:=FloatToStr(FMain.Project.ProjObjects[Index].F0);
    StringGrid1.Cells[1,5]:=FloatToStr(RadToDeg(FMain.Project.ProjObjects[Index].AngleF));
    StringGrid1.Cells[1,6]:=FloatToStr(FMain.Project.ProjObjects[Index].Radius);
    StringGrid1.Cells[1,7]:=FloatToStr(FMain.Project.ProjObjects[Index].Mass);
    StringGrid1.Cells[1,8]:=FloatToStr(FMain.Project.ProjObjects[Index].x0);
    StringGrid1.Cells[1,9]:=FloatToStr(FMain.Project.ProjObjects[Index].y0);
    ColorBox1.Selected:=FMain.Project.ProjObjects[Index].FillColor;
    ColorBox2.Selected:=FMain.Project.ProjObjects[Index].BorderColor;
    SpinEdit1.Value:=FMain.Project.ProjObjects[Index].BorderWidth;

    StringGrid1.Enabled:=True;
    ComboBox1.Visible:=True;
    SpinEdit1.Visible:=True;
    ColorBox1.Visible:=True;
    ColorBox2.Visible:=True;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     UnloadProperties                                             }
{                                                                             }
{  discription   ������� ���� ����� ������                                    }
{-----------------------------------------------------------------------------}
procedure TFPanelProp.UnloadProperties;
var
  i: Integer;
begin
  for i:=1 to 9 do
    StringGrid1.Cells[1,i]:='';
  StringGrid1.Enabled:=False;
  ComboBox1.Visible:=False;
  SpinEdit1.Visible:=False;
  ColorBox1.Visible:=False;
  ColorBox2.Visible:=False;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                               FPanelProp                                    }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFPanelProp.FormCreate(Sender: TObject);
var
  Rect: TRect;
begin
  StringGrid1.Cells[0,0]:='�����';
  StringGrid1.Cells[0,1]:='���';
  StringGrid1.Cells[0,2]:='��������';
  StringGrid1.Cells[0,3]:='����';
  StringGrid1.Cells[0,4]:='����';
  StringGrid1.Cells[0,5]:='����';
  StringGrid1.Cells[0,6]:='������';
  StringGrid1.Cells[0,7]:='�����';
  StringGrid1.Cells[0,8]:='x0';
  StringGrid1.Cells[0,9]:='y0';
  StringGrid1.Cells[0,10]:='���� �������';
  StringGrid1.Cells[0,11]:='���� �������';
  StringGrid1.Cells[0,12]:='������ �������';

  SystemParametersInfo(SPI_GETWORKAREA,0,@Rect,0);
  Top:=FMain.cfg.ReadInteger('PanelProperties','Top',FPanelObjects.Height +
    FPanelClasses.Height + Rect.Top + 70);
  Left:=FMain.cfg.ReadInteger('PanelProperties','Left',Rect.Left + 3);
  Height:=FMain.cfg.ReadInteger('PanelProperties','Height',277);
  Visible:=FMain.cfg.ReadBool('PanelProperties','Visible',True);
end;

procedure TFPanelProp.FormResize(Sender: TObject);
begin
  ScrollBox1.Height:=ClientHeight;
end;

procedure TFPanelProp.FormShow(Sender: TObject);
begin
  FMain.aShowProp.Checked:=True;
end;

procedure TFPanelProp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FMain.aShowProp.Checked:=False;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             <Other procedures>                              }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFPanelProp.ColorBox1Change(Sender: TObject);
begin
  if FMain.Project.ProjObjects[FMain.ActiveObject].FillColor <> ColorBox1.Selected then
    FMain.ModifyObject(FMain.ActiveObject,
      FMain.Project.ProjObjects[FMain.ActiveObject].ClassID,
      FMain.Project.ProjObjects[FMain.ActiveObject].PartName,
      StrToFloat(StringGrid1.Cells[1,2]),DegToRad(StrToFloat(StringGrid1.Cells[1,3])),
      StrToFloat(StringGrid1.Cells[1,4]),DegToRad(StrToFloat(StringGrid1.Cells[1,5])),
      StrToInt(StringGrid1.Cells[1,6]),StrToFloat(StringGrid1.Cells[1,7]),
      StrToFloat(StringGrid1.Cells[1,8]),StrToFloat(StringGrid1.Cells[1,9]),
      FMain.Project.ProjObjects[FMain.ActiveObject].x,
      FMain.Project.ProjObjects[FMain.ActiveObject].y,
      ColorBox1.Selected,ColorBox2.Selected,SpinEdit1.Value);
end;

procedure TFPanelProp.ColorBox2Change(Sender: TObject);
begin
  if FMain.Project.ProjObjects[FMain.ActiveObject].BorderColor <> ColorBox2.Selected then
    FMain.ModifyObject(FMain.ActiveObject,
      FMain.Project.ProjObjects[FMain.ActiveObject].ClassID,
      FMain.Project.ProjObjects[FMain.ActiveObject].PartName,
      StrToFloat(StringGrid1.Cells[1,2]),DegToRad(StrToFloat(StringGrid1.Cells[1,3])),
      StrToFloat(StringGrid1.Cells[1,4]),DegToRad(StrToFloat(StringGrid1.Cells[1,5])),
      StrToInt(StringGrid1.Cells[1,6]),StrToFloat(StringGrid1.Cells[1,7]),
      StrToFloat(StringGrid1.Cells[1,8]),StrToFloat(StringGrid1.Cells[1,9]),
      FMain.Project.ProjObjects[FMain.ActiveObject].x,
      FMain.Project.ProjObjects[FMain.ActiveObject].y,
      ColorBox1.Selected,ColorBox2.Selected,SpinEdit1.Value);
end;

procedure TFPanelProp.SpinEdit1Change(Sender: TObject);
begin
  if FMain.Project.ProjObjects[FMain.ActiveObject].BorderWidth <> SpinEdit1.Value then
    FMain.ModifyObject(FMain.ActiveObject,
      FMain.Project.ProjObjects[FMain.ActiveObject].ClassID,
      FMain.Project.ProjObjects[FMain.ActiveObject].PartName,
      StrToFloat(StringGrid1.Cells[1,2]),DegToRad(StrToFloat(StringGrid1.Cells[1,3])),
      StrToFloat(StringGrid1.Cells[1,4]),DegToRad(StrToFloat(StringGrid1.Cells[1,5])),
      StrToInt(StringGrid1.Cells[1,6]),StrToFloat(StringGrid1.Cells[1,7]),
      StrToFloat(StringGrid1.Cells[1,8]),StrToFloat(StringGrid1.Cells[1,9]),
      FMain.Project.ProjObjects[FMain.ActiveObject].x,
      FMain.Project.ProjObjects[FMain.ActiveObject].y,
      ColorBox1.Selected,ColorBox2.Selected,SpinEdit1.Value);
end;

procedure TFPanelProp.StringGrid1KeyPress(Sender: TObject; var Key: Char);
var
  xn, yn: Real;
begin
  if Key = #13 then
  begin
    if FMain.aStart.Checked then
    begin
      xn:=FMain.Project.ProjObjects[FMain.ActiveObject].x;
      yn:=FMain.Project.ProjObjects[FMain.ActiveObject].y;
    end
    else
    begin
      xn:=StrToFloat(StringGrid1.Cells[1,8]);
      yn:=StrToFloat(StringGrid1.Cells[1,9]);
    end;
    FMain.ModifyObject(FMain.ActiveObject,
    FMain.Project.ProjObjects[FMain.ActiveObject].ClassID,
    StringGrid1.Cells[1,1],
    StrToFloat(StringGrid1.Cells[1,2]),DegToRad(StrToFloat(StringGrid1.Cells[1,3])),
    StrToFloat(StringGrid1.Cells[1,4]),DegToRad(StrToFloat(StringGrid1.Cells[1,5])),
    StrToInt(StringGrid1.Cells[1,6]),StrToFloat(StringGrid1.Cells[1,7]),
    StrToFloat(StringGrid1.Cells[1,8]),StrToFloat(StringGrid1.Cells[1,9]),
    xn,yn,ColorBox1.Selected,ColorBox2.Selected,SpinEdit1.Value);
    FMain.ReloadObjectList;
    Key:=#0;
  end;
end;

procedure TFPanelProp.StringGrid1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if FMain.ActiveObject<>0 then
    with FMain.Project.ProjObjects[FMain.ActiveObject] do
      if(StringGrid1.Cells[1,1]<>PartName)
        or(StringGrid1.Cells[1,2]<>FloatToStr(V))
        or(StringGrid1.Cells[1,3]<>FloatToStr(RadToDeg(AngleV)))
        or(StringGrid1.Cells[1,4]<>FloatToStr(F))
        or(StringGrid1.Cells[1,5]<>FloatToStr(RadToDeg(AngleF)))
        or(StringGrid1.Cells[1,6]<>FloatToStr(Radius))
        or(StringGrid1.Cells[1,7]<>FloatToStr(Mass))
        or(StringGrid1.Cells[1,8]<>FloatToStr(x0))
        or(StringGrid1.Cells[1,9]<>FloatToStr(y0))then
        begin
          FMain.ModifyObject(FMain.ActiveObject,
            FMain.Project.ProjObjects[FMain.ActiveObject].ClassID,
            StringGrid1.Cells[1,1],
            StrToFloat(StringGrid1.Cells[1,2]),DegToRad(StrToFloat(StringGrid1.Cells[1,3])),
            StrToFloat(StringGrid1.Cells[1,4]),DegToRad(StrToFloat(StringGrid1.Cells[1,5])),
            StrToInt(StringGrid1.Cells[1,6]),StrToFloat(StringGrid1.Cells[1,7]),
            StrToFloat(StringGrid1.Cells[1,8]),StrToFloat(StringGrid1.Cells[1,9]),
            FMain.Project.ProjObjects[FMain.ActiveObject].x,
            FMain.Project.ProjObjects[FMain.ActiveObject].y,
            ColorBox1.Selected,ColorBox2.Selected,SpinEdit1.Value);
          FMain.ReloadObjectList;
        end;

end;

procedure TFPanelProp.ComboBox1Change(Sender: TObject);
begin
  FMain.ChangeObjClass(FMain.ActiveObject, ComboBox1.ItemIndex+1);
  FMain.ReloadObjectList;
end;



end.
