unit uFPanelGraph;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Spin, Menus, Buttons, ComCtrls, TeEngine,
  Series, TeeProcs, Chart, IniFiles;

type
  TFPanelGraph = class(TForm)
    Timer1: TTimer;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Chart1: TChart;
    Series1: TFastLineSeries;
    Series2: TFastLineSeries;
    Series3: TFastLineSeries;
    Panel2: TPanel;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    SaveDialog1: TSaveDialog;
    TabSheet3: TTabSheet;
    RadioGroup1: TRadioGroup;
    SpeedButton1: TSpeedButton;
    Label9: TLabel;
    TrackBar1: TTrackBar;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    CheckBox1: TCheckBox;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    ComboBox1: TComboBox;
    ColorBox1: TColorBox;
    SpinEdit1: TSpinEdit;
    CheckBox2: TCheckBox;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    ComboBox2: TComboBox;
    ColorBox2: TColorBox;
    SpinEdit2: TSpinEdit;
    CheckBox3: TCheckBox;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    ComboBox3: TComboBox;
    ColorBox3: TColorBox;
    SpinEdit3: TSpinEdit;
    GroupBox8: TGroupBox;
    Label13: TLabel;
    ColorBox5: TColorBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    GroupBox6: TGroupBox;
    Label12: TLabel;
    ColorBox4: TColorBox;
    ComboBox4: TComboBox;
    ChBxYEn: TCheckBox;
    GroupBox9: TGroupBox;
    ClrBxY: TColorBox;
    ChBxYGr: TCheckBox;
    ComboBox5: TComboBox;
    GroupBox10: TGroupBox;
    ChBxYAu: TCheckBox;
    SpEdY: TSpinEdit;
    SpeedButton4: TSpeedButton;
    Panel1: TPanel;
    ChBxEn: TCheckBox;
    Label17: TLabel;
    SpEdYmin: TSpinEdit;
    Label3: TLabel;
    EdUnit: TEdit;
    Label15: TLabel;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ChBxEnClick(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure SpinEdit2Change(Sender: TObject);
    procedure SpinEdit3Change(Sender: TObject);
    procedure ColorBox1Change(Sender: TObject);
    procedure ColorBox2Change(Sender: TObject);
    procedure ColorBox3Change(Sender: TObject);
    procedure ChBxYAuClick(Sender: TObject);
    procedure SpEdYChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure ChBxYEnClick(Sender: TObject);
    procedure ChBxYGrClick(Sender: TObject);
    procedure ComboBox5Change(Sender: TObject);
    procedure ClrBxYChange(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure ColorBox4Change(Sender: TObject);
    procedure ComboBox4Change(Sender: TObject);
    procedure CheckBox5Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure ColorBox5Change(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure CheckBox4Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpEdYminChange(Sender: TObject);
  private
    { Private declarations }
  public
    DefValuesFile: TMemIniFile;
    Values: Array [1..3,1..200] of Double;
    yy1, yy2, yy3: Double;
    procedure ResetValues;
    procedure WriteValues;
    procedure DrawValues;
    procedure RefreshLegend;
    procedure SaveDefaultValues;
    procedure LoadDefaultValues;
    procedure ApplyParams;
  end;

var
  FPanelGraph: TFPanelGraph;

implementation

uses uFMain, uFPanelHistogr1;

{$R *.dfm}

{-----------------------------------------------------------------------------}
{  procedure     ResetValues                                                  }
{                                                                             }
{  discription   ����� ���� �������� 3-� �������� �� 0                        }
{-----------------------------------------------------------------------------}
procedure TFPanelGraph.ResetValues;
var
  i, Index: Integer;
begin
  for Index:=1 to 3 do
    for i:=1 to 200 do
      Values[Index,i]:=0;
end;

{-----------------------------------------------------------------------------}
{  procedure     WriteValues                                                  }
{                                                                             }
{  discription   ���������� ����� �������� ��������                           }
{-----------------------------------------------------------------------------}
procedure TFPanelGraph.WriteValues;
const
  k = 1.380650524E-23;
var
  i, Index, c1, c2, c3: Integer;
  vv: Real;
  sc: Real;
begin
  for Index:=1 to 3 do
    for i:=2 to 200 do
      Values[Index,i-1]:=Values[Index,i];

  try
    sc:=StrToFloat(EdUnit.Text);
  except
    sc:=1;
  end;

  yy1:=0;
  yy2:=0;
  yy3:=0;
  c1:=0;
  c2:=0;
  c3:=0;
  for i:=1 to FMain.Project.ProjObjectsCount do
    with FMain.Project.ProjObjects[i] do
    begin
      if(CheckBox1.Checked)and(ClassID = ComboBox1.ItemIndex+1) then
      begin
        vv:=Sqrt(Vx*Vx+Vy*Vy);
        yy1:=yy1 + Mass*vv*vv;
        Inc(c1);
      end;
      if(CheckBox2.Checked)and(ClassID = ComboBox2.ItemIndex+1) then
      begin
        vv:=Sqrt(Vx*Vx+Vy*Vy);
        yy2:=yy2 + Mass*vv*vv;
        Inc(c2);
      end;
      if(CheckBox3.Checked)and(ClassID = ComboBox3.ItemIndex+1) then
      begin
        vv:=Sqrt(Vx*Vx+Vy*Vy);
        yy3:=yy3 + Mass*vv*vv;
        Inc(c3);
      end;
    end;
  if RadioGroup1.ItemIndex = 0 then
  begin
    if c1 <> 0 then yy1:=2*yy1/c1/k/3;
    if c2 <> 0 then yy2:=2*yy2/c2/k/3;
    if c3 <> 0 then yy3:=2*yy3/c3/k/3;
  end;
  if RadioGroup1.ItemIndex = 1 then
  begin
    if c1 <> 0 then yy1:=yy1/c1;
    if c2 <> 0 then yy2:=yy2/c2;
    if c3 <> 0 then yy3:=yy3/c3;
  end;
  Values[1,200]:=yy1/sc/2;
  Values[2,200]:=yy2/sc/2;
  Values[3,200]:=yy3/sc/2;
end;

{-----------------------------------------------------------------------------}
{  procedure     DrawValues                                                   }
{                                                                             }
{  discription   ����� �������� �� �����                                      }
{-----------------------------------------------------------------------------}
procedure TFPanelGraph.DrawValues;
var
  i: Integer;
begin
  Series1.Clear;
  Series2.Clear;
  Series3.Clear;

  if CheckBox1.Checked then
    for i:=1 to 200 do
      Series1.Add(Values[1,i],'',clBlack);

  if CheckBox2.Checked then
    for i:=1 to 200 do
      Series2.Add(Values[2,i],'',clBlack);

  if CheckBox3.Checked then
    for i:=1 to 200 do
      Series3.Add(Values[3,i],'',clBlack);
  Chart1.Refresh;
end;

{-----------------------------------------------------------------------------}
{  procedure     RefreshLegend                                                }
{                                                                             }
{  discription   �������� �������                                             }
{-----------------------------------------------------------------------------}
procedure TFPanelGraph.RefreshLegend;
begin
  Series1.ShowInLegend:=CheckBox1.Checked;
  Series1.Title:=FMain.Project.ProjClasses[ComboBox1.ItemIndex+1].ClassName1;
  Series2.ShowInLegend:=CheckBox2.Checked;
  Series2.Title:=FMain.Project.ProjClasses[ComboBox2.ItemIndex+1].ClassName1;
  Series3.ShowInLegend:=CheckBox3.Checked;
  Series3.Title:=FMain.Project.ProjClasses[ComboBox3.ItemIndex+1].ClassName1;
end;

{-----------------------------------------------------------------------------}
{  procedure     SaveDefaultValues                                            }
{                                                                             }
{  discription   ���������� ���������� �������� �� ���������                  }
{-----------------------------------------------------------------------------}
procedure TFPanelGraph.SaveDefaultValues;
begin
  CreateDir(ExtractFilePath(ParamStr(0))+'Data\');
  DefValuesFile:=TMemIniFile.Create(ExtractFilePath(ParamStr(0))+'Data\PanelGraphDefValues.dat');
  with DefValuesFile do
  begin
    WriteBool('Graph#1','Visible',CheckBox1.Checked);
    WriteInteger('Graph#1','Color',ColorBox1.Selected);
    WriteInteger('Graph#1','Width',SpinEdit1.Value);

    WriteBool('Graph#2','Visible',CheckBox2.Checked);
    WriteInteger('Graph#2','Color',ColorBox2.Selected);
    WriteInteger('Graph#2','Width',SpinEdit2.Value);

    WriteBool('Graph#3','Visible',CheckBox3.Checked);
    WriteInteger('Graph#3','Color',ColorBox3.Selected);
    WriteInteger('Graph#3','Width',SpinEdit3.Value);

    WriteInteger('Performance','Interval',TrackBar1.Position*100);

    WriteBool('Appearance','Enable3D',CheckBox4.Checked);
    WriteInteger('Appearance','Backgr',ColorBox5.Selected);

    WriteBool('Legend','Visible',CheckBox5.Checked);
    WriteInteger('Legend','Alignment',ComboBox4.ItemIndex);
    WriteInteger('Legend','Color',ColorBox4.Selected);

    WriteBool('LeftAxis','Visible',ChBxYEn.Checked);
    WriteBool('LeftAxis','Grid',ChBxYGr.Checked);
    WriteInteger('LeftAxis','GridStyle',ComboBox5.ItemIndex);
    WriteInteger('LeftAxis','GridColor',ClrBxY.Selected);
    WriteBool('LeftAxis','ScaleAuto',ChBxYAu.Checked);
    WriteInteger('LeftAxis','ScaleValMax',SpEdY.Value);
    WriteInteger('LeftAxis','ScaleValMin',SpEdYmin.Value);
    WriteString('LeftAxis','UnitScale',EdUnit.Text);
    UpdateFile;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     SaveDefaultValues                                            }
{                                                                             }
{  discription   �������� ���������� �������� �� ���������                    }
{-----------------------------------------------------------------------------}
procedure TFPanelGraph.LoadDefaultValues;
begin
  if FileExists(ExtractFilePath(ParamStr(0))+'Data\PanelGraphDefValues.dat') then
  begin
    DefValuesFile:=TMemIniFile.Create(ExtractFilePath(ParamStr(0))+'Data\PanelGraphDefValues.dat');
    with DefValuesFile do
    begin
      ColorBox1.Selected:=ReadInteger('Graph#1','Color',clRed);
      SpinEdit1.Value:=ReadInteger('Graph#1','Width',2);
      CheckBox1.Checked:=ReadBool('Graph#1','Visible',True);

      ColorBox2.Selected:=ReadInteger('Graph#2','Color',clBlue);
      SpinEdit2.Value:=ReadInteger('Graph#2','Width',2);
      CheckBox2.Checked:=ReadBool('Graph#2','Visible',False);

      ColorBox3.Selected:=ReadInteger('Graph#3','Color',clGreen);
      SpinEdit3.Value:=ReadInteger('Graph#3','Width',2);
      CheckBox3.Checked:=ReadBool('Graph#3','Visible',False);

      TrackBar1.Position:=ReadInteger('Performance','Interval',100) div 100;

      CheckBox4.Checked:=ReadBool('Appearance','Enable3D',False);
      ColorBox5.Selected:=ReadInteger('Appearance','Backgr',clWhite);

      CheckBox5.Checked:=ReadBool('Legend','Visible',False);
      ComboBox4.ItemIndex:=ReadInteger('Legend','Alignment',2);
      ColorBox4.Selected:=ReadInteger('Legend','Color',clWhite);

      ChBxYEn.Checked:=ReadBool('LeftAxis','Visible',True);
      ChBxYGr.Checked:=ReadBool('LeftAxis','Grid',True);
      ComboBox5.ItemIndex:=ReadInteger('LeftAxis','GridStyle',4);
      ClrBxY.Selected:=ReadInteger('LeftAxis','GridColor',clGray);
      SpEdY.Value:=ReadInteger('LeftAxis','ScaleValMax',100);
      SpEdYmin.Value:=ReadInteger('LeftAxis','ScaleValMin',0);
      EdUnit.Text:=ReadString('LeftAxis','UnitScale','1');
      ChBxYAu.Checked:=ReadBool('LeftAxis','ScaleAuto',False);
    end;
  end
  else
  begin
    ColorBox1.Selected:=clRed;
    SpinEdit1.Value:=2;
    CheckBox1.Checked:=True;

    ColorBox2.Selected:=clBlue;
    SpinEdit2.Value:=2;
    CheckBox2.Checked:=False;

    ColorBox3.Selected:=clGreen;
    SpinEdit3.Value:=2;
    CheckBox3.Checked:=False;

    TrackBar1.Position:=1;

    CheckBox4.Checked:=False;
    ColorBox5.Selected:=clWhite;

    CheckBox5.Checked:=False;
    ComboBox4.ItemIndex:=2;
    ColorBox4.Selected:=clWhite;

    ChBxYEn.Checked:=True;
    ChBxYGr.Checked:=True;
    ComboBox5.ItemIndex:=4;
    ClrBxY.Selected:=clGray;
    SpEdY.Value:=100;
    SpEdYmin.Value:=0;
    EdUnit.Text:='1';
    ChBxYAu.Checked:=False;
  end;
  ApplyParams;
end;

{-----------------------------------------------------------------------------}
{  procedure     SaveDefaultValues                                            }
{                                                                             }
{  discription   ���������� ���������� � ��������                             }
{-----------------------------------------------------------------------------}
procedure TFPanelGraph.ApplyParams;
begin
  Series1.SeriesColor:=ColorBox1.Selected;
  Series1.LinePen.Width:=SpinEdit1.Value;
  Series2.SeriesColor:=ColorBox2.Selected;
  Series2.LinePen.Width:=SpinEdit2.Value;
  Series3.SeriesColor:=ColorBox3.Selected;
  Series3.LinePen.Width:=SpinEdit3.Value;
  Timer1.Interval:=TrackBar1.Position*100;
  Label9.Caption:=IntToStr(Timer1.Interval)+' ��';
  Chart1.View3D:=CheckBox4.Checked;
  Chart1.BackColor:=ColorBox5.Selected;
  RefreshLegend;
  Chart1.Legend.Visible:=CheckBox5.Checked;
  case ComboBox4.ItemIndex of
    0: Chart1.Legend.Alignment:=laBottom;
    1: Chart1.Legend.Alignment:=laLeft;
    2: Chart1.Legend.Alignment:=laRight;
    3: Chart1.Legend.Alignment:=laTop;
  end;
  Chart1.Legend.Color:=ColorBox4.Selected;

  Chart1.LeftAxis.Visible:=ChBxYEn.Checked;
  if not ChBxYEn.Checked then ChBxYGr.Checked:=False;
  Chart1.LeftAxis.Grid.Visible:=ChBxYGr.Checked;
  case ComboBox5.ItemIndex of
    0: Chart1.LeftAxis.Grid.Style:=psClear;
   1: Chart1.LeftAxis.Grid.Style:=psDash;
    2: Chart1.LeftAxis.Grid.Style:=psDashDot;
    3: Chart1.LeftAxis.Grid.Style:=psDashDotDot;
    4: Chart1.LeftAxis.Grid.Style:=psDot;
    5: Chart1.LeftAxis.Grid.Style:=psInsideFrame;
    6: Chart1.LeftAxis.Grid.Style:=psSolid;
  end;
  Chart1.LeftAxis.Grid.Color:=ClrBxY.Selected;
  if ChBxYAu.Checked then
  begin
    Chart1.LeftAxis.Minimum:=0;
    Chart1.LeftAxis.AutomaticMaximum:=True;
  end
  else
  begin
    Chart1.LeftAxis.AutomaticMaximum:=False;
    Chart1.LeftAxis.Maximum:=SpEdY.Value;
    Chart1.LeftAxis.Minimum:=SpEdYmin.Value;
  end;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                              FPanelGraph                                    }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFPanelGraph.FormCreate(Sender: TObject);
var
  Rect: TRect;
begin
  SystemParametersInfo(SPI_GETWORKAREA,0,@Rect,0);
  Top:=FMain.cfg.ReadInteger('PanelGraph','Top',Rect.Bottom - FPanelHistogr1.Height - Height - 21);
  Left:=FMain.cfg.ReadInteger('PanelGraph','Left',Rect.Right - Width - 3);
  LoadDefaultValues;
  SaveDialog1.InitialDir:=ExtractFilePath(ParamStr(0));
  Visible:=FMain.cfg.ReadBool('PanelGraph','Visible',False);
  FMain.ReloadClassesList;
  ResetValues;
  Randomize;
end;

procedure TFPanelGraph.FormShow(Sender: TObject);
begin
  FMain.aShowGraph.Checked:=True;
end;

procedure TFPanelGraph.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FMain.aShowGraph.Checked:=False;
  ChBxEn.Checked:=False;
  Timer1.Enabled:=False;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             <Other procedures>                              }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFPanelGraph.Timer1Timer(Sender: TObject);
begin
  WriteValues;
  DrawValues;
end;

procedure TFPanelGraph.SpeedButton2Click(Sender: TObject);
begin
  SaveDefaultValues;
end;

procedure TFPanelGraph.SpeedButton4Click(Sender: TObject);
begin
  LoadDefaultValues;
end;

procedure TFPanelGraph.ChBxEnClick(Sender: TObject);
begin
  RefreshLegend;
  Timer1.Enabled:=ChBxEn.Checked;
  if ChBxEn.Checked then PageControl1.TabIndex:=0;
end;

procedure TFPanelGraph.SpinEdit1Change(Sender: TObject);
begin
  CheckBox1.Checked:=True;
  Series1.LinePen.Width:=SpinEdit1.Value;
end;

procedure TFPanelGraph.SpinEdit2Change(Sender: TObject);
begin
  CheckBox2.Checked:=True;
  Series2.LinePen.Width:=SpinEdit2.Value;
end;

procedure TFPanelGraph.SpinEdit3Change(Sender: TObject);
begin
  CheckBox3.Checked:=True;
  Series3.LinePen.Width:=SpinEdit3.Value;
end;

procedure TFPanelGraph.ColorBox1Change(Sender: TObject);
begin
  CheckBox1.Checked:=True;
  Series1.SeriesColor:=ColorBox1.Selected;
end;

procedure TFPanelGraph.ColorBox2Change(Sender: TObject);
begin
  CheckBox2.Checked:=True;
  Series2.SeriesColor:=ColorBox2.Selected;
end;

procedure TFPanelGraph.ColorBox3Change(Sender: TObject);
begin
  CheckBox3.Checked:=True;
  Series3.SeriesColor:=ColorBox3.Selected;
end;

procedure TFPanelGraph.ChBxYAuClick(Sender: TObject);
begin
  if ChBxYAu.Checked then
  begin
    Chart1.LeftAxis.Minimum:=0;
    Chart1.LeftAxis.AutomaticMaximum:=True;
  end
  else
  begin
    Chart1.LeftAxis.AutomaticMaximum:=False;
    Chart1.LeftAxis.Maximum:=SpEdY.Value;
    Chart1.LeftAxis.Minimum:=SpEdYmin.Value;
  end;
end;

procedure TFPanelGraph.SpEdYChange(Sender: TObject);
begin
  ChBxYAu.Checked:=False;
  Chart1.LeftAxis.Maximum:=SpEdY.Value;
end;

procedure TFPanelGraph.SpEdYminChange(Sender: TObject);
begin
  ChBxYAu.Checked:=False;
  Chart1.LeftAxis.Minimum:=SpEdYmin.Value;
end;

procedure TFPanelGraph.SpeedButton1Click(Sender: TObject);
begin
  Series1.Clear;
  Series2.Clear;
  Series3.Clear;
  ResetValues;
end;

procedure TFPanelGraph.ChBxYEnClick(Sender: TObject);
begin
  Chart1.LeftAxis.Visible:=ChBxYEn.Checked;
  if not ChBxYEn.Checked then ChBxYGr.Checked:=False;
end;

procedure TFPanelGraph.ChBxYGrClick(Sender: TObject);
begin
  Chart1.LeftAxis.Grid.Visible:=ChBxYGr.Checked;
end;

procedure TFPanelGraph.ComboBox5Change(Sender: TObject);
begin
  case ComboBox5.ItemIndex of
    0: Chart1.LeftAxis.Grid.Style:=psClear;
    1: Chart1.LeftAxis.Grid.Style:=psDash;
    2: Chart1.LeftAxis.Grid.Style:=psDashDot;
    3: Chart1.LeftAxis.Grid.Style:=psDashDotDot;
    4: Chart1.LeftAxis.Grid.Style:=psDot;
    5: Chart1.LeftAxis.Grid.Style:=psInsideFrame;
    6: Chart1.LeftAxis.Grid.Style:=psSolid;
  end;
end;

procedure TFPanelGraph.ClrBxYChange(Sender: TObject);
begin
  Chart1.LeftAxis.Grid.Color:=ClrBxY.Selected;
end;

procedure TFPanelGraph.TrackBar1Change(Sender: TObject);
begin
  Timer1.Interval:=TrackBar1.Position*100;
  Label9.Caption:=IntToStr(Timer1.Interval)+' ��';
end;

procedure TFPanelGraph.ColorBox4Change(Sender: TObject);
begin
  Chart1.Legend.Color:=ColorBox4.Selected;
end;

procedure TFPanelGraph.ComboBox4Change(Sender: TObject);
begin
  case ComboBox4.ItemIndex of
    0: Chart1.Legend.Alignment:=laBottom;
    1: Chart1.Legend.Alignment:=laLeft;
    2: Chart1.Legend.Alignment:=laRight;
    3: Chart1.Legend.Alignment:=laTop;
  end;
end;

procedure TFPanelGraph.CheckBox5Click(Sender: TObject);
begin
  RefreshLegend;
  Chart1.Legend.Visible:=CheckBox5.Checked;
end;

procedure TFPanelGraph.CheckBox1Click(Sender: TObject);
begin
  RefreshLegend;
end;

procedure TFPanelGraph.ComboBox1Change(Sender: TObject);
begin
  CheckBox1.Checked:=True;
  RefreshLegend;
end;

procedure TFPanelGraph.ComboBox2Change(Sender: TObject);
begin
  CheckBox2.Checked:=True;
  RefreshLegend;
end;

procedure TFPanelGraph.ComboBox3Change(Sender: TObject);
begin
  CheckBox3.Checked:=True;
  RefreshLegend;
end;

procedure TFPanelGraph.ColorBox5Change(Sender: TObject);
begin
  Chart1.BackColor:=ColorBox5.Selected;
end;

procedure TFPanelGraph.SpeedButton3Click(Sender: TObject);
begin
  if SaveDialog1.Execute then
    Chart1.SaveToBitmapFile(SaveDialog1.FileName);
end;

procedure TFPanelGraph.CheckBox4Click(Sender: TObject);
begin
  Chart1.View3D:=CheckBox4.Checked;
end;



end.
