unit uFDlgClassEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Spin, Math;

type
  TFDlgClassEdit = class(TForm)
    Label10: TLabel;
    EdObjName: TEdit;
    Label4: TLabel;
    Bevel1: TBevel;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    EdName: TEdit;
    RadioGroup1: TRadioGroup;
    EdSpeed1: TEdit;
    RadioGroup2: TRadioGroup;
    EdSAngl1: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    RadioGroup3: TRadioGroup;
    RadioGroup4: TRadioGroup;
    EdAcc1: TEdit;
    EdAAngl1: TEdit;
    RadioGroup5: TRadioGroup;
    RadioGroup6: TRadioGroup;
    EdMass1: TEdit;
    ColorBox2: TColorBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    ColorBox1: TColorBox;
    SpinEdit9: TSpinEdit;
    SpinEdit8: TSpinEdit;
    SpinEdit7: TSpinEdit;
    SpinEdit6: TSpinEdit;
    SpinEdit5: TSpinEdit;
    SpinEdit4: TSpinEdit;
    SpinEdit3: TSpinEdit;
    SpinEdit2: TSpinEdit;
    SpinEdit14: TSpinEdit;
    SpinEdit13: TSpinEdit;
    SpEdBW: TSpinEdit;
    SpinEdit12: TSpinEdit;
    SpinEdit11: TSpinEdit;
    SpinEdit10: TSpinEdit;
    SpinEdit1: TSpinEdit;
    SpEditRad: TSpinEdit;
    Button4: TButton;
    procedure Button3Click(Sender: TObject);
    procedure EdSpeed1Change(Sender: TObject);
    procedure EdSAngl1Change(Sender: TObject);
    procedure EdAcc1Change(Sender: TObject);
    procedure EdAAngl1Change(Sender: TObject);
    procedure EdMass1Change(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure SpinEdit3Change(Sender: TObject);
    procedure SpinEdit5Change(Sender: TObject);
    procedure SpinEdit7Change(Sender: TObject);
    procedure SpinEdit9Change(Sender: TObject);
    procedure SpinEdit11Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdNameClick(Sender: TObject);
    procedure SpEditRadChange(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure SaveDefaultValues;
    procedure LoadDefaultValues;
  end;

var
  FDlgClassEdit: TFDlgClassEdit;

implementation

uses uFMain;

{$R *.dfm}

{-----------------------------------------------------------------------------}
{  procedure     SaveDefaultValues                                            }
{                                                                             }
{  discription   ���������� ���������� ������ �� ���������                    }
{-----------------------------------------------------------------------------}
procedure TFDlgClassEdit.SaveDefaultValues;
begin
  with FMain.DefaultClass do
  begin
    ObjName:=EdObjName.Text;
    ClassName1:=EdName.Text;
    V0:=StrToFloat(EdSpeed1.Text);
    AngleV:=DegToRad(StrToFloat(EdSAngl1.Text));
    F0:=StrToFloat(EdAcc1.Text);
    AngleF:=DegToRad(StrToFloat(EdAAngl1.Text));
    Radius:=SpEditRad.Value;
    Mass:=StrToFloat(EdMass1.Text);
    FillColor:=ColorBox1.Selected;
    BorderColor:=ColorBox2.Selected;
    BorderWidth:=SpEdBW.Value;

    Val1:=RadioGroup1.ItemIndex;
    Val2:=RadioGroup2.ItemIndex;
    Val3:=RadioGroup3.ItemIndex;
    Val4:=RadioGroup4.ItemIndex;
    Val5:=RadioGroup5.ItemIndex;
    Val6:=RadioGroup6.ItemIndex;

    Int1:=SpinEdit1.Value;
    Int2:=SpinEdit2.Value;
    Int3:=SpinEdit3.Value;
    Int4:=SpinEdit4.Value;
    Int5:=SpinEdit5.Value;
    Int6:=SpinEdit6.Value;
    Int7:=SpinEdit7.Value;
    Int8:=SpinEdit8.Value;
    Int9:=SpinEdit9.Value;
    Int10:=SpinEdit10.Value;
    Int11:=SpinEdit11.Value;
    Int12:=SpinEdit12.Value;
    Int13:=SpinEdit13.Value;
    Int14:=SpinEdit14.Value;

    Ch1:=CheckBox1.Checked;
    Ch2:=CheckBox2.Checked;
    Ch3:=CheckBox3.Checked;
  end;
  FMain.SaveDefaultClass;
end;

{-----------------------------------------------------------------------------}
{  procedure     LoadDefaultValues                                            }
{                                                                             }
{  discription   �������� ���������� ������ �� ���������                      }
{-----------------------------------------------------------------------------}
procedure TFDlgClassEdit.LoadDefaultValues;
begin
  with FMain.DefaultClass do
  begin
    EdName.Text:=ClassName1;
    EdObjName.Text:=ObjName;
    EdSpeed1.Text:=FloatToStr(V0);
    EdSAngl1.Text:=FloatToStr(RadToDeg(AngleV));
    EdAcc1.Text:=FloatToStr(F0);
    EdAAngl1.Text:=FloatToStr(RadToDeg(AngleF));
    EdMass1.Text:=FloatToStr(Mass);
    SpEditRad.Value:=Radius;
    ColorBox1.Selected:=FillColor;
    ColorBox2.Selected:=BorderColor;
    SpEdBW.Value:=BorderWidth;

    SpinEdit1.Value:=Int1;
    SpinEdit2.Value:=Int2;
    SpinEdit3.Value:=Int3;
    SpinEdit4.Value:=Int4;
    SpinEdit5.Value:=Int5;
    SpinEdit6.Value:=Int6;
    SpinEdit7.Value:=Int7;
    SpinEdit8.Value:=Int8;
    SpinEdit9.Value:=Int9;
    SpinEdit10.Value:=Int10;
    SpinEdit11.Value:=Int11;
    SpinEdit12.Value:=Int12;
    SpinEdit13.Value:=Int13;
    SpinEdit14.Value:=Int14;

    RadioGroup1.ItemIndex:=Val1;
    RadioGroup2.ItemIndex:=Val2;
    RadioGroup3.ItemIndex:=Val3;
    RadioGroup4.ItemIndex:=Val4;
    RadioGroup5.ItemIndex:=Val5;
    RadioGroup6.ItemIndex:=Val6;

    CheckBox1.Checked:=Ch1;
    CheckBox2.Checked:=Ch2;
    CheckBox3.Checked:=Ch3;
  end;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             FDlgClassEdit                                   }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFDlgClassEdit.FormShow(Sender: TObject);
begin
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
  EdName.SetFocus;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             <Other procedures>                              }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFDlgClassEdit.Button3Click(Sender: TObject);
begin
  SaveDefaultValues;
end;

procedure TFDlgClassEdit.Button4Click(Sender: TObject);
begin
  LoadDefaultValues;
end;

procedure TFDlgClassEdit.EdSpeed1Change(Sender: TObject);
begin
  RadioGroup1.ItemIndex:=0;
end;

procedure TFDlgClassEdit.EdSAngl1Change(Sender: TObject);
begin
  RadioGroup2.ItemIndex:=0;
end;

procedure TFDlgClassEdit.EdAcc1Change(Sender: TObject);
begin
  RadioGroup3.ItemIndex:=0;
end;

procedure TFDlgClassEdit.EdAAngl1Change(Sender: TObject);
begin
  RadioGroup4.ItemIndex:=0;
end;

procedure TFDlgClassEdit.SpEditRadChange(Sender: TObject);
begin
  RadioGroup5.ItemIndex:=0;
end;

procedure TFDlgClassEdit.EdMass1Change(Sender: TObject);
begin
  RadioGroup6.ItemIndex:=0;
end;

procedure TFDlgClassEdit.SpinEdit1Change(Sender: TObject);
begin
  RadioGroup1.ItemIndex:=1;
end;

procedure TFDlgClassEdit.SpinEdit3Change(Sender: TObject);
begin
  RadioGroup2.ItemIndex:=1;
end;

procedure TFDlgClassEdit.SpinEdit5Change(Sender: TObject);
begin
  RadioGroup3.ItemIndex:=1;
end;

procedure TFDlgClassEdit.SpinEdit7Change(Sender: TObject);
begin
  RadioGroup4.ItemIndex:=1;
end;

procedure TFDlgClassEdit.SpinEdit9Change(Sender: TObject);
begin
  RadioGroup5.ItemIndex:=1;
end;

procedure TFDlgClassEdit.SpinEdit11Change(Sender: TObject);
begin
  RadioGroup6.ItemIndex:=1;
end;

procedure TFDlgClassEdit.EdNameClick(Sender: TObject);
begin
  (Sender as TEdit).SelectAll;
end;

end.
