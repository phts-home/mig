unit uFDlgJPEG;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls;

type
  TFDlgJPEG = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    Button2: TButton;
    TrackBar1: TTrackBar;
    procedure TrackBar1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    CompValue: Integer;
    { Public declarations }
  end;

var
  FDlgJPEG: TFDlgJPEG;

implementation

uses uFMain;

{$R *.dfm}

procedure TFDlgJPEG.TrackBar1Change(Sender: TObject);
begin
  CompValue:=TrackBar1.Position * 10;
  Label2.Caption:=IntToStr(CompValue)+'%';
  if CompValue = 0 then CompValue:=1;
end;

procedure TFDlgJPEG.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CompValue:=TrackBar1.Position * 10;
end;

procedure TFDlgJPEG.FormCreate(Sender: TObject);
begin
  CompValue:=90;
end;

procedure TFDlgJPEG.FormShow(Sender: TObject);
begin
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
end;

end.
