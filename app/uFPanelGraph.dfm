object FPanelGraph: TFPanelGraph
  Left = 355
  Top = 98
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderStyle = bsToolWindow
  Caption = #1043#1088#1072#1092#1080#1082#1080
  ClientHeight = 265
  ClientWidth = 486
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 5
    Top = 5
    Width = 476
    Height = 236
    ActivePage = TabSheet2
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1054#1090#1086#1073#1088#1072#1078#1077#1085#1080#1077
      object Chart1: TChart
        Left = 0
        Top = 0
        Width = 468
        Height = 208
        BackWall.Brush.Color = clWhite
        BackWall.Color = clWhite
        Gradient.Direction = gdBottomTop
        MarginBottom = 3
        MarginTop = 3
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BackColor = clWhite
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.Labels = False
        BottomAxis.LogarithmicBase = 1
        BottomAxis.Maximum = 200.000000000000000000
        BottomAxis.Visible = False
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.Maximum = 1000.000000000000000000
        Legend.ShadowSize = 1
        Legend.Visible = False
        View3D = False
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 0
        object Series1: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          LinePen.Color = clRed
          LinePen.Width = 2
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series2: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clBlue
          ShowInLegend = False
          LinePen.Color = clBlue
          LinePen.Width = 2
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series3: TFastLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clGreen
          ShowInLegend = False
          LinePen.Color = clGreen
          LinePen.Width = 2
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1075#1088#1072#1092#1080#1082#1086#1074
      ImageIndex = 1
      object GroupBox1: TGroupBox
        Left = 5
        Top = 15
        Width = 146
        Height = 86
        Caption = '      '
        TabOrder = 3
        object Label1: TLabel
          Left = 10
          Top = 22
          Width = 34
          Height = 13
          Caption = #1050#1083#1072#1089#1089':'
        end
        object ComboBox1: TComboBox
          Left = 65
          Top = 20
          Width = 71
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          TabOrder = 0
          OnChange = ComboBox1Change
        end
        object ColorBox1: TColorBox
          Left = 65
          Top = 39
          Width = 71
          Height = 18
          Selected = clRed
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 1
          OnChange = ColorBox1Change
        end
        object SpinEdit1: TSpinEdit
          Left = 65
          Top = 59
          Width = 71
          Height = 22
          EditorEnabled = False
          MaxValue = 10
          MinValue = 1
          TabOrder = 2
          Value = 2
          OnChange = SpinEdit1Change
        end
      end
      object GroupBox2: TGroupBox
        Left = 160
        Top = 15
        Width = 146
        Height = 86
        Caption = '      '
        TabOrder = 5
        object Label5: TLabel
          Left = 10
          Top = 22
          Width = 34
          Height = 13
          Caption = #1050#1083#1072#1089#1089':'
        end
        object ComboBox2: TComboBox
          Left = 65
          Top = 20
          Width = 71
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          TabOrder = 0
          OnChange = ComboBox2Change
        end
        object ColorBox2: TColorBox
          Left = 65
          Top = 39
          Width = 71
          Height = 18
          Selected = clBlue
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 1
          OnChange = ColorBox2Change
        end
        object SpinEdit2: TSpinEdit
          Left = 65
          Top = 59
          Width = 71
          Height = 22
          EditorEnabled = False
          MaxValue = 10
          MinValue = 1
          TabOrder = 2
          Value = 2
          OnChange = SpinEdit2Change
        end
      end
      object GroupBox3: TGroupBox
        Left = 315
        Top = 15
        Width = 146
        Height = 86
        Caption = '      '
        TabOrder = 7
        object Label6: TLabel
          Left = 10
          Top = 22
          Width = 34
          Height = 13
          Caption = #1050#1083#1072#1089#1089':'
        end
        object ComboBox3: TComboBox
          Left = 65
          Top = 20
          Width = 71
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          TabOrder = 0
          OnChange = ComboBox3Change
        end
        object ColorBox3: TColorBox
          Left = 65
          Top = 39
          Width = 71
          Height = 18
          Selected = clGreen
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 1
          OnChange = ColorBox3Change
        end
        object SpinEdit3: TSpinEdit
          Left = 65
          Top = 59
          Width = 71
          Height = 22
          EditorEnabled = False
          MaxValue = 10
          MinValue = 1
          TabOrder = 2
          Value = 2
          OnChange = SpinEdit3Change
        end
      end
      object GroupBox4: TGroupBox
        Left = 5
        Top = 110
        Width = 146
        Height = 86
        Caption = ' '#1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100' '
        TabOrder = 0
        object Label4: TLabel
          Left = 9
          Top = 25
          Width = 52
          Height = 13
          Caption = #1048#1085#1090#1077#1088#1074#1072#1083':'
        end
        object Label9: TLabel
          Left = 75
          Top = 25
          Width = 61
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '100 '#1084#1089
        end
        object TrackBar1: TTrackBar
          Left = 3
          Top = 45
          Width = 140
          Height = 26
          Max = 20
          Min = 1
          Position = 1
          TabOrder = 0
          ThumbLength = 15
          OnChange = TrackBar1Change
        end
      end
      object RadioGroup1: TRadioGroup
        Left = 160
        Top = 110
        Width = 301
        Height = 86
        Caption = ' '#1042#1077#1083#1080#1095#1080#1085#1072' '
        ItemIndex = 0
        Items.Strings = (
          #1058#1077#1084#1087#1077#1088#1072#1090#1091#1088#1072' ('#1050')'
          #1057#1088#1077#1076#1085#1103#1103' '#1082#1080#1085'. '#1101#1085#1077#1088#1075#1080#1103' ('#1044#1078')'
          #1057#1091#1084#1084#1072#1088#1085#1072#1103' '#1082#1080#1085'. '#1101#1085#1077#1088#1075#1080#1103' ('#1044#1078')')
        TabOrder = 1
      end
      object CheckBox1: TCheckBox
        Left = 15
        Top = 13
        Width = 76
        Height = 17
        Caption = #1043#1088#1072#1092#1080#1082' #1'
        Checked = True
        State = cbChecked
        TabOrder = 2
        OnClick = CheckBox1Click
      end
      object CheckBox2: TCheckBox
        Left = 170
        Top = 13
        Width = 76
        Height = 17
        Caption = #1043#1088#1072#1092#1080#1082' #2'
        TabOrder = 4
        OnClick = CheckBox1Click
      end
      object CheckBox3: TCheckBox
        Left = 325
        Top = 13
        Width = 76
        Height = 17
        Caption = #1043#1088#1072#1092#1080#1082' #3'
        TabOrder = 6
        OnClick = CheckBox1Click
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103
      ImageIndex = 2
      object GroupBox9: TGroupBox
        Left = 160
        Top = 110
        Width = 146
        Height = 86
        Caption = '        '
        TabOrder = 4
        object ClrBxY: TColorBox
          Left = 65
          Top = 50
          Width = 71
          Height = 18
          Selected = clGray
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 2
          OnChange = ClrBxYChange
        end
        object ChBxYGr: TCheckBox
          Left = 10
          Top = 25
          Width = 56
          Height = 17
          Caption = #1057#1077#1090#1082#1072
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = ChBxYGrClick
        end
        object ComboBox5: TComboBox
          Left = 65
          Top = 25
          Width = 71
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          ItemIndex = 4
          TabOrder = 1
          Text = 'Dot'
          OnChange = ComboBox5Change
          Items.Strings = (
            'Clear'
            'Dash'
            'DashDot'
            'DashDotDot'
            'Dot'
            'InsideFrame'
            'Solid')
        end
      end
      object GroupBox6: TGroupBox
        Left = 5
        Top = 110
        Width = 146
        Height = 86
        Caption = '     '
        TabOrder = 2
        object Label12: TLabel
          Left = 10
          Top = 52
          Width = 25
          Height = 13
          Caption = #1062#1074#1077#1090
        end
        object ColorBox4: TColorBox
          Left = 65
          Top = 50
          Width = 71
          Height = 18
          Selected = clWhite
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 1
          OnChange = ColorBox4Change
        end
        object ComboBox4: TComboBox
          Left = 65
          Top = 25
          Width = 71
          Height = 18
          Style = csOwnerDrawFixed
          ItemHeight = 12
          ItemIndex = 2
          TabOrder = 0
          Text = #1057#1087#1088#1072#1074#1072
          OnChange = ComboBox4Change
          Items.Strings = (
            #1042#1085#1080#1079#1091
            #1057#1083#1077#1074#1072
            #1057#1087#1088#1072#1074#1072
            #1042#1074#1077#1088#1093#1091)
        end
      end
      object GroupBox8: TGroupBox
        Left = 5
        Top = 15
        Width = 146
        Height = 86
        Caption = ' '#1054#1092#1086#1088#1084#1083#1077#1085#1080#1077' '
        TabOrder = 0
        object Label13: TLabel
          Left = 7
          Top = 52
          Width = 23
          Height = 13
          Caption = #1060#1086#1085
        end
        object ColorBox5: TColorBox
          Left = 65
          Top = 50
          Width = 71
          Height = 18
          Selected = clWhite
          Style = [cbStandardColors, cbPrettyNames]
          ItemHeight = 12
          TabOrder = 0
          OnChange = ColorBox5Change
        end
        object CheckBox4: TCheckBox
          Left = 10
          Top = 20
          Width = 61
          Height = 17
          Caption = '3D'
          TabOrder = 1
          OnClick = CheckBox4Click
        end
      end
      object CheckBox5: TCheckBox
        Left = 15
        Top = 108
        Width = 66
        Height = 17
        Caption = #1051#1077#1075#1077#1085#1076#1072
        TabOrder = 1
        OnClick = CheckBox5Click
      end
      object ChBxYEn: TCheckBox
        Left = 170
        Top = 108
        Width = 53
        Height = 17
        Caption = #1054#1089#1100' Y'
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = ChBxYEnClick
      end
      object GroupBox10: TGroupBox
        Left = 315
        Top = 20
        Width = 146
        Height = 176
        Caption = ' '#1052#1072#1089#1096#1090#1072#1073' ('#1086#1089#1100' Y) '
        TabOrder = 5
        object Label17: TLabel
          Left = 10
          Top = 40
          Width = 31
          Height = 13
          Caption = 'max = '
        end
        object Label3: TLabel
          Left = 10
          Top = 65
          Width = 28
          Height = 13
          Caption = 'min = '
        end
        object Label15: TLabel
          Left = 10
          Top = 90
          Width = 42
          Height = 13
          Caption = '1 '#1076#1077#1083'. = '
        end
        object ChBxYAu: TCheckBox
          Left = 10
          Top = 20
          Width = 56
          Height = 17
          Caption = #1040#1074#1090#1086
          TabOrder = 0
          OnClick = ChBxYAuClick
        end
        object SpEdY: TSpinEdit
          Left = 70
          Top = 35
          Width = 66
          Height = 22
          EditorEnabled = False
          Increment = 100
          MaxValue = 0
          MinValue = 0
          TabOrder = 1
          Value = 100
          OnChange = SpEdYChange
        end
        object SpEdYmin: TSpinEdit
          Left = 70
          Top = 60
          Width = 66
          Height = 22
          EditorEnabled = False
          Increment = 100
          MaxValue = 0
          MinValue = 0
          TabOrder = 2
          Value = 0
          OnChange = SpEdYminChange
        end
        object EdUnit: TEdit
          Left = 70
          Top = 85
          Width = 66
          Height = 21
          TabOrder = 3
          Text = '1E30'
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 5
    Top = 245
    Width = 476
    Height = 18
    BevelOuter = bvNone
    TabOrder = 1
    object SpeedButton1: TSpeedButton
      Left = 0
      Top = 0
      Width = 41
      Height = 16
      Caption = #1057#1073#1088#1086#1089
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 365
      Top = 0
      Width = 111
      Height = 16
      Caption = #1057#1086#1093#1088'. '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
      OnClick = SpeedButton2Click
    end
    object SpeedButton3: TSpeedButton
      Left = 165
      Top = 0
      Width = 76
      Height = 16
      Caption = #1057#1086#1093#1088'. '#1074' BMP'
      OnClick = SpeedButton3Click
    end
    object SpeedButton4: TSpeedButton
      Left = 250
      Top = 0
      Width = 111
      Height = 16
      Caption = #1047#1072#1075#1088'. '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
      OnClick = SpeedButton4Click
    end
  end
  object Panel1: TPanel
    Left = 435
    Top = 5
    Width = 46
    Height = 17
    BevelOuter = bvNone
    TabOrder = 2
    object ChBxEn: TCheckBox
      Left = 0
      Top = 0
      Width = 46
      Height = 17
      Caption = #1042#1082#1083
      TabOrder = 0
      OnClick = ChBxEnClick
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 250
    OnTimer = Timer1Timer
    Left = 15
    Top = 200
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'bmp'
    Filter = 'BMP Images (*.bmp)|*.bmp'
    Left = 44
    Top = 199
  end
end
