unit uFDlgObjAddRegion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, Buttons;

type
  TFDlgObjAddRegion = class(TForm)
    ButApply: TButton;
    ButClose: TButton;
    GroupBox1: TGroupBox;
    SpEdLeftTopX: TSpinEdit;
    SpEdLeftTopY: TSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    SpEdRightBotX: TSpinEdit;
    SpEdRightBotY: TSpinEdit;
    Label3: TLabel;
    Label4: TLabel;
    GroupBox3: TGroupBox;
    SpEdAmount: TSpinEdit;
    GroupBox4: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    SpEdClX: TSpinEdit;
    SpEdClY: TSpinEdit;
    SpButPick001: TSpeedButton;
    SpButPick002: TSpeedButton;
    CheckBox1: TCheckBox;
    procedure ButApplyClick(Sender: TObject);
    procedure SpButPick001Click(Sender: TObject);
    procedure SpButPick002Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDlgObjAddRegion: TFDlgObjAddRegion;

implementation

uses uFMain, uFDlgObjAddRegionPick;

{$R *.dfm}

procedure TFDlgObjAddRegion.ButApplyClick(Sender: TObject);
var
  W, H, OW: Real;
  I, J, nw, nh, xx, yy, Amount: Integer;
begin
  OW:=FMain.Project.ProjClasses[FMain.Project.ProjActiveClass].Radius * 2;
  W:=SpEdRightBotX.Value - SpEdLeftTopX.Value - OW;
  H:=SpEdLeftTopY.Value - SpEdRightBotY.Value - OW;
  nw:=0;
  while (OW+SpEdClX.Value)*nw < W do
    Inc(nw);
  nh:=0;
  while (OW+SpEdClY.Value)*nh < H do
    Inc(nh);
  Amount:=0;
  for I:=1 to nh do
  begin
    for J:=1 to nw do
    begin
      xx:=SpEdLeftTopX.Value + Round(J*(OW+SpEdClX.Value)-OW/2);
      yy:=SpEdRightBotY.Value + Round(I*(OW+SpEdClY.Value)-OW/2);
      FMain.QuickAddObject(Round(xx/FMain.Project.ProjScale),
          Round(yy/FMain.Project.ProjScale));
      Inc(Amount);
      if(not CheckBox1.Checked)and(Amount = SpEdAmount.Value)then Break;
    end;
    if(not CheckBox1.Checked)and(Amount = SpEdAmount.Value)then Break;
  end;
  FMain.ReloadObjectList;
end;

procedure TFDlgObjAddRegion.SpButPick001Click(Sender: TObject);
begin
  FDlgObjAddRegionPick.ShowModal;
  if not FDlgObjAddRegionPick.CancelPressed then
  begin
    SpEdLeftTopX.Value:=FDlgObjAddRegionPick.PickX;
    SpEdLeftTopY.Value:=FDlgObjAddRegionPick.PickY;
  end;
end;

procedure TFDlgObjAddRegion.SpButPick002Click(Sender: TObject);
begin
  FDlgObjAddRegionPick.ShowModal;
  if not FDlgObjAddRegionPick.CancelPressed then
  begin
    SpEdRightBotX.Value:=FDlgObjAddRegionPick.PickX;
    SpEdRightBotY.Value:=FDlgObjAddRegionPick.PickY;
  end;
end;

procedure TFDlgObjAddRegion.FormShow(Sender: TObject);
begin
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
  SpEdLeftTopX.MinValue:=
    Round(FMain.Project.ProjClasses[FMain.Project.ProjActiveClass].Radius)+1;
  SpEdLeftTopY.MinValue:=
    Round(FMain.Project.ProjClasses[FMain.Project.ProjActiveClass].Radius)+1;
  SpEdLeftTopX.MaxValue:=
    FMain.Project.ProjWidth -
    Round(FMain.Project.ProjClasses[FMain.Project.ProjActiveClass].Radius)-1;
  SpEdLeftTopY.MaxValue:=
    FMain.Project.ProjHeight -
    Round(FMain.Project.ProjClasses[FMain.Project.ProjActiveClass].Radius)-1;
  SpEdRightBotX.MinValue:=SpEdLeftTopX.MinValue;
  SpEdRightBotX.MaxValue:=SpEdLeftTopX.MaxValue;
  SpEdRightBotY.MinValue:=SpEdLeftTopY.MinValue;
  SpEdRightBotY.MaxValue:=SpEdLeftTopY.MaxValue;
end;

procedure TFDlgObjAddRegion.CheckBox1Click(Sender: TObject);
begin
  SpEdAmount.Enabled:=not CheckBox1.Checked;
end;

end.
