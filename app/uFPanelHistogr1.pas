unit uFPanelHistogr1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TeEngine, Series, ExtCtrls, TeeProcs, Chart, ComCtrls,
  Spin, Buttons, IniFiles;

type
  TFPanelHistogr1 = class(TForm)
    Chart1: TChart;
    Series1: TBarSeries;
    Timer1: TTimer;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    TabSheet3: TTabSheet;
    SaveDialog1: TSaveDialog;
    GroupBox3: TGroupBox;
    ClrBxX: TColorBox;
    ChBxXGr: TCheckBox;
    ChBxXEn: TCheckBox;
    ChBxYEn: TCheckBox;
    GroupBox6: TGroupBox;
    ClrBxY: TColorBox;
    ChBxYGr: TCheckBox;
    GroupBox5: TGroupBox;
    Label5: TLabel;
    CheckBox3: TCheckBox;
    ColorBox2: TColorBox;
    CheckBox1: TCheckBox;
    GroupBox4: TGroupBox;
    ChBxXAu: TCheckBox;
    SpEdX: TSpinEdit;
    ComboBox4: TComboBox;
    ComboBox5: TComboBox;
    GroupBox7: TGroupBox;
    SpEdY: TSpinEdit;
    ChBxYAu: TCheckBox;
    TrackBar1: TTrackBar;
    Label2: TLabel;
    SpeedButton2: TSpeedButton;
    Series2: TBarSeries;
    SpeedButton1: TSpeedButton;
    CheckBox2: TCheckBox;
    CheckBox4: TCheckBox;
    GroupBox2: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    ComboBox3: TComboBox;
    ComboBox2: TComboBox;
    ColorBox1: TColorBox;
    ColorBox3: TColorBox;
    Label7: TLabel;
    Label1: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    GroupBox8: TGroupBox;
    Label3: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    ComboBox6: TComboBox;
    ComboBox7: TComboBox;
    ColorBox4: TColorBox;
    ColorBox5: TColorBox;
    ComboBox1: TComboBox;
    ComboBox8: TComboBox;
    Label4: TLabel;
    SpeedButton3: TSpeedButton;
    SpinEdit1: TSpinEdit;
    Label14: TLabel;
    CheckBox5: TCheckBox;
    RadioGroup1: TRadioGroup;
    Panel1: TPanel;
    ChBxEn: TCheckBox;
    EdUnit: TEdit;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ChBxEnClick(Sender: TObject);
    procedure ChBxXEnClick(Sender: TObject);
    procedure ChBxYEnClick(Sender: TObject);
    procedure ChBxXGrClick(Sender: TObject);
    procedure ClrBxXChange(Sender: TObject);
    procedure ChBxYGrClick(Sender: TObject);
    procedure ClrBxYChange(Sender: TObject);
    procedure ChBxXAuClick(Sender: TObject);
    procedure ChBxYAuClick(Sender: TObject);
    procedure SpEdXChange(Sender: TObject);
    procedure SpEdYChange(Sender: TObject);
    procedure ColorBox2Change(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckBox1Click(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure ColorBox3Change(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
    procedure ComboBox4Change(Sender: TObject);
    procedure ComboBox5Change(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure ComboBox8Change(Sender: TObject);
    procedure ColorBox5Change(Sender: TObject);
    procedure ComboBox6Change(Sender: TObject);
    procedure ComboBox7Change(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure CheckBox4Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure ColorBox1Change(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure ColorBox4Change(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure CheckBox5Click(Sender: TObject);
  private
    { Private declarations }
  public
    DefValuesFile: TMemIniFile;
    Values1, Values2: Array [0..2047] of Integer;
    procedure ResetValues;
    procedure WriteSpeeds1;
    procedure WriteSpeeds2;
    procedure WriteEnergies1;
    procedure WriteEnergies2;
    procedure DrawValues1;
    procedure DrawValues2;
    procedure SaveDefaultValues;
    procedure LoadDefaultValues;
    procedure ApplyParams;
  end;

var
  FPanelHistogr1: TFPanelHistogr1;

implementation

uses uFMain, uFPanelProp;

{$R *.dfm}

{-----------------------------------------------------------------------------}
{  procedure     ResetValues                                                  }
{                                                                             }
{  discription   ����� ���� �������� 2-� ���������� �� 0                      }
{-----------------------------------------------------------------------------}
procedure TFPanelHistogr1.ResetValues;
var i: Integer;
begin
  for i:=0 to 2047 do
  begin
    Values1[i]:=0;
    Values2[i]:=0;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     WriteSpeeds1                                                 }
{                                                                             }
{  discription   ���������� ����� �������� ��������� ��� 1-� �����������      }
{-----------------------------------------------------------------------------}
procedure TFPanelHistogr1.WriteSpeeds1;
var
  i, ind: Integer;
  vv: Real;
  sc: Real;
begin
  try
    sc:=StrToFloat(EdUnit.Text);
  except
    sc:=0;
  end;
  if CheckBox2.Checked then
  begin
    if RadioButton1.Checked then
      for i:=1 to FMain.Project.ProjObjectsCount do
        with FMain.Project.ProjObjects[i] do
        begin
          vv:=Sqrt(Vx*Vx + Vy*Vy);
          ind:=Trunc(vv*sc*FMain.Project.ProjScale);
          if ind > 2047 then ind:=2047;
          Inc(Values1[ind]);
          //Inc(Values1[Trunc(vv*sc*FMain.Project.ProjScale)]);
        end
    else
      for i:=1 to FMain.Project.ProjObjectsCount do
        with FMain.Project.ProjObjects[i] do
          if (ComboBox1.ItemIndex<>-1)and(ClassID = ComboBox1.ItemIndex+1) then
          begin
            vv:=Sqrt(Vx*Vx + Vy*Vy);
            ind:=Trunc(vv*sc*FMain.Project.ProjScale);
            if ind > 2047 then ind:=2047;
            Inc(Values1[ind]);
            //Inc(Values1[Trunc(vv*sc*FMain.Project.ProjScale)]);
          end;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     WriteSpeeds2                                                 }
{                                                                             }
{  discription   ���������� ����� �������� ��������� ��� 2-� �����������      }
{-----------------------------------------------------------------------------}
procedure TFPanelHistogr1.WriteSpeeds2;
var
  i, ind: Integer;
  vv: Real;
  sc: Real;
begin
  try
    sc:=StrToFloat(EdUnit.Text);
  except
    sc:=0;
  end;
  if CheckBox4.Checked then
  begin
    if RadioButton3.Checked then
      for i:=1 to FMain.Project.ProjObjectsCount do
        with FMain.Project.ProjObjects[i] do
        begin
          vv:=Sqrt(Vx*Vx + Vy*Vy);
          ind:=Trunc(vv*sc*FMain.Project.ProjScale);
          if ind > 2047 then ind:=2047;
          Inc(Values2[ind]);
        end
    else
      for i:=1 to FMain.Project.ProjObjectsCount do
        with FMain.Project.ProjObjects[i] do
          if (ComboBox1.ItemIndex<>-1)and(ClassID = ComboBox8.ItemIndex+1) then
          begin
            vv:=Sqrt(Vx*Vx + Vy*Vy);
            ind:=Trunc(vv*sc*FMain.Project.ProjScale);
            if ind > 2047 then ind:=2047;
            Inc(Values2[ind]);
          end;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     WriteEnergies1                                               }
{                                                                             }
{  discription   ���������� ����� �������� ������� ��� 1-� �����������        }
{-----------------------------------------------------------------------------}
procedure TFPanelHistogr1.WriteEnergies1;
var
  i, ind: Integer;
  vv: Real;
  sc: Real;
begin
  try
    sc:=StrToFloat(EdUnit.Text);
  except
    sc:=0;
  end;
  if CheckBox2.Checked then
  begin
    if RadioButton1.Checked then
      for i:=1 to FMain.Project.ProjObjectsCount do
        with FMain.Project.ProjObjects[i] do
        begin
          vv:=Mass*(Vx*Vx + Vy*Vy);
          ind:=Trunc(vv*sc*FMain.Project.ProjScale*FMain.Project.ProjScale);
          if ind > 2047 then ind:=2047;
          Inc(Values1[ind]);
          //Inc(Values1[Trunc(vv*sc*FMain.Project.ProjScale*FMain.Project.ProjScale)]);
        end
    else
      for i:=1 to FMain.Project.ProjObjectsCount do
        with FMain.Project.ProjObjects[i] do
          if (ComboBox1.ItemIndex<>-1)and(ClassID = ComboBox1.ItemIndex+1) then
          begin
            vv:=Mass*(Vx*Vx + Vy*Vy);
            ind:=Trunc(vv*sc*FMain.Project.ProjScale*FMain.Project.ProjScale);
            if ind > 2047 then ind:=2047;
            Inc(Values1[ind]);
            //Inc(Values1[Trunc(vv*sc*FMain.Project.ProjScale*FMain.Project.ProjScale)]);
          end;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     WriteEnergies2                                               }
{                                                                             }
{  discription   ���������� ����� �������� ������� ��� 2-� �����������        }
{-----------------------------------------------------------------------------}
procedure TFPanelHistogr1.WriteEnergies2;
var
  i, ind: Integer;
  vv: Real;
  sc: Real;
begin
  try
    sc:=StrToFloat(EdUnit.Text);
  except
    sc:=0;
  end;
  if CheckBox4.Checked then
  begin
    if RadioButton3.Checked then
      for i:=1 to FMain.Project.ProjObjectsCount do
        with FMain.Project.ProjObjects[i] do
        begin
          vv:=Mass*(Vx*Vx + Vy*Vy);
          ind:=Trunc(vv*sc*FMain.Project.ProjScale*FMain.Project.ProjScale);
          if ind > 2047 then ind:=2047;
          Inc(Values2[ind]);
          //Inc(Values2[Trunc(vv*sc*FMain.Project.ProjScale*FMain.Project.ProjScale)]);
        end
    else
      for i:=1 to FMain.Project.ProjObjectsCount do
        with FMain.Project.ProjObjects[i] do
          if (ComboBox1.ItemIndex<>-1)and(ClassID = ComboBox8.ItemIndex+1) then
          begin
            vv:=Mass*(Vx*Vx + Vy*Vy);
            ind:=Trunc(vv*sc*FMain.Project.ProjScale*FMain.Project.ProjScale);
            if ind > 2047 then ind:=2047;
            Inc(Values2[ind]);
            //Inc(Values2[Trunc(vv*sc*FMain.Project.ProjScale*FMain.Project.ProjScale)]);
          end;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     DrawValues1                                                  }
{                                                                             }
{  discription   ����� 1-� ����������� �� �����                               }
{-----------------------------------------------------------------------------}
procedure TFPanelHistogr1.DrawValues1;
var
  i, j: Integer;
begin
  Series1.Clear;
  for j:=2047 downto 0 do
    if Values1[j]<>0 then Break;
  for i:=0 to j+1 do
    Series1.Add(Values1[i],'',ColorBox1.Selected);
end;

{-----------------------------------------------------------------------------}
{  procedure     DrawValues2                                                  }
{                                                                             }
{  discription   ����� 2-� ����������� �� �����                               }
{-----------------------------------------------------------------------------}
procedure TFPanelHistogr1.DrawValues2;
var
  i, j: Integer;
begin
  Series2.Clear;
  for j:=2047 downto 0 do
    if Values2[j]<>0 then Break;
  for i:=0 to j+1 do
    Series2.Add(Values2[i],'',ColorBox4.Selected);
end;

{-----------------------------------------------------------------------------}
{  procedure     SaveDefaultValues                                            }
{                                                                             }
{  discription   ���������� ���������� ���������� �� ���������                }
{-----------------------------------------------------------------------------}
procedure TFPanelHistogr1.SaveDefaultValues;
begin
  CreateDir(ExtractFilePath(ParamStr(0))+'Data\');
  DefValuesFile:=TMemIniFile.Create(ExtractFilePath(ParamStr(0))+'Data\PanelHistogramDefValues.dat');
  with DefValuesFile do
  begin
    WriteBool('Graph#1','Visible',CheckBox2.Checked);
    WriteInteger('Graph#1','Type',ComboBox3.ItemIndex);
    WriteInteger('Graph#1','Style',ComboBox2.ItemIndex);
    WriteInteger('Graph#1','Color1',ColorBox1.Selected);
    WriteInteger('Graph#1','Color2',ColorBox3.Selected);

    WriteBool('Graph#2','Visible',CheckBox4.Checked);
    WriteInteger('Graph#2','Type',ComboBox6.ItemIndex);
    WriteInteger('Graph#2','Style',ComboBox7.ItemIndex);
    WriteInteger('Graph#2','Color1',ColorBox4.Selected);
    WriteInteger('Graph#2','Color2',ColorBox5.Selected);

    WriteInteger('Performance','Interval',TrackBar1.Position*100);

    WriteBool('Appearance','Enable3D',CheckBox3.Checked);
    WriteBool('Appearance','EnableMarks',CheckBox1.Checked);
    WriteInteger('Appearance','Backgr',ColorBox2.Selected);
    WriteInteger('Appearance','BarWidth',SpinEdit1.Value);
    WriteBool('Appearance','Monochrome',CheckBox5.Checked);

    WriteBool('BottomAxis','Visible',ChBxXEn.Checked);
    WriteBool('BottomAxis','Grid',ChBxXGr.Checked);
    WriteInteger('BottomAxis','GridStyle',ComboBox4.ItemIndex);
    WriteInteger('BottomAxis','GridColor',ClrBxX.Selected);
    WriteBool('BottomAxis','ScaleAuto',ChBxXAu.Checked);
    WriteInteger('BottomAxis','ScaleVal',SpEdX.Value);
    WriteString('BottomAxis','UnitScale',EdUnit.Text);

    WriteBool('LeftAxis','Visible',ChBxYEn.Checked);
    WriteBool('LeftAxis','Grid',ChBxYGr.Checked);
    WriteInteger('LeftAxis','GridStyle',ComboBox5.ItemIndex);
    WriteInteger('LeftAxis','GridColor',ClrBxY.Selected);
    WriteBool('LeftAxis','ScaleAuto',ChBxYAu.Checked);
    WriteInteger('LeftAxis','ScaleVal',SpEdY.Value);

    UpdateFile;
  end;
end;

{-----------------------------------------------------------------------------}
{  procedure     LoadDefaultValues                                            }
{                                                                             }
{  discription   �������� ���������� ���������� �� ���������                  }
{-----------------------------------------------------------------------------}
procedure TFPanelHistogr1.LoadDefaultValues;
begin
  if FileExists(ExtractFilePath(ParamStr(0))+'Data\PanelHistogramDefValues.dat') then
  begin
    DefValuesFile:=TMemIniFile.Create(ExtractFilePath(ParamStr(0))+'Data\PanelHistogramDefValues.dat');
    with DefValuesFile do
    begin
      CheckBox2.Checked:=ReadBool('Graph#1','Visible',True);
      ComboBox3.ItemIndex:=ReadInteger('Graph#1','Type',5);
      ComboBox2.ItemIndex:=ReadInteger('Graph#1','Style',6);
      ColorBox1.Selected:=ReadInteger('Graph#1','Color1',clRed);
      ColorBox3.Selected:=ReadInteger('Graph#1','Color2',clWhite);

      CheckBox4.Checked:=ReadBool('Graph#2','Visible',False);
      ComboBox6.ItemIndex:=ReadInteger('Graph#2','Type',5);
      ComboBox7.ItemIndex:=ReadInteger('Graph#2','Style',6);
      ColorBox4.Selected:=ReadInteger('Graph#2','Color1',clBlue);
      ColorBox5.Selected:=ReadInteger('Graph#2','Color2',clWhite);

      TrackBar1.Position:=ReadInteger('Performance','Interval',100) div 100;

      CheckBox3.Checked:=ReadBool('Appearance','Enable3D',False);
      CheckBox1.Checked:=ReadBool('Appearance','EnableMarks',False);
      ColorBox2.Selected:=ReadInteger('Appearance','Backgr',clWhite);
      SpinEdit1.Value:=ReadInteger('Appearance','BarWidth',15);
      CheckBox5.Checked:=ReadBool('Appearance','Monochrome',False);

      ChBxXEn.Checked:=ReadBool('BottomAxis','Visible',True);
      ChBxXGr.Checked:=ReadBool('BottomAxis','Grid',True);
      ComboBox4.ItemIndex:=ReadInteger('BottomAxis','GridStyle',4);
      ClrBxX.Selected:=ReadInteger('BottomAxis','GridColor',clGray);
      SpEdX.Value:=ReadInteger('BottomAxis','ScaleVal',20);
      EdUnit.Text:=ReadString('BottomAxis','UnitScale','100');
      ChBxXAu.Checked:=ReadBool('BottomAxis','ScaleAuto',True);

      ChBxYEn.Checked:=ReadBool('LeftAxis','Visible',True);
      ChBxYGr.Checked:=ReadBool('LeftAxis','Grid',True);
      ComboBox5.ItemIndex:=ReadInteger('LeftAxis','GridStyle',4);
      ClrBxY.Selected:=ReadInteger('LeftAxis','GridColor',clGray);
      SpEdY.Value:=ReadInteger('LeftAxis','ScaleVal',100);
      ChBxYAu.Checked:=ReadBool('LeftAxis','ScaleAuto',True);
    end;
  end
  else
  begin
    CheckBox2.Checked:=True;
    ComboBox3.ItemIndex:=5;
    ComboBox2.ItemIndex:=6;
    ColorBox1.Selected:=clRed;
    ColorBox3.Selected:=clWhite;

    CheckBox4.Checked:=False;
    ComboBox6.ItemIndex:=5;
    ComboBox7.ItemIndex:=6;
    ColorBox4.Selected:=clBlue;
    ColorBox5.Selected:=clWhite;

    TrackBar1.Position:=1;

    CheckBox3.Checked:=False;
    CheckBox1.Checked:=False;
    ColorBox2.Selected:=clWhite;
    SpinEdit1.Value:=15;
    CheckBox5.Checked:=False;

    ChBxXEn.Checked:=True;
    ChBxXGr.Checked:=True;
    ComboBox4.ItemIndex:=4;
    ClrBxX.Selected:=clGray;
    SpEdX.Value:=20;
    EdUnit.Text:='100';
    ChBxXAu.Checked:=True;

    ChBxYEn.Checked:=True;
    ChBxYGr.Checked:=True;
    ComboBox5.ItemIndex:=4;
    ClrBxY.Selected:=clGray;
    SpEdY.Value:=100;
    ChBxYAu.Checked:=True;
  end;
  ApplyParams;
end;

{-----------------------------------------------------------------------------}
{  procedure     ApplyParams                                                  }
{                                                                             }
{  discription   ���������� ���������� � ������������                         }
{-----------------------------------------------------------------------------}
procedure TFPanelHistogr1.ApplyParams;
begin
  Series1.Active:=CheckBox2.Checked;
  Series2.Active:=CheckBox4.Checked;
  case ComboBox3.ItemIndex of
    0: Series1.BarStyle:=bsArrow;
    1: Series1.BarStyle:=bsCilinder;
    2: Series1.BarStyle:=bsEllipse;
    3: Series1.BarStyle:=bsInvPyramid;
    4: Series1.BarStyle:=bsPyramid;
    5: Series1.BarStyle:=bsRectangle;
    6: Series1.BarStyle:=bsRectGradient;
  end;
  case ComboBox2.ItemIndex of
    0: Series1.BarBrush.Style:=bsBDiagonal;
    1: Series1.BarBrush.Style:=bsClear;
    2: Series1.BarBrush.Style:=bsCross;
    3: Series1.BarBrush.Style:=bsDiagCross;
    4: Series1.BarBrush.Style:=bsFDiagonal;
    5: Series1.BarBrush.Style:=bsHorizontal;
    6: Series1.BarBrush.Style:=bsSolid;
    7: Series1.BarBrush.Style:=bsVertical;
  end;
  Series1.BarBrush.Color:=ColorBox3.Selected;
  case ComboBox6.ItemIndex of
    0: Series2.BarStyle:=bsArrow;
    1: Series2.BarStyle:=bsCilinder;
    2: Series2.BarStyle:=bsEllipse;
    3: Series2.BarStyle:=bsInvPyramid;
    4: Series2.BarStyle:=bsPyramid;
    5: Series2.BarStyle:=bsRectangle;
    6: Series2.BarStyle:=bsRectGradient;
  end;
  case ComboBox7.ItemIndex of
    0: Series2.BarBrush.Style:=bsBDiagonal;
    1: Series2.BarBrush.Style:=bsClear;
    2: Series2.BarBrush.Style:=bsCross;
    3: Series2.BarBrush.Style:=bsDiagCross;
    4: Series2.BarBrush.Style:=bsFDiagonal;
    5: Series2.BarBrush.Style:=bsHorizontal;
    6: Series2.BarBrush.Style:=bsSolid;
    7: Series2.BarBrush.Style:=bsVertical;
  end;
  Series2.BarBrush.Color:=ColorBox5.Selected;
  Timer1.Interval:=TrackBar1.Position*100;
  Label2.Caption:=IntToStr(Timer1.Interval)+' ��';
  Chart1.View3D:=CheckBox3.Checked;
  Series1.Marks.Visible:=CheckBox1.Checked;
  Series2.Marks.Visible:=CheckBox1.Checked;
  Chart1.BackColor:=ColorBox2.Selected;
  Chart1.BottomAxis.Visible:=ChBxXEn.Checked;
  if not ChBxXEn.Checked then ChBxXGr.Checked:=False;
  Chart1.BottomAxis.Grid.Visible:=ChBxXGr.Checked;
  case ComboBox4.ItemIndex of
    0: Chart1.BottomAxis.Grid.Style:=psClear;
    1: Chart1.BottomAxis.Grid.Style:=psDash;
    2: Chart1.BottomAxis.Grid.Style:=psDashDot;
    3: Chart1.BottomAxis.Grid.Style:=psDashDotDot;
    4: Chart1.BottomAxis.Grid.Style:=psDot;
    5: Chart1.BottomAxis.Grid.Style:=psInsideFrame;
    6: Chart1.BottomAxis.Grid.Style:=psSolid;
  end;
  Chart1.BottomAxis.Grid.Color:=ClrBxX.Selected;
  if ChBxXAu.Checked then
  begin
    Chart1.BottomAxis.Automatic:=True;
  end
  else
  begin
    Chart1.BottomAxis.AutomaticMaximum:=False;
    Chart1.BottomAxis.AutomaticMinimum:=False;
    Chart1.BottomAxis.Maximum:=SpEdX.Value+1;
  end;
  Chart1.BottomAxis.Maximum:=SpEdX.Value+1;
  Chart1.LeftAxis.Visible:=ChBxYEn.Checked;
  if not ChBxYEn.Checked then ChBxYGr.Checked:=False;
  Chart1.LeftAxis.Grid.Visible:=ChBxYGr.Checked;
  case ComboBox5.ItemIndex of
    0: Chart1.LeftAxis.Grid.Style:=psClear;
    1: Chart1.LeftAxis.Grid.Style:=psDash;
    2: Chart1.LeftAxis.Grid.Style:=psDashDot;
    3: Chart1.LeftAxis.Grid.Style:=psDashDotDot;
    4: Chart1.LeftAxis.Grid.Style:=psDot;
    5: Chart1.LeftAxis.Grid.Style:=psInsideFrame;
    6: Chart1.LeftAxis.Grid.Style:=psSolid;
  end;
  Chart1.LeftAxis.Grid.Color:=ClrBxY.Selected;
  if ChBxYAu.Checked then
  begin
    Chart1.LeftAxis.Automatic:=True;
  end
  else
  begin
    Chart1.LeftAxis.AutomaticMaximum:=False;
    Chart1.LeftAxis.Maximum:=SpEdY.Value;
  end;
  Chart1.LeftAxis.Maximum:=SpEdY.Value;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                               FPanelHistogr1                                }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFPanelHistogr1.FormCreate(Sender: TObject);
var
  Rect: TRect;
begin
  SystemParametersInfo(SPI_GETWORKAREA,0,@Rect,0);
  Top:=FMain.cfg.ReadInteger('PanelHistogram','Top',Rect.Bottom - Height - 21);
  Left:=FMain.cfg.ReadInteger('PanelHistogram','Left',Rect.Right - Width - 3);
  LoadDefaultValues;
  SaveDialog1.InitialDir:=ExtractFilePath(ParamStr(0));
  Visible:=FMain.cfg.ReadBool('PanelHistogram','Visible',False);
  ResetValues;
  DrawValues1;
  DrawValues2;
end;

procedure TFPanelHistogr1.FormShow(Sender: TObject);
begin
  FMain.aShowHistogr1.Checked:=True;
end;

procedure TFPanelHistogr1.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FMain.aShowHistogr1.Checked:=False;
  ChBxEn.Checked:=False;
  Timer1.Enabled:=False;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             <Other procedures>                              }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFPanelHistogr1.Timer1Timer(Sender: TObject);
begin
  ResetValues;
  case RadioGroup1.ItemIndex of
    0: begin WriteSpeeds1; WriteSpeeds2; end;
    1: begin WriteEnergies1; WriteEnergies2; end;
  end;
  DrawValues1;
  DrawValues2;
end;

procedure TFPanelHistogr1.SpeedButton2Click(Sender: TObject);
begin
  SaveDefaultValues;
end;

procedure TFPanelHistogr1.SpeedButton3Click(Sender: TObject);
begin
  LoadDefaultValues;
end;

procedure TFPanelHistogr1.ChBxEnClick(Sender: TObject);
begin
  Timer1.Enabled:=ChBxEn.Checked;
  if ChBxEn.Checked then PageControl1.TabIndex:=0;
end;

procedure TFPanelHistogr1.SpeedButton1Click(Sender: TObject);
begin
  if SaveDialog1.Execute then
    Chart1.SaveToBitmapFile(SaveDialog1.FileName);
end;

procedure TFPanelHistogr1.CheckBox2Click(Sender: TObject);
begin
  Series1.Active:=CheckBox2.Checked;
end;

procedure TFPanelHistogr1.CheckBox4Click(Sender: TObject);
begin
  Series2.Active:=CheckBox4.Checked;
end;

procedure TFPanelHistogr1.ChBxXEnClick(Sender: TObject);
begin
  Chart1.BottomAxis.Visible:=ChBxXEn.Checked;
  if not ChBxXEn.Checked then ChBxXGr.Checked:=False;
end;

procedure TFPanelHistogr1.ChBxXGrClick(Sender: TObject);
begin
  Chart1.BottomAxis.Grid.Visible:=ChBxXGr.Checked;
end;

procedure TFPanelHistogr1.ClrBxXChange(Sender: TObject);
begin
  Chart1.BottomAxis.Grid.Color:=ClrBxX.Selected;
end;

procedure TFPanelHistogr1.ChBxYEnClick(Sender: TObject);
begin
  Chart1.LeftAxis.Visible:=ChBxYEn.Checked;
  if not ChBxYEn.Checked then ChBxYGr.Checked:=False;
end;

procedure TFPanelHistogr1.ChBxYGrClick(Sender: TObject);
begin
  Chart1.LeftAxis.Grid.Visible:=ChBxYGr.Checked;
end;

procedure TFPanelHistogr1.ClrBxYChange(Sender: TObject);
begin
  Chart1.LeftAxis.Grid.Color:=ClrBxY.Selected;
end;

procedure TFPanelHistogr1.ChBxXAuClick(Sender: TObject);
begin
  if ChBxXAu.Checked then
  begin
    Chart1.BottomAxis.Automatic:=True;
  end
  else
  begin
    Chart1.BottomAxis.AutomaticMaximum:=False;
    Chart1.BottomAxis.AutomaticMinimum:=False;
    Chart1.BottomAxis.Maximum:=SpEdX.Value+1;
  end;
end;

procedure TFPanelHistogr1.ChBxYAuClick(Sender: TObject);
begin
  if ChBxYAu.Checked then
  begin
    Chart1.LeftAxis.Automatic:=True;
  end
  else
  begin
    Chart1.LeftAxis.AutomaticMaximum:=False;
    Chart1.LeftAxis.Maximum:=SpEdY.Value;
  end;
end;

procedure TFPanelHistogr1.SpEdXChange(Sender: TObject);
begin
  ChBxXAu.Checked:=False;
  Chart1.BottomAxis.Maximum:=SpEdX.Value+1;
end;

procedure TFPanelHistogr1.SpEdYChange(Sender: TObject);
begin
  ChBxYAu.Checked:=False;
  Chart1.LeftAxis.Maximum:=SpEdY.Value;
end;

procedure TFPanelHistogr1.ColorBox2Change(Sender: TObject);
begin
  Chart1.BackColor:=ColorBox2.Selected;
end;

procedure TFPanelHistogr1.CheckBox3Click(Sender: TObject);
begin
  Chart1.View3D:=CheckBox3.Checked;
end;

procedure TFPanelHistogr1.CheckBox1Click(Sender: TObject);
begin
  Series1.Marks.Visible:=CheckBox1.Checked;
  Series2.Marks.Visible:=CheckBox1.Checked;
end;

procedure TFPanelHistogr1.ComboBox2Change(Sender: TObject);
begin
  CheckBox2.Checked:=True;
  case ComboBox2.ItemIndex of
    0: Series1.BarBrush.Style:=bsBDiagonal;
    1: Series1.BarBrush.Style:=bsClear;
    2: Series1.BarBrush.Style:=bsCross;
    3: Series1.BarBrush.Style:=bsDiagCross;
    4: Series1.BarBrush.Style:=bsFDiagonal;
    5: Series1.BarBrush.Style:=bsHorizontal;
    6: Series1.BarBrush.Style:=bsSolid;
    7: Series1.BarBrush.Style:=bsVertical;
  end;
end;

procedure TFPanelHistogr1.ColorBox3Change(Sender: TObject);
begin
  CheckBox2.Checked:=True;
  Series1.BarBrush.Color:=ColorBox3.Selected;
end;

procedure TFPanelHistogr1.ComboBox3Change(Sender: TObject);
begin
  CheckBox2.Checked:=True;
  case ComboBox3.ItemIndex of
    0: Series1.BarStyle:=bsArrow;
    1: Series1.BarStyle:=bsCilinder;
    2: Series1.BarStyle:=bsEllipse;
    3: Series1.BarStyle:=bsInvPyramid;
    4: Series1.BarStyle:=bsPyramid;
    5: Series1.BarStyle:=bsRectangle;
    6: Series1.BarStyle:=bsRectGradient;
  end;
end;

procedure TFPanelHistogr1.ComboBox4Change(Sender: TObject);
begin
  case ComboBox4.ItemIndex of
    0: Chart1.BottomAxis.Grid.Style:=psClear;
    1: Chart1.BottomAxis.Grid.Style:=psDash;
    2: Chart1.BottomAxis.Grid.Style:=psDashDot;
    3: Chart1.BottomAxis.Grid.Style:=psDashDotDot;
    4: Chart1.BottomAxis.Grid.Style:=psDot;
    5: Chart1.BottomAxis.Grid.Style:=psInsideFrame;
    6: Chart1.BottomAxis.Grid.Style:=psSolid;
  end;
end;

procedure TFPanelHistogr1.ComboBox5Change(Sender: TObject);
begin
  case ComboBox5.ItemIndex of
    0: Chart1.LeftAxis.Grid.Style:=psClear;
    1: Chart1.LeftAxis.Grid.Style:=psDash;
    2: Chart1.LeftAxis.Grid.Style:=psDashDot;
    3: Chart1.LeftAxis.Grid.Style:=psDashDotDot;
    4: Chart1.LeftAxis.Grid.Style:=psDot;
    5: Chart1.LeftAxis.Grid.Style:=psInsideFrame;
    6: Chart1.LeftAxis.Grid.Style:=psSolid;
  end;
end;

procedure TFPanelHistogr1.ColorBox5Change(Sender: TObject);
begin
  CheckBox4.Checked:=True;
  Series2.BarBrush.Color:=ColorBox5.Selected;
end;

procedure TFPanelHistogr1.ComboBox6Change(Sender: TObject);
begin
  CheckBox4.Checked:=True;
  case ComboBox6.ItemIndex of
    0: Series2.BarStyle:=bsArrow;
    1: Series2.BarStyle:=bsCilinder;
    2: Series2.BarStyle:=bsEllipse;
    3: Series2.BarStyle:=bsInvPyramid;
    4: Series2.BarStyle:=bsPyramid;
    5: Series2.BarStyle:=bsRectangle;
    6: Series2.BarStyle:=bsRectGradient;
  end;
end;

procedure TFPanelHistogr1.ComboBox7Change(Sender: TObject);
begin
  CheckBox4.Checked:=True;
  case ComboBox7.ItemIndex of
    0: Series2.BarBrush.Style:=bsBDiagonal;
    1: Series2.BarBrush.Style:=bsClear;
    2: Series2.BarBrush.Style:=bsCross;
    3: Series2.BarBrush.Style:=bsDiagCross;
    4: Series2.BarBrush.Style:=bsFDiagonal;
    5: Series2.BarBrush.Style:=bsHorizontal;
    6: Series2.BarBrush.Style:=bsSolid;
    7: Series2.BarBrush.Style:=bsVertical;
  end;
end;

procedure TFPanelHistogr1.TrackBar1Change(Sender: TObject);
begin
  Timer1.Interval:=TrackBar1.Position*100;
  Label2.Caption:=IntToStr(Timer1.Interval)+' ��';
end;

procedure TFPanelHistogr1.ComboBox1Change(Sender: TObject);
begin
  RadioButton2.Checked:=True;
  CheckBox2.Checked:=True;
end;

procedure TFPanelHistogr1.ComboBox8Change(Sender: TObject);
begin
  RadioButton4.Checked:=True;
  CheckBox4.Checked:=True;
end;

procedure TFPanelHistogr1.RadioButton1Click(Sender: TObject);
begin
  CheckBox2.Checked:=True;
end;

procedure TFPanelHistogr1.RadioButton2Click(Sender: TObject);
begin
  CheckBox2.Checked:=True;
end;

procedure TFPanelHistogr1.ColorBox1Change(Sender: TObject);
begin
  CheckBox2.Checked:=True;
end;

procedure TFPanelHistogr1.RadioButton3Click(Sender: TObject);
begin
  CheckBox4.Checked:=True;
end;

procedure TFPanelHistogr1.RadioButton4Click(Sender: TObject);
begin
  CheckBox4.Checked:=True;
end;

procedure TFPanelHistogr1.ColorBox4Change(Sender: TObject);
begin
  CheckBox4.Checked:=True;
end;

procedure TFPanelHistogr1.SpinEdit1Change(Sender: TObject);
begin
  Series1.CustomBarWidth:=SpinEdit1.Value;
  Series2.CustomBarWidth:=SpinEdit1.Value;
end;

procedure TFPanelHistogr1.CheckBox5Click(Sender: TObject);
begin
  Chart1.Monochrome:=CheckBox5.Checked;
end;



end.
