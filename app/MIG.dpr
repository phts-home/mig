program MIG;

{%ToDo 'MIG.todo'}

uses
  Forms,
  uFMain in 'uFMain.pas' {FMain},
  uFDlgObjAdd in 'uFDlgObjAdd.pas' {FDlgObjAdd},
  uFPanelObjects in 'uFPanelObjects.pas' {FPanelObjects},
  uFDlgOptions in 'uFDlgOptions.pas' {FDlgOptions},
  uFPanelRes in 'uFPanelRes.pas' {FPanelRes},
  uFDlgAbout in 'uFDlgAbout.pas' {FDlgAbout},
  uFDlgProjOpt in 'uFDlgProjOpt.pas' {FDlgProjOpt},
  uFDlgProjCreate in 'uFDlgProjCreate.pas' {FDlgProjCreate},
  uFDlgObjEdit in 'uFDlgObjEdit.pas' {FDlgObjEdit},
  uFPanelProp in 'uFPanelProp.pas' {FPanelProp},
  uFView in 'uFView.pas' {FView},
  uFPanelClasses in 'uFPanelClasses.pas' {FPanelClasses},
  uFDlgClassAdd in 'uFDlgClassAdd.pas' {FDlgClassAdd},
  uFDlgClassEdit in 'uFDlgClassEdit.pas' {FDlgClassEdit},
  uFPanelGraph in 'uFPanelGraph.pas' {FPanelGraph},
  uFPanelHistogr1 in 'uFPanelHistogr1.pas' {FPanelHistogr1},
  uFDlgJPEG in 'uFDlgJPEG.pas' {FDlgJPEG},
  uFDlgObjAddRegion in 'uFDlgObjAddRegion.pas' {FDlgObjAddRegion},
  uFDlgObjAddRegionPick in 'uFDlgObjAddRegionPick.pas' {FDlgObjAddRegionPick};

{$R *.res}
                             
begin
  Application.Initialize;
  Application.Title := '���';
  Application.CreateForm(TFMain, FMain);
  Application.CreateForm(TFDlgProjOpt, FDlgProjOpt);
  Application.CreateForm(TFView, FView);
  Application.CreateForm(TFPanelClasses, FPanelClasses);
  Application.CreateForm(TFPanelObjects, FPanelObjects);
  Application.CreateForm(TFPanelProp, FPanelProp);
  Application.CreateForm(TFPanelRes, FPanelRes);
  Application.CreateForm(TFPanelHistogr1, FPanelHistogr1);
  Application.CreateForm(TFPanelGraph, FPanelGraph);
  Application.CreateForm(TFDlgClassAdd, FDlgClassAdd);
  Application.CreateForm(TFDlgClassEdit, FDlgClassEdit);
  Application.CreateForm(TFDlgObjAdd, FDlgObjAdd);
  Application.CreateForm(TFDlgObjEdit, FDlgObjEdit);
  Application.CreateForm(TFDlgProjCreate, FDlgProjCreate);
  Application.CreateForm(TFDlgOptions, FDlgOptions);
  Application.CreateForm(TFDlgAbout, FDlgAbout);
  Application.CreateForm(TFDlgJPEG, FDlgJPEG);
  Application.CreateForm(TFDlgObjAddRegion, FDlgObjAddRegion);
  Application.CreateForm(TFDlgObjAddRegionPick, FDlgObjAddRegionPick);
  if ParamCount>0 then FMain.LoadProject(ParamStr(1));
  FMain.SetFocus;
  Application.Run;
end.
