unit uFDlgAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ShellApi;

type
  TFDlgAbout = class(TForm)
    Button1: TButton;
    Label4: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    procedure Label9Click(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure Label12Click(Sender: TObject);
    procedure Label9MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Label9MouseLeave(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDlgAbout: TFDlgAbout;

implementation
                      
uses uFMain;

{$R *.dfm}

procedure TFDlgAbout.Label9Click(Sender: TObject);
begin
  ShellExecute(0,'',pchar('mailto:'+Label9.Caption+'?subject=���'),'','',1);
end;

procedure TFDlgAbout.Label10Click(Sender: TObject);
begin
  ShellExecute(0,'',pchar(Label10.Caption),'','',1);
end;

procedure TFDlgAbout.Label12Click(Sender: TObject);
begin
  ShellExecute(0,'',pchar('mailto:'+Label12.Caption+'?subject=���'),'','',1);
end;

procedure TFDlgAbout.Label9MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  (Sender as TLabel).Font.Style:=[fsUnderline];
end;

procedure TFDlgAbout.Label9MouseLeave(Sender: TObject);
begin
  (Sender as TLabel).Font.Style:=[];
end;

procedure TFDlgAbout.FormShow(Sender: TObject);
begin
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
end;

end.
