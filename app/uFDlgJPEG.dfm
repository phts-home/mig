object FDlgJPEG: TFDlgJPEG
  Left = 189
  Top = 105
  Width = 354
  Height = 226
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' JPEG-'#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 25
    Top = 35
    Width = 50
    Height = 13
    Caption = #1050#1072#1095#1077#1089#1090#1074#1086':'
  end
  object Label2: TLabel
    Left = 85
    Top = 35
    Width = 20
    Height = 13
    Caption = '90%'
  end
  object Button1: TButton
    Left = 160
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Ok'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object Button2: TButton
    Left = 250
    Top = 160
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 1
  end
  object TrackBar1: TTrackBar
    Left = 20
    Top = 60
    Width = 311
    Height = 45
    Ctl3D = True
    ParentCtl3D = False
    Position = 9
    TabOrder = 2
    ThumbLength = 15
    TickMarks = tmBoth
    OnChange = TrackBar1Change
  end
end
