object FDlgClassAdd: TFDlgClassAdd
  Left = 380
  Top = 192
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082#1083#1072#1089#1089
  ClientHeight = 443
  ClientWidth = 516
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 275
    Top = 20
    Width = 76
    Height = 13
    Caption = #1048#1084#1103' '#1086#1073#1098#1077#1082#1090#1086#1074':'
  end
  object Label4: TLabel
    Left = 25
    Top = 20
    Width = 64
    Height = 13
    Caption = #1048#1084#1103' '#1082#1083#1072#1089#1089#1072':'
  end
  object Bevel1: TBevel
    Left = 15
    Top = 55
    Width = 481
    Height = 11
    Shape = bsTopLine
  end
  object EdObjName: TEdit
    Left = 365
    Top = 15
    Width = 121
    Height = 21
    MaxLength = 32
    TabOrder = 1
    Text = 'Circle'
    OnClick = EdNameClick
  end
  object GroupBox1: TGroupBox
    Left = 265
    Top = 235
    Width = 231
    Height = 156
    Caption = ' '#1054#1090#1086#1073#1088#1072#1078#1077#1085#1080#1077' '
    TabOrder = 26
    object Label8: TLabel
      Left = 25
      Top = 23
      Width = 46
      Height = 13
      Caption = #1047#1072#1083#1080#1074#1082#1072':'
    end
    object Label9: TLabel
      Left = 25
      Top = 50
      Width = 38
      Height = 13
      Caption = #1050#1086#1085#1090#1091#1088':'
    end
  end
  object EdName: TEdit
    Left = 115
    Top = 15
    Width = 121
    Height = 21
    MaxLength = 32
    TabOrder = 0
    Text = 'Class'
    OnClick = EdNameClick
  end
  object RadioGroup1: TRadioGroup
    Left = 15
    Top = 70
    Width = 231
    Height = 76
    Caption = ' '#1057#1082#1086#1088#1086#1089#1090#1100' '
    ItemIndex = 0
    Items.Strings = (
      #1047#1085#1072#1095#1077#1085#1080#1077':'
      #1048#1085#1090#1077#1088#1074#1072#1083':')
    TabOrder = 2
  end
  object EdSpeed1: TEdit
    Left = 115
    Top = 85
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '1'
    OnChange = EdSpeed1Change
    OnClick = EdNameClick
  end
  object RadioGroup2: TRadioGroup
    Left = 15
    Top = 150
    Width = 231
    Height = 76
    Caption = ' '#1059#1075#1086#1083' '
    ItemIndex = 0
    Items.Strings = (
      #1047#1085#1072#1095#1077#1085#1080#1077':'
      #1048#1085#1090#1077#1088#1074#1072#1083':')
    TabOrder = 6
  end
  object EdSAngl1: TEdit
    Left = 115
    Top = 165
    Width = 121
    Height = 21
    TabOrder = 7
    Text = '60'
    OnChange = EdSAngl1Change
    OnClick = EdNameClick
  end
  object Button1: TButton
    Left = 330
    Top = 405
    Width = 75
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    Default = True
    ModalResult = 1
    TabOrder = 35
  end
  object Button2: TButton
    Left = 420
    Top = 405
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 36
  end
  object Button3: TButton
    Left = 15
    Top = 405
    Width = 75
    Height = 25
    Caption = #1057#1086#1093#1088'. '#1087#1086' '#1091#1084'.'
    TabOrder = 37
    OnClick = Button3Click
  end
  object RadioGroup3: TRadioGroup
    Left = 15
    Top = 235
    Width = 231
    Height = 76
    Caption = ' '#1044#1077#1081#1089#1090#1074#1091#1102#1097#1072#1103' '#1089#1080#1083#1072' '
    ItemIndex = 0
    Items.Strings = (
      #1047#1085#1072#1095#1077#1085#1080#1077':'
      #1048#1085#1090#1077#1088#1074#1072#1083':')
    TabOrder = 10
  end
  object RadioGroup4: TRadioGroup
    Left = 15
    Top = 315
    Width = 231
    Height = 76
    Caption = ' '#1059#1075#1086#1083' '
    ItemIndex = 0
    Items.Strings = (
      #1047#1085#1072#1095#1077#1085#1080#1077':'
      #1048#1085#1090#1077#1088#1074#1072#1083':')
    TabOrder = 14
  end
  object EdAcc1: TEdit
    Left = 115
    Top = 250
    Width = 121
    Height = 21
    TabOrder = 11
    Text = '0'
    OnChange = EdAcc1Change
    OnClick = EdNameClick
  end
  object EdAAngl1: TEdit
    Left = 115
    Top = 330
    Width = 121
    Height = 21
    TabOrder = 15
    Text = '60'
    OnChange = EdAAngl1Change
    OnClick = EdNameClick
  end
  object RadioGroup5: TRadioGroup
    Left = 265
    Top = 70
    Width = 231
    Height = 76
    Caption = ' '#1056#1072#1076#1080#1091#1089' '
    ItemIndex = 0
    Items.Strings = (
      #1047#1085#1072#1095#1077#1085#1080#1077':'
      #1048#1085#1090#1077#1088#1074#1072#1083':')
    TabOrder = 19
  end
  object RadioGroup6: TRadioGroup
    Left = 265
    Top = 150
    Width = 231
    Height = 76
    Caption = ' '#1052#1072#1089#1089#1072' '
    ItemIndex = 0
    Items.Strings = (
      #1047#1085#1072#1095#1077#1085#1080#1077':'
      #1048#1085#1090#1077#1088#1074#1072#1083':')
    TabOrder = 22
  end
  object EdMass1: TEdit
    Left = 365
    Top = 165
    Width = 121
    Height = 21
    TabOrder = 23
    Text = '1'
    OnChange = EdMass1Change
    OnClick = EdNameClick
  end
  object ColorBox2: TColorBox
    Left = 410
    Top = 280
    Width = 76
    Height = 22
    Style = [cbStandardColors, cbPrettyNames]
    ItemHeight = 16
    TabOrder = 28
  end
  object CheckBox1: TCheckBox
    Left = 275
    Top = 305
    Width = 211
    Height = 17
    Caption = #1057#1083#1091#1095#1072#1081#1085#1099#1081' '#1094#1074#1077#1090' '#1079#1072#1083#1080#1074#1082#1080
    TabOrder = 30
  end
  object CheckBox2: TCheckBox
    Left = 275
    Top = 320
    Width = 211
    Height = 17
    Caption = #1057#1083#1091#1095#1072#1081#1085#1099#1081' '#1094#1074#1077#1090' '#1082#1086#1085#1090#1091#1088#1072
    TabOrder = 31
  end
  object CheckBox3: TCheckBox
    Left = 275
    Top = 340
    Width = 211
    Height = 17
    Caption = #1057#1083#1091#1095#1072#1081#1085#1072#1103' '#1096#1080#1088#1080#1085#1072' '#1082#1086#1085#1090#1091#1088#1072
    TabOrder = 32
  end
  object ColorBox1: TColorBox
    Left = 365
    Top = 250
    Width = 121
    Height = 22
    DefaultColorColor = clYellow
    NoneColorColor = clYellow
    Selected = clYellow
    Style = [cbStandardColors, cbPrettyNames]
    ItemHeight = 16
    TabOrder = 27
  end
  object SpinEdit9: TSpinEdit
    Left = 365
    Top = 115
    Width = 56
    Height = 22
    MaxValue = 100
    MinValue = 1
    TabOrder = 20
    Value = 5
    OnChange = SpinEdit9Change
  end
  object SpinEdit8: TSpinEdit
    Left = 180
    Top = 360
    Width = 56
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 17
    Value = 60
    OnChange = SpinEdit7Change
  end
  object SpinEdit7: TSpinEdit
    Left = 115
    Top = 360
    Width = 56
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 16
    Value = 30
    OnChange = SpinEdit7Change
  end
  object SpinEdit6: TSpinEdit
    Left = 180
    Top = 280
    Width = 56
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 13
    Value = 2
    OnChange = SpinEdit5Change
  end
  object SpinEdit5: TSpinEdit
    Left = 115
    Top = 280
    Width = 56
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 12
    Value = 1
    OnChange = SpinEdit5Change
  end
  object SpinEdit4: TSpinEdit
    Left = 180
    Top = 195
    Width = 56
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 9
    Value = 60
    OnChange = SpinEdit3Change
  end
  object SpinEdit3: TSpinEdit
    Left = 115
    Top = 195
    Width = 56
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 8
    Value = 30
    OnChange = SpinEdit3Change
  end
  object SpinEdit2: TSpinEdit
    Left = 180
    Top = 115
    Width = 56
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 2
    OnChange = SpinEdit1Change
  end
  object SpinEdit14: TSpinEdit
    Left = 430
    Top = 360
    Width = 56
    Height = 22
    MaxValue = 9
    MinValue = 1
    TabOrder = 34
    Value = 9
  end
  object SpinEdit13: TSpinEdit
    Left = 365
    Top = 360
    Width = 56
    Height = 22
    MaxValue = 9
    MinValue = 1
    TabOrder = 33
    Value = 1
  end
  object SpEdBW: TSpinEdit
    Left = 365
    Top = 280
    Width = 36
    Height = 22
    MaxValue = 9
    MinValue = 1
    TabOrder = 29
    Value = 1
  end
  object SpinEdit12: TSpinEdit
    Left = 430
    Top = 195
    Width = 56
    Height = 22
    MaxValue = 100
    MinValue = 1
    TabOrder = 25
    Value = 3
    OnChange = SpinEdit11Change
  end
  object SpinEdit11: TSpinEdit
    Left = 365
    Top = 195
    Width = 56
    Height = 22
    MaxValue = 100
    MinValue = 1
    TabOrder = 24
    Value = 1
    OnChange = SpinEdit11Change
  end
  object SpinEdit10: TSpinEdit
    Left = 430
    Top = 115
    Width = 56
    Height = 22
    MaxValue = 100
    MinValue = 1
    TabOrder = 21
    Value = 10
    OnChange = SpinEdit9Change
  end
  object SpinEdit1: TSpinEdit
    Left = 115
    Top = 115
    Width = 56
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 4
    Value = 1
    OnChange = SpinEdit1Change
  end
  object SpEditRad: TSpinEdit
    Left = 365
    Top = 85
    Width = 121
    Height = 22
    MaxValue = 100
    MinValue = 1
    TabOrder = 18
    Value = 10
    OnChange = SpEditRadChange
  end
  object Button4: TButton
    Left = 105
    Top = 405
    Width = 75
    Height = 25
    Caption = #1047#1072#1075#1088'. '#1087#1086' '#1091#1084'.'
    TabOrder = 38
    OnClick = Button4Click
  end
end
