object FDlgOptions: TFDlgOptions
  Left = 222
  Top = 259
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  ClientHeight = 258
  ClientWidth = 514
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 420
    Top = 220
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 1
  end
  object Button2: TButton
    Left = 330
    Top = 220
    Width = 75
    Height = 25
    Caption = 'Ok'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object PageControl1: TPageControl
    Left = 15
    Top = 15
    Width = 481
    Height = 191
    ActivePage = TabSheet1
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = #1048#1085#1090#1077#1088#1092#1077#1081#1089
      object Label11: TLabel
        Left = 195
        Top = 51
        Width = 38
        Height = 13
        Caption = #1092#1072#1081#1083#1086#1074
      end
      object CheckBox1: TCheckBox
        Left = 10
        Top = 20
        Width = 206
        Height = 17
        Caption = #1055#1086#1083#1085#1099#1081' '#1087#1091#1090#1100' '#1074' '#1089#1090#1088#1086#1082#1077' '#1079#1072#1075#1086#1083#1086#1074#1082#1072
        TabOrder = 0
      end
      object CheckBox3: TCheckBox
        Left = 10
        Top = 50
        Width = 131
        Height = 17
        Caption = #1055#1086#1084#1085#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1080#1079':'
        TabOrder = 2
        OnClick = CheckBox3Click
      end
      object SpinEdit1: TSpinEdit
        Left = 140
        Top = 47
        Width = 51
        Height = 22
        EditorEnabled = False
        MaxValue = 30
        MinValue = 0
        TabOrder = 1
        Value = 0
        OnChange = SpinEdit1Change
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100
      ImageIndex = 1
      object Label6: TLabel
        Left = 120
        Top = 50
        Width = 52
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = '10 '#1084#1089
        Enabled = False
      end
      object Label5: TLabel
        Left = 120
        Top = 20
        Width = 52
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = '10 '#1084#1089
      end
      object Label1: TLabel
        Left = 10
        Top = 20
        Width = 113
        Height = 13
        Caption = #1048#1085#1090#1077#1088#1074#1072#1083' '#1074#1099#1095#1080#1089#1083#1077#1085#1080#1081
      end
      object Label2: TLabel
        Left = 10
        Top = 50
        Width = 118
        Height = 13
        Caption = #1048#1085#1090#1088#1077#1074#1072#1083' '#1087#1077#1088#1077#1088#1080#1089#1086#1074#1082#1080
        Enabled = False
      end
      object TrackBar2: TTrackBar
        Left = 175
        Top = 45
        Width = 291
        Height = 26
        Enabled = False
        Max = 100
        Min = 1
        Frequency = 2
        Position = 1
        TabOrder = 2
        ThumbLength = 18
        OnChange = TrackBar2Change
      end
      object CheckBox2: TCheckBox
        Left = 10
        Top = 115
        Width = 451
        Height = 17
        Caption = #1054#1089#1090#1072#1085#1072#1074#1083#1080#1074#1072#1090#1100' '#1087#1088#1080' '#1080#1079#1084#1077#1085#1077#1085#1080#1080' '#1086#1073#1098#1077#1082#1090#1086#1074'/'#1082#1083#1072#1089#1089#1086#1074
        TabOrder = 0
      end
      object TrackBar1: TTrackBar
        Left = 175
        Top = 15
        Width = 291
        Height = 26
        Max = 100
        Min = 1
        Frequency = 2
        Position = 1
        TabOrder = 1
        ThumbLength = 18
        OnChange = TrackBar1Change
      end
      object CheckBox4: TCheckBox
        Left = 10
        Top = 85
        Width = 451
        Height = 17
        Caption = #1042#1080#1079#1091#1072#1083#1080#1079#1072#1094#1080#1103
        TabOrder = 3
        OnClick = CheckBox4Click
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1085#1080#1077' '#1086#1073#1098#1077#1082#1090#1086#1074
      ImageIndex = 2
      object Label3: TLabel
        Left = 10
        Top = 20
        Width = 79
        Height = 13
        Caption = #1057#1084#1077#1097#1077#1085#1080#1077' '#1087#1086' X'
      end
      object Label4: TLabel
        Left = 10
        Top = 50
        Width = 79
        Height = 13
        Caption = #1057#1084#1077#1097#1077#1085#1080#1077' '#1087#1086' Y'
      end
      object SpinEdit3: TSpinEdit
        Left = 120
        Top = 18
        Width = 51
        Height = 22
        EditorEnabled = False
        MaxValue = 200
        MinValue = -200
        TabOrder = 0
        Value = 50
        OnChange = SpinEdit3Change
      end
      object SpinEdit4: TSpinEdit
        Left = 120
        Top = 48
        Width = 51
        Height = 22
        EditorEnabled = False
        MaxValue = 200
        MinValue = -200
        TabOrder = 1
        Value = 50
        OnChange = SpinEdit4Change
      end
      object TrackBar3: TTrackBar
        Left = 175
        Top = 15
        Width = 291
        Height = 26
        Max = 200
        Min = -200
        Frequency = 10
        Position = 50
        TabOrder = 2
        ThumbLength = 18
        OnChange = TrackBar3Change
      end
      object TrackBar4: TTrackBar
        Left = 175
        Top = 45
        Width = 291
        Height = 26
        Max = 200
        Min = -200
        Frequency = 10
        Position = 50
        TabOrder = 3
        ThumbLength = 18
        OnChange = TrackBar4Change
      end
    end
    object TabSheet4: TTabSheet
      Caption = #1040#1089#1089#1086#1094#1080#1072#1094#1080#1103' '#1089' '#1092#1072#1081#1083#1072#1084#1080
      ImageIndex = 3
      object Bevel1: TBevel
        Left = 105
        Top = 25
        Width = 346
        Height = 16
        Shape = bsTopLine
      end
      object Label7: TLabel
        Left = 10
        Top = 15
        Width = 96
        Height = 13
        AutoSize = False
        Caption = 'MIG Project (*.pro)'
      end
      object Label8: TLabel
        Left = 10
        Top = 50
        Width = 37
        Height = 13
        Caption = #1057#1090#1072#1090#1091#1089':'
      end
      object Label9: TLabel
        Left = 60
        Top = 50
        Width = 19
        Height = 13
        Caption = #1053#1077#1090
      end
      object Button3: TButton
        Left = 200
        Top = 45
        Width = 121
        Height = 25
        Caption = #1040#1089#1089#1086#1094#1080#1080#1088#1086#1074#1072#1090#1100
        TabOrder = 0
        OnClick = Button3Click
      end
      object Button4: TButton
        Left = 330
        Top = 45
        Width = 121
        Height = 25
        Caption = #1059#1076#1072#1083#1080#1090#1100' '#1072#1089#1089#1086#1094#1080#1072#1094#1080#1102
        TabOrder = 1
        OnClick = Button4Click
      end
    end
  end
end
