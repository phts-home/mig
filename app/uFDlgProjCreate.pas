unit uFDlgProjCreate;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls;

type
  TFDlgProjCreate = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    ColorBox1: TColorBox;
    SpEdW: TSpinEdit;
    SpEdH: TSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Bevel1: TBevel;
    Memo1: TMemo;
    Label5: TLabel;
    EdScale: TEdit;
    Button4: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdScaleKeyPress(Sender: TObject; var Key: Char);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure SaveDefaultValues;
    procedure LoadDefaultValues;
  end;

var
  FDlgProjCreate: TFDlgProjCreate;

implementation

uses uFMain;

{$R *.dfm}

{-----------------------------------------------------------------------------}
{  procedure     SaveDefaultValues                                            }
{                                                                             }
{  discription   ���������� ���������� ������� �� ���������                   }
{-----------------------------------------------------------------------------}
procedure TFDlgProjCreate.SaveDefaultValues;
begin
  with FMain.DefaultProject do
  begin
    ProjObjectsCount:=0;
    ProjWidth:=SpEdW.Value;
    ProjHeight:=SpEdH.Value;
    ProjBackgr:=ColorBox1.Selected;
    ProjDiscription:=Memo1.Text;
    ProjScale:=StrToFloat(EdScale.Text);
  end;
  FMain.SaveDefaultProject;
end;

{-----------------------------------------------------------------------------}
{  procedure     LoadDefaultValues                                            }
{                                                                             }
{  discription   �������� ���������� ������� �� ���������                     }
{-----------------------------------------------------------------------------}
procedure TFDlgProjCreate.LoadDefaultValues;
begin
  with FMain.DefaultProject do
  begin
    SpEdW.Value:=ProjWidth;
    SpEdH.Value:=ProjHeight;
    ColorBox1.Selected:=ProjBackgr;
    Memo1.Text:=ProjDiscription;
    EdScale.Text:=FloatToStr(ProjScale);
  end;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                           FDlgProjCreate                                    }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFDlgProjCreate.FormCreate(Sender: TObject);
begin
  LoadDefaultValues;
end;

procedure TFDlgProjCreate.FormShow(Sender: TObject);
begin
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
end;

{-----------------------------------------------------------------------------}
{                                                                             }
{                             <Other procedures>                              }
{                                                                             }
{-----------------------------------------------------------------------------}

procedure TFDlgProjCreate.Button3Click(Sender: TObject);
begin
  SaveDefaultValues;
end;

procedure TFDlgProjCreate.Button4Click(Sender: TObject);
begin
  LoadDefaultValues;
end;

procedure TFDlgProjCreate.EdScaleKeyPress(Sender: TObject; var Key: Char);
begin
  if not(((Key >= #48)and(Key <= #57))or(Key = #46)or(Key = #44)or(Key = #8))then
    Key:=#0;
end;

end.
